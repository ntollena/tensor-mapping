#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <x86intrin.h>
#define I (1 << 10)
#define J (1 << 13)
#define K (1 << 6)
#define EPSILON 0.001
#define MIN(a, b) (a < b ?a:b)


void init_zero(float A[], int M, int N) {
	for (int i = 0; i < M; i++) {
		for (int k = 0; k < N; k++) {
			A[i*N+k] = 0.;
		}
	}
}

void init_rand(float A[], int M, int N) {
	for (int i = 0; i < M; i++) {
		for (int k = 0; k < N; k++) {
			//A[i*N+k] = (float) (i*(k+1) % N) / N;
			A[i*N+k] = (float) (i * (k + 1) % N) / N;
		}
	}
}

// Is C = A * B ?
int is_matmul(float * A, float * B, float * C, int NI, int NJ, int NK) {
	float * Check = (float *)aligned_alloc(32, sizeof(float) * NI * NJ);
	init_zero(Check, NI, NJ);
	for (int i = 0; i < NI; i++) {
		for (int j = 0; j < NJ; j++) {
			for (int k = 0; k < NK; k++) {
				Check[i * NJ + j] += A[i * NK + k] * B[k * NJ + j];
			}
		}
	}
	int err_count = 0;
	float max_error = 0.;
	for (int i = 0; i < NI; i++) {
		for (int j = 0; j < NJ; j++) {
			if (Check[i * NJ + j] - C[i * NJ + j] > EPSILON) {
				err_count++;
				float error = (Check[i * NJ + j] > C[i * NJ + j]) ?
				  Check[i * NJ + j] - C[i * NJ + j] :C[i * NJ + j] - Check[i * NJ + j]  ;
				max_error = (error > max_error)? error : max_error;
			}
		}
	}
	printf("Matrix size: %d, Error count : %d max error : %f \n", NI * NJ, err_count, max_error);
	return (err_count == 0);
}

void matmul(float * A, float * B, float * C) {
// INSERT CODE HERE
}

int main() {
  float (*const A) = (float *)aligned_alloc(32, sizeof(float) * I * K);
  float (*const B) = (float *)aligned_alloc(32, sizeof(float) * K * J);
  float *C = (float *)aligned_alloc(32, sizeof(float) * I * J);
  init_rand(A, I, K);
  init_rand(B, K, J);
  init_zero(C, I, J);
  matmul(A, B, C);
  if (is_matmul(A, B, C, I, J, K)) {
    printf("Result is alright\n" );
  } else {
    printf("Something went wrong\n");
  }
}
