open Tensor_loops
open Ids


let map_fst f (x, y) = f x, y
let app_fst p = map_fst (fun f -> f ()) p

module T_tile = Tensor_tile(Arch.AVX512)
module Conv = T_tile.Conv_build(struct
    let dim_gen = Dim.fresh_gen
    let x_dim, reset_x = dim_gen "x" |> app_fst
    let y_dim, reset_y = dim_gen "y" |> app_fst
    let f_dim, reset_f = dim_gen "f" |> app_fst
    let c_dim, reset_c = dim_gen "c" |> app_fst
    let w_dim, reset_w = dim_gen "w" |> app_fst
    let h_dim, reset_h = dim_gen "h" |> app_fst
        (* nwhc *)
    let input = Tensor.(make_join ~name:"input" [|single c_dim;
                                              join_dims y_dim h_dim;
                                              join_dims x_dim w_dim;
                                            |])
    (* nchw *)
    let _input = Tensor.(make_join ~name:"input" [|join_dims x_dim w_dim;
                                              join_dims y_dim h_dim;
                                              single c_dim;
                                            |])
    let params = Tensor.make ~name:"params" [|f_dim; c_dim; h_dim; w_dim|]
    let output =  Tensor.make ~name:"output" [|f_dim; y_dim; x_dim|]

    let reset () = reset_c (); reset_f (); reset_h (); reset_w (); reset_x
        (); reset_y ()
  end)

module MM = T_tile.MM_build( struct
    let dim_gen = Dim.fresh_gen

    let i_dim, reset_i = dim_gen "i"|> map_fst @@ fun f -> f ()
    let j_dim, reset_j = dim_gen "j"|> map_fst @@ fun f -> f ()
    let k_dim, reset_k = dim_gen "k"|> map_fst @@ fun f -> f ()

    let a = Tensor.make ~name:"A" [|k_dim; i_dim|]
    let b = Tensor.make ~name:"B" [|j_dim; k_dim|]
    let c = Tensor.make ~name:"C" [|j_dim; i_dim|]

    let reset () = reset_i (); reset_j (); reset_k ()
  end)

let weird = let open T_tile in let open Conv in
  [ V f_dim;  U (2, f_dim); T (32, x_dim); T (4, c_dim); R w_dim;  Pack_tens input;
    T (32, f_dim); R c_dim; R y_dim; R x_dim; R f_dim; R h_dim;]

let for_steph_conv_tvm_like = let open T_tile in let open Conv in
  [ V f_dim;  U (8, f_dim); T (512, c_dim); R w_dim;
    T (32, f_dim); R c_dim; R y_dim; R x_dim; R f_dim; R h_dim;]

let for_steph_conv = let open T_tile in let open Conv in
  [ V f_dim;  U (2, f_dim); T (6, y_dim); T (512, c_dim); R w_dim;
    T (32, f_dim); R c_dim; R y_dim; R x_dim; R f_dim; R h_dim;]

 let mobilNet_3_motoko = let open T_tile in let open Conv in    
[V f_dim; U (1, f_dim); U (14, y_dim); U (3, h_dim); T (128, c_dim); Hoist_vars [c_dim]; T (1, x_dim);
     T (3, w_dim); T (1, h_dim); T (1, y_dim); T (8, f_dim); T (1, c_dim); T (56, x_dim); T (4, y_dim)]

 let mobilNet_3 = let open T_tile in let open Conv in    
      [V f_dim; U (2, f_dim); U (14, y_dim); U (3, h_dim); T (128, c_dim); Hoist_vars [c_dim];
       T (1, x_dim);    
       T (3, w_dim); T (1, h_dim); T (1, y_dim); T (8, f_dim); T (1, c_dim); T (56, x_dim); T (4, y_dim)]

let mobilNet_4 = let open T_tile in let open Conv in
    [V f_dim; U (1, f_dim); U (14, y_dim); U (3, h_dim); T (128, c_dim); Hoist_vars [c_dim]; T (1, x_dim);    
     T (3, w_dim); T (1, h_dim); T (1, y_dim); T (8, f_dim); T (1, c_dim); T (56, x_dim); T (4, y_dim)]   

 let mobilNet_3_tvm_like =let open T_tile in let open Conv in    
   [V f_dim; U (8, f_dim); U (4, c_dim);
    R h_dim; R w_dim;
    Hoist_vars [h_dim; w_dim];
    T (14, x_dim); T (2, y_dim);
    Pack_tens params;
    T (2, f_dim);
    T (32, c_dim);
    T (4, x_dim);
    T (28, y_dim)]

let for_stephane = let open T_tile in let open MM in
  [V j_dim; U (2, j_dim); U (6, i_dim); T (512, k_dim); R i_dim; R j_dim; R k_dim]

let lame = let open T_tile in let open MM in
  [V j_dim; U (2, j_dim); U (2, i_dim); T (32, j_dim);  R i_dim; T (64, k_dim); R j_dim; R k_dim]

let try_pack = let open T_tile in let open MM in
  [   ULambda j_dim; U (2, i_dim);  R k_dim;
      Pack_tens a;
     Lambda_apply (j_dim, [Iter 2, Arg 3; Iter 6, Arg 2]);
     R i_dim; R j_dim]

let lambda_test = let open T_tile in let open MM in
  [      R k_dim; TLambda j_dim; R i_dim;
      Lambda_apply (j_dim, [Iter 2, Arg 3; Iter 6, Arg 2]);
       R j_dim]

let auguste_tile (* Gen with IOLB %*) =let open T_tile in let open Conv in
  [
    V f_dim;
    U (2, f_dim);
    U (12, y_dim);
    T (32, c_dim);
    Hoist_vars [c_dim];
    (*T (4, x_dim);*)
    R x_dim;
    R w_dim;
    Pack_tens input;
    R h_dim;
    (* L2 full *)
    R y_dim;
    R c_dim;
    R f_dim;
  ]

let unroll_k =  let open T_tile in let open MM in
  [V j_dim; U(2, k_dim); T (32, k_dim); Hoist_vars [k_dim]; R i_dim; R j_dim;  R k_dim]

let dull_tile = let open T_tile in let open MM in
  [R i_dim; R j_dim;  R k_dim]

let _pack_tile = let open T_tile in let open MM in
  [R i_dim; R k_dim; Pack_tens a; R j_dim;  ]

let trans_tile = let open T_tile in let open MM in
  [R i_dim; R k_dim; Pack_trans (a, [k_dim; i_dim]); R j_dim]

let lambda_tile = let open T_tile in let open MM in
  [U (3, j_dim); T (32, i_dim);  T (2, j_dim); R i_dim; R j_dim; R k_dim]

let lambda_tile2 = let open T_tile in let open MM in
  [U (2, j_dim); T (32, i_dim);  T (3, j_dim); R i_dim; R j_dim; R k_dim]

(*let running_example = let open T_tile in let open MM in
[V j_dim; U (2, j_dim); U_lambda  "label" i_dim; T (64, k_dim);
   Lambda_apply "label" i_dim [(4, 3), (6, 1)];  Pack_tens b; R i_dim; R k_dim; R j_dim]*)
let running_example1 = let open T_tile in let open MM in
[V j_dim; U (2, j_dim); U  (3, i_dim); T (64, k_dim);
   T  (4, i_dim) ;  Pack_tens b; R i_dim; R k_dim; R j_dim]

let running_example2 = let open T_tile in let open MM in
  [V j_dim;  U (2, j_dim); U (3, i_dim);  T (64, k_dim);
   T  (6, i_dim) ;  Pack_tens b; R i_dim; R k_dim; R j_dim]

let pack_tile =let open T_tile in let open MM in
  [U (3, j_dim); T (32, i_dim);  T (256, k_dim); Pack_tens b; T (8, i_dim); R j_dim; R k_dim; R i_dim]

let dull_tile_conv2 = let open T_tile in let open Conv in
  [V f_dim; R f_dim; T_par (2, x_dim); R y_dim; R w_dim; R c_dim; R x_dim; R h_dim]

let unroll_tile = let open T_tile in let open MM in
  [U (2, j_dim); R i_dim; R j_dim; R k_dim]

let unroll_jam_tile = let open T_tile in let open MM in
  [U (2, j_dim); U (2, i_dim); R k_dim; R j_dim; R i_dim]

let pack_tile_conv2 = let open T_tile in let open Conv in
  [V f_dim; U (2, f_dim); T (32, x_dim); R y_dim; Pack_tens input; R w_dim; R
     x_dim; R c_dim; R f_dim; R h_dim]

let permute_tile = let open T_tile in let open MM in
  [V j_dim; U (2, j_dim); T(16, k_dim); Hoist_vars [k_dim]; Pack_tens c; R k_dim;  R i_dim; R j_dim]

let other_tile = let open T_tile in let open MM in
  [V j_dim; U (2, k_dim); T (4, i_dim); R i_dim; T(8, j_dim); R k_dim; R j_dim]

let motoko_tile = let open T_tile in let open MM in
  [V j_dim; U (2, j_dim); U (2, i_dim); T (64, k_dim); (*Hoist_vars [k_dim];*)
   T (192, j_dim);T (192, i_dim); (*Pack_tens a;*) Pack_tens b; R i_dim; R k_dim; R j_dim]

let miracle_tile_3x3 = let open T_tile in let open MM in
  [V j_dim; U (3, j_dim); U (3, i_dim); T (128, k_dim); Hoist_vars [k_dim];
   R  j_dim; R i_dim; (*Pack_tens a; Pack_tens b;*)
   R k_dim; ]

let miracle_tile_4x4 = let open T_tile in let open MM in
  [V j_dim; U (8, j_dim); U (8, i_dim); T (128, k_dim); Hoist_vars [k_dim];
   R  j_dim; R i_dim; (*Pack_tens a; Pack_tens b;*)
   R k_dim; ]

let miracle_tile = let open T_tile in  let open MM in
  [V j_dim; U (2, j_dim); U (6, i_dim); T (128, k_dim); Hoist_vars [k_dim];
   R  j_dim; R i_dim; Pack_tens a; Pack_tens b;
   R k_dim; ]

let other_trans_tile = let open T_tile in  let open MM in
  [T (8, i_dim); T (8, k_dim); Pack_trans (a, [k_dim; i_dim]); R i_dim; R k_dim; R j_dim]

let tile_9x3= let open T_tile in  let open MM in
  [V j_dim; U (3, j_dim); U (9, i_dim); T (148, k_dim);  Hoist_vars [k_dim]; ]

let resNet18_10 = let open T_tile in let open Conv in
  [V f_dim; U (2, f_dim); U (7, y_dim); T (128, c_dim); T (7, x_dim); T (3, w_dim);
   T (3, h_dim); T_par (16, f_dim); T (4, c_dim)]


let conv (f,c,y,x,w,h) tile = 
  let loop_code =  let open Conv in
    gen_code  ~dim_sizes:[f_dim, f; c_dim, c; y_dim, y; x_dim, x; w_dim, w;
                          h_dim, h] tile in
  print_endline loop_code

let conv_from_tile_only  tile = 
  let loop_code =  let open Conv in
    let size d = T_tile.dim_tile_size tile d in
    let f, c, y, x, w, h=  size f_dim, size c_dim, size y_dim,
                           size x_dim, size w_dim, size h_dim in
    gen_code  ~dim_sizes:[f_dim, f; c_dim, c; y_dim, y; x_dim, x; w_dim, w;
                          h_dim, h] tile in
  print_endline loop_code

let mm tile = 
  let loop_code =  let open MM in
    gen_code tile in
  print_endline loop_code

let external_call = let open T_tile in  let open MM in 
  [External_call {name = "flute"; fixed_size =[j_dim, 16; (*k_dim, 256*)];
                  var_dim = i_dim; tensors_list = [a; b; c]; max_range = 8;
                  dynamic_sizes = [k_dim, 3]};
   T (2, j_dim); Tile_exact (6, i_dim); (*Tile_exact (25, j_dim);*) R i_dim; R j_dim; R k_dim
  ]

let partial_tile = let open T_tile in  let open MM in 
  [  Tile_partial (8, i_dim); Tile_gexact (20, i_dim); Tile_exact (70, i_dim);
   R i_dim; R j_dim; R k_dim]

let buggy_partial =let open T_tile in let open Conv in
  [V f_dim;  T (256, c_dim);
   Tile_partial (32, f_dim);
   Tile_exact (48, f_dim);
   Tile_partial (3, y_dim); 
   Tile_gexact (10, y_dim); (*Tile_exact (45, y_dim);*)  R y_dim]


let blu () = mm lambda_test
let truc () = mm unroll_k
let vlan () = conv_from_tile_only Conv.[V f_dim; U (2, f_dim); U (12, y_dim);
                                   T (256, c_dim); Hoist_vars [c_dim]]


(* let () = conv (128,128,56,56,3,3) mobilNet_3_tvm_like *)
let  () = mm external_call
(* let () = mm partial_tile *)
 let _a () = conv_from_tile_only external_call
