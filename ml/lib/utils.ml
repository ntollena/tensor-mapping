include Batteries

module Opt_syntax = struct
  let ( let+ ) opt f = Option.map f opt

  let ( and+ ) opt1 opt2 =
    match (opt1, opt2) with Some x, Some y -> Some (x, y) | _ -> None

  let ( let* ) = Option.bind
  let ( and* ) = ( and+ )
end

let bimap (f, g) (a, b) = (f a, g b)
let map_fst f = bimap (f, Fun.id)
let map_snd f = bimap (Fun.id, f)

module Either = struct
  type ('a, 'b) t = Left of 'a | Right of 'b [@@deriving show]

  let left x = Left x
  let right x = Right x
  let map_left f = function Left a -> Left (f a) | r -> r

  let map_left_or_fail f = function
    | Left a -> Left (f a)
    | _ -> failwith "Supposed to be called on left"

  let apply_left_exn : exn -> ('a -> 'b -> 'c) -> ('a, 'd) t -> 'b -> ('c, 'd) t
      =
   fun error f either arg ->
    match either with Left a -> Left (f a arg) | Right _ -> raise error

  let apply_left2_exn :
      exn ->
      ('a -> 'arg1 -> 'arg2 -> 'c) ->
      ('a, 'd) t ->
      'arg1 ->
      'arg2 ->
      ('c, 'd) t =
   fun error f either arg1 arg2 ->
    match either with Left a -> Left (f a arg1 arg2) | Right _ -> raise error

  let apply_left3_exn :
      exn ->
      ('a -> 'arg1 -> 'arg2 -> 'arg3 -> 'c) ->
      ('a, 'd) t ->
      'arg1 ->
      'arg2 ->
      'arg3 ->
      ('c, 'd) t =
   fun error f either arg1 arg2 arg3 ->
    match either with
    | Left a -> Left (f a arg1 arg2 arg3)
    | Right _ -> raise error

  let apply_left4_exn :
      exn ->
      ('a -> 'arg1 -> 'arg2 -> 'arg3 -> 'arg4 -> 'c) ->
      ('a, 'd) t ->
      'arg1 ->
      'arg2 ->
      'arg3 ->
      'arg4 ->
      ('c, 'd) t =
   fun error f either arg1 arg2 arg3 arg4 ->
    match either with
    | Left a -> Left (f a arg1 arg2 arg3 arg4)
    | Right _ -> raise error

  let map_right f = function Right b -> Left (f b) | l -> l

  let map_right_or_fail f = function
    | Right b -> Right (f b)
    | _ -> failwith "Supposed to be called on right"

  let apply_right_exn :
      exn -> ('a -> 'b -> 'c) -> ('d, 'a) t -> 'b -> ('d, 'c) t =
   fun error f either arg ->
    match either with Right a -> Right (f a arg) | Left _ -> raise error

  let bimap f g = function Left a -> Left (f a) | Right b -> Right (g b)

  (* ('a -> ('b, 'c) Either.t) -> 'a list -> ('b list, 'c list) Either.t list *)
  let partition_consecutive f seg_list =
    let rec segregate current_list acc_list proceeding =
      match (current_list, proceeding) with
      | Left current_list, [] ->
          List.rev (Left (List.rev current_list) :: acc_list)
      | Right current_list, [] ->
          List.rev (Right (List.rev current_list) :: acc_list)
      | Left current_list, head :: tail -> (
          match f head with
          | Left head -> segregate (Left (head :: current_list)) acc_list tail
          | Right head ->
              segregate (Right [ head ])
                (Left (List.rev current_list) :: acc_list)
                tail)
      | Right current_list, head :: tail -> (
          match f head with
          | Right head -> segregate (Right (head :: current_list)) acc_list tail
          | Left head ->
              segregate (Left [ head ])
                (Right (List.rev current_list) :: acc_list)
                tail)
    in
    match seg_list with
    | [] -> []
    | head :: tail -> (
        match f head with
        | Left head -> segregate (Left [ head ]) [] tail
        | Right head -> segregate (Right [ head ]) [] tail)
end

type ('a, 'b) either = ('a, 'b) Either.t = Left of 'a | Right of 'b

module Option = struct
  include Option

  let from_cond_val cond x = if cond then Some x else None
end

module List = struct
  include List
  include List.Exceptionless

  let insert_at_end l elt =
    let rec insert acc = function
      | [] -> List.rev (elt :: acc)
      | head :: tail -> insert (head :: acc) tail
    in
    insert [] l

  let rec assoc_eq eq a list_a =
    match list_a with
    | [] -> None
    | (a', ret) :: _ when eq a' a -> Some ret
    | _ :: tail -> assoc_eq eq a tail

  let rec mem_eq eq a list_a =
    match list_a with
    | [] -> false
    | head :: tail -> eq a head || mem_eq eq a tail

  let remove_assoc eq a list_a : 'b option * ('a * 'b) list =
    let rec remove accum a = function
      | [] -> (None, List.rev accum)
      | (a', ret) :: tail when eq a' a -> (Some ret, List.rev_append accum tail)
      | head :: tail -> remove (head :: accum) a tail
    in
    remove [] a list_a

  let modify_assoc eq f elt l =
    let rec modify acc = function
      | [] -> (
          match f None with
          | Some value -> List.rev ((elt, value) :: acc)
          | None -> List.rev acc)
      | (key, value) :: tail when eq key elt ->
          List.rev_append acc
            (match f @@ Some value with
            | Some value -> (key, value) :: tail
            | None -> tail)
      | (key, value) :: tail -> modify ((key, value) :: acc) tail
    in
    modify [] l

  let split3 l =
    let rec split l1 l2 l3 = function
      | [] -> (List.rev l1, List.rev l2, List.rev l3)
      | (x1, x2, x3) :: tail -> split (x1 :: l1) (x2 :: l2) (x3 :: l3) tail
    in
    split [] [] [] l
end

module Array = struct
  include Array

  let find_opt p arr = try Some (Array.find p arr) with Not_found -> None
end

let span p l =
  let rec loop pre_list = function
    | [] -> (List.rev pre_list, None, [])
    | h :: t when p h -> (List.rev pre_list, Some h, t)
    | h :: t -> loop (h :: pre_list) t
  in
  loop [] l

(* fold_apply: 'a -> ('a -> 'a) list -> 'a *)
let fold_apply init f_list =
  List.fold_left (fun current f -> f current) init f_list

module type MAPPABLE = sig
  type 'a t

  val return : 'a -> 'a t
  val map : 'a t -> ('a -> 'b) -> 'b t
end

module type MONAD = sig
  type 'a t

  val return : 'a -> 'a t
  val bind : 'a t -> ('a -> 'b t) -> 'b t
end

module List_monad : MONAD with type 'a t = 'a list = struct
  type 'a t = 'a list

  let return x = [ x ]
  let bind x f = List.concat_map f x
end

module Mappable (M : MONAD) : MAPPABLE with type 'a t = 'a M.t = struct
  include M

  let map m f = bind m (fun a -> return (f a))
end

module type STATE = sig
  type state

  include MONAD

  val get : state t
  val put : state -> unit t
  val runState : 'a t -> init:state -> state * 'a
end

module State (S : sig
  type t
end) : STATE with type state = S.t = struct
  type state = S.t
  type 'a t = state -> state * 'a

  let return v s = (s, v)

  let bind m k s =
    let s', v = m s in
    k v s'

  let get s = (s, s)
  let put s _ = (s, ())
  let runState m ~init = m init
end

module type SYNTAX = sig
  include MONAD

  val ( let* ) : 'a t -> ('a -> 'b t) -> 'b t
  val ( let+ ) : 'a t -> ('a -> 'b) -> 'b t
end

module MSyntax (M : sig
  include MONAD

  val map : 'a t -> ('a -> 'b) -> 'b t
end) : SYNTAX with type 'a t = 'a M.t = struct
  include M

  let ( let* ) = bind
  let ( let+ ) = map
end

module type CUSTOM_MAP = sig
  type ord

  include Map.S with type key = ord

  val find : key -> 'a t -> 'a option
  val choose : 'a t -> (key * 'a) option
  val any : 'a t -> (key * 'a) option
  val modify_map : key -> ('a option -> 'a option * 'b) -> 'a t -> 'a t * 'b
end

module Make_map (Ord : sig
  type t

  val compare : t -> t -> int
end) : CUSTOM_MAP with type ord := Ord.t = struct
  module Map_exc = Map.Make (Ord)
  include Map_exc
  include Map_exc.Exceptionless

  let modify_map key f dict =
    let new_dict_opt, value = f (find key dict) in
    (modify_opt key (Fun.const new_dict_opt) dict, value)
end
