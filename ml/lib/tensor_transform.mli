open Ids
open Instruction
open Kernels_sign

type iter = Iter of int [@@deriving show]
type arg = Arg of int [@@deriving show]

module Tensor_tile (A : Arch.Vec_arch_t) : sig
  module Inst : Inst_t with module A = A
  module LN : Loop_nest.Loopnest_t with module Inst := Inst

  type ext_call_arg = {
    name : string;
    fixed_size : (Dim.t * int) list;
    var_dim : Dim.t;
    dynamic_sizes : (Dim.t * int) list;
    tensors_list : Tensor.t list;
    max_range : int;
  }
  [@@deriving show]

  (* Invariant : Some transformers impose divisibility, such as V/U/Ulambda
   * Tile_exact n is legal if and only if n is dividible by the current
   * divisibilty constraint *)
  type loop_type =
    | U of int * Dim.t
    | V of Dim.t
    | External_call of ext_call_arg
    (* Tile_partial supports a dynamic bound *)
    | Tile_partial of int * Dim.t
    | Tile_gexact of int * Dim.t
    (* Tile_exact declares a dynamic variable that can be used as a
     * dynamic bound *)
    | Tile_exact of int * Dim.t
      (* Tile_Exact (n, d) does exactly n
         iterations - no multiplication *)
    | ULambda of Dim.t
    | TLambda of Dim.t
    | Lambda_apply of Dim.t * (iter * arg) list
    | T of int * Dim.t
    | T_par of int * Dim.t
    | Fused_T_pars of (int * Dim.t) list
    | Pack_tens of Tensor.t
    | Pack_trans of Tensor.t * Dim.t list
    | Hoist_vars of Dim.t list
    | R of Dim.t
  [@@deriving show]

  type tile_scheme = loop_type list

  val tile_to_string : tile_scheme -> string
  val dim_tile_size : tile_scheme -> Dim.t -> int

  module MM_build (MM_args : MM_args_t) : sig
    include MM_args_t

    val gen_code : ?dim_sizes:(Dim.t * int) list -> tile_scheme -> string
  end

  module Conv_build (Conv_args : Conv_args_t) : sig
    include Conv_args_t

    val gen_code : ?dim_sizes:(Dim.t * int) list -> tile_scheme -> string
  end

  module TC_build (TC_args : TC_args_t) : sig
    include TC_args_t

    val gen_code : ?dim_sizes:(Dim.t * int) list -> tile_scheme -> string
  end
end
