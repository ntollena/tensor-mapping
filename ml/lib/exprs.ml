open Utils
open Ids

module Expr = struct
  type t =
    | Const of int
    | One
    | Zero
    | Add of t * t
    | Sub of t * t
    | Mul of t * t
    | Min of t * t
    | Var of string
    | SizeVar of Size.id
    | PlaceHolder of string
    | IndVar of Index.t

  let is_atomic = function
    | Const _ | One | Zero | IndVar _ | PlaceHolder _ | Var _ | SizeVar _ ->
        true
    | _ -> false

  let rec is_constant = function
    | IndVar _ | Var _ -> false
    | Zero | One | Const _ | SizeVar _ | PlaceHolder _ -> true
    | Add (e1, e2) | Sub (e1, e2) | Mul (e1, e2) | Min (e1, e2) ->
        is_constant e1 && is_constant e2

  let rec show =
    let show_paren e =
      if is_atomic e then show e else Format.sprintf "(%s)" (show e)
    in
    function
    | Const i -> Format.sprintf "%d" i
    | Zero -> "0"
    | One -> "1"
    | Var name -> name
    | Add (e1, e2) -> Format.sprintf "%s + %s" (show_paren e1) (show_paren e2)
    | Sub (e1, e2) -> Format.sprintf "%s - %s" (show_paren e1) (show_paren e2)
    | Mul (e1, e2) -> Format.sprintf "%s * %s" (show_paren e1) (show_paren e2)
    | Min (e1, e2) -> Format.sprintf "MIN(%s, %s)" (show e1) (show e2)
    | PlaceHolder s -> s
    | SizeVar ind_id -> Format.sprintf "%s" (Size.show_id ind_id)
    | IndVar index -> Format.sprintf "%s" (Index.show_id (Index.id index))

  let pp fmt expr = Format.fprintf fmt "%s" (show expr)

  let rec recursor f = function
    | (Const _ | One | Zero) as expr -> f expr
    | Add (e1, e2) -> (
        match (recursor f e1, recursor f e2) with
        | e1, Zero -> e1
        | Zero, e2 -> e2
        | Const c1, Const c2 -> Const (c1 + c2)
        | e1, e2 -> Add (e1, e2))
    | Sub (e1, e2) -> (
        match (recursor f e1, recursor f e2) with
        | e1, Zero -> e1
        | Const c1, Const c2 -> Const (c1 - c2)
        | e1, e2 -> Sub (e1, e2))
    | Mul (e1, e2) -> (
        match (recursor f e1, recursor f e2) with
        | _, Zero -> Zero
        | Zero, _ -> Zero
        | e1, One -> e1
        | One, e2 -> e2
        | Const c1, Const c2 -> Const (c1 * c2)
        | e1, e2 -> Mul (e1, e2))
    | Min (e1, e2) -> (
        match (recursor f e1, recursor f e2) with
        | _, Zero | Zero, _ -> Zero
        | One, Const c | Const c, One -> if c < 1 then Zero else One
        | Const c1, Const c2 -> Const (min c1 c2)
        | e1, e2 -> Min (e1, e2))
    | SizeVar _ as sv -> f sv
    | PlaceHolder _ as ph -> f ph
    | IndVar _ as e -> f e
    | Var _ as e -> f e

  let rec equal e1 e2 =
    match (e1, e2) with
    | Const c1, Const c2 -> Int.equal c1 c2
    | One, One -> true
    | Zero, Zero -> true
    | Add (e11, e12), Add (e21, e22) -> equal e11 e21 && equal e12 e22
    | Sub (e11, e12), Sub (e21, e22) -> equal e11 e21 && equal e12 e22
    | Mul (e11, e12), Mul (e21, e22) -> equal e11 e21 && equal e12 e22
    | Min (e11, e12), Min (e21, e22) -> equal e11 e21 && equal e12 e22
    | SizeVar sid1, SizeVar sid2 -> Size.equal_id sid1 sid2
    | PlaceHolder s1, PlaceHolder s2 -> String.equal s1 s2
    | IndVar vid1, IndVar vid2 -> Index.equal vid1 vid2
    | Var n1, Var n2 -> String.equal n1 n2
    | _ -> false

  let one = One
  let zero = Zero
  let const i = match i with 0 -> Zero | 1 -> One | _ -> Const i

  let add e1 e2 =
    match (e1, e2) with
    | Zero, e | e, Zero -> e
    | Const c1, Const c2 -> Const (c1 + c2)
    | _ -> Add (e1, e2)

  let sub e1 e2 =
    match (e1, e2) with
    | e, Zero -> e
    | Const c1, Const c2 -> Const (c1 - c2)
    | _ -> Sub (e1, e2)

  let mul e1 e2 =
    match (e1, e2) with
    | One, e | e, One -> e
    | Zero, _ | _, Zero -> Zero
    | Const c1, Const c2 -> Const (c1 * c2)
    | _ -> Mul (e1, e2)

  let min e1 e2 =
    match (e1, e2) with
    | _, Zero | Zero, _ -> Zero
    | One, Const c | Const c, One -> if c < 1 then Zero else One
    | Const c1, Const c2 -> Const (min c1 c2)
    | e1, e2 -> Min (e1, e2)

  let rec to_int_opt sizes_indexes =
    let open Opt_syntax in
    function
    | SizeVar id -> List.assoc_opt id sizes_indexes
    | PlaceHolder _ -> None
    | Const i -> Some i
    | One -> Some 1
    | Zero -> Some 0
    | Add (x, y) ->
        let+ x' = to_int_opt sizes_indexes x
        and+ y' = to_int_opt sizes_indexes y in
        x' + y'
    | Sub (x, y) ->
        let+ x' = to_int_opt sizes_indexes x
        and+ y' = to_int_opt sizes_indexes y in
        x' - y'
    | Mul (x, y) ->
        let+ x' = to_int_opt sizes_indexes x
        and+ y' = to_int_opt sizes_indexes y in
        x' * y'
    | Min (x, y) ->
        let+ x' = to_int_opt sizes_indexes x
        and+ y' = to_int_opt sizes_indexes y in
        Int.min x' y'
    | _ -> None

  let simplify' =
    let f = function
      | Add (Zero, e) | Add (e, Zero) -> e
      | Mul (Zero, _) | Mul (_, Zero) -> Zero
      | Mul (One, e) | Mul (e, One) -> e
      | Mul (Const c1, Const c2) -> Const (c1 * c2)
      | Add (Const c1, Const c2) -> Const (c1 + c2)
      | Sub (Const c1, Const c2) -> Const (c1 - c2)
      | e -> e
    in
    recursor f

  let rec fix eq f e = if eq (f e) e then e else fix eq f (f e)
  let simplify = fix equal simplify'

  (* This could be nicer with Algebra *)
  let rec expand e =
    match e with
    | (Const _ | One | Zero | PlaceHolder _ | SizeVar _ | Var _ | IndVar _) as e
      ->
        e
    | Add (e1, e2) -> Add (expand e1, expand e2)
    | Sub (e1, e2) -> Sub (expand e1, expand e2)
    | Min (e1, e2) -> Min (expand e1, expand e2)
    | Mul (e1, e2) -> (
        match (expand e1, expand e2) with
        | Add (e1, e2), Add (e3, e4) ->
            Add
              ( Add (Mul (e1, e3), Mul (e1, e4)),
                Add (Mul (e2, e3), Mul (e2, e4)) )
        | Add (e1, e2), Sub (e3, e4) ->
            Add
              ( Sub (Mul (e1, e3), Mul (e1, e4)),
                Sub (Mul (e2, e3), Mul (e2, e4)) )
        | Sub (e1, e2), Add (e3, e4) ->
            Sub
              ( Add (Mul (e1, e3), Mul (e1, e4)),
                Add (Mul (e2, e3), Mul (e2, e4)) )
        | Sub (e1, e2), Sub (e3, e4) ->
            Add
              ( Sub (Mul (e1, e3), Mul (e1, e4)),
                Sub (Mul (e2, e4), Mul (e2, e3)) )
        | e1, Add (e2, e3) -> Add (Mul (e1, e2), Mul (e1, e3))
        | Add (e1, e2), e3 -> Add (Mul (e1, e3), Mul (e2, e3))
        | e1, Sub (e2, e3) -> Sub (Mul (e1, e2), Mul (e1, e3))
        | Sub (e1, e2), e3 -> Sub (Mul (e1, e3), Mul (e2, e3))
        | e1, e2 -> Mul (e1, e2))

  type sign = Plus | Minus

  let negate = function Plus -> Minus | Minus -> Plus

  let sign_mul s1 s2 =
    match (s1, s2) with Plus, Minus | Minus, Plus -> Minus | _ -> Plus

  let rec normalize e =
    let open Utils in
    match e with
    | Const _ | One | Zero | PlaceHolder _ | SizeVar _ | Var _ | IndVar _ ->
        Left (Plus, [ e ])
    | Min (e1, e2) -> (
        match (normalize e1, normalize e2) with
        | Left p1, Left p2 -> Right [ p1; p2 ]
        | Right e1, Left p2 -> Right (e1 @ [ p2 ])
        | Left p1, Right l2 -> Right (p1 :: l2)
        | Right p1, Right p2 -> Right (p1 @ p2))
    | Add (e1, e2) -> (
        match (normalize e1, normalize e2) with
        | Left p1, Left p2 -> Right [ p1; p2 ]
        | Right e1, Left p2 -> Right (e1 @ [ p2 ])
        | Left p1, Right l2 -> Right (p1 :: l2)
        | Right p1, Right p2 -> Right (p1 @ p2))
    | Sub (e1, e2) -> (
        match (normalize e1, normalize e2) with
        | Left p1, Left (s2, e2) -> Right [ p1; (negate s2, e2) ]
        | Right e1, Left (s2, e2) -> Right (e1 @ [ (negate s2, e2) ])
        | Left p1, Right l2 ->
            Right (p1 :: List.map (fun (s, e) -> (negate s, e)) l2)
        | Right l1, Right l2 ->
            Right (l1 @ List.map (fun (s, e) -> (negate s, e)) l2))
    | Mul (e1, e2) -> (
        let distribute concat p lp =
          List.map
            (fun (s2, l2) ->
              let s1, l1 = p in
              (sign_mul s1 s2, concat l1 l2))
            lp
        in
        let distribute_left = distribute List.append in
        let distribute_right = distribute (Fun.flip List.append) in
        match (normalize e1, normalize e2) with
        | Left (s1, l1), Left (s2, l2) -> Left (sign_mul s1 s2, l1 @ l2)
        | Left p1, Right l2 -> Right (distribute_left p1 l2)
        | Right l1, Left p2 -> Right (distribute_right p2 l1)
        | Right l1, Right l2 ->
            let l_fused = List.concat_map (fun p -> distribute_left p l2) l1 in
            Right l_fused)

  let size sid = SizeVar sid

  (* takes f that maps a size to another ConstExpr
   * (potentially living in another "space") *)
  let rec map_size_index f = function
    | SizeVar id -> f id
    | (Const _ | PlaceHolder _ | One | Zero) as c -> c
    | e -> recursor (map_size_index f) e

  let instanciate_placeholder var c =
    let f = function
      | PlaceHolder s when String.equal var s -> Const c
      | e -> e
    in
    recursor f

  (* contain_var [var_expr] [var_id] returns true if Variable var_id is used in var_expr *)
  let rec contain_var var_expr var_id =
    match var_expr with
    | IndVar index -> Index.equal index var_id
    | Zero | One | Const _ | Var _ | SizeVar _ | PlaceHolder _ -> false
    | Add (e1, e2) | Sub (e1, e2) | Mul (e1, e2) | Min (e1, e2) ->
        contain_var e1 var_id || contain_var e2 var_id

  let index ind_id = IndVar ind_id

  (* contain_var [var_expr] [var_id] returns true if Variable var_id is used in var_expr *)
  let rec access_dim_id var_expr dim =
    match var_expr with
    | IndVar index -> Dim.equal_id (Index.dim index) dim
    | Zero | One | Const _ -> false
    | Var _ | SizeVar _ | PlaceHolder _ -> false
    | Add (e1, e2) | Sub (e1, e2) | Mul (e1, e2) | Min (e1, e2) ->
        access_dim_id e1 dim || access_dim_id e2 dim

  (* contain_var [var_expr] [var_id] returns true if Variable var_id is used in var_expr *)
  let access_dim var_expr dim = access_dim_id var_expr (Dim.id dim)

  (* Apply a function on all indexes by recursing on an expression *)
  let rec map_index f = function
    | IndVar vid -> f vid
    | (Const _ | One | Zero | SizeVar _ | PlaceHolder _) as e -> e
    | e -> recursor (map_index f) e

  let alpha_replace_pred pred var_expr expr =
    let replace vid = if pred vid then var_expr else IndVar vid in
    map_index replace expr

  let alpha_replace var_id var_expr expr =
    alpha_replace_pred (Index.equal var_id) var_expr expr

  let alpha_replace_dim dim var_expr expr =
    alpha_replace_pred
      (fun vid -> Dim.equal_id (Index.dim vid) dim)
      var_expr expr

  let alpha_conv old_vid new_vid = alpha_replace old_vid (IndVar new_vid)
  let prop_const old_vid const expr = alpha_replace old_vid (Const const) expr

  module I = struct
    let ( + ) = add
    let ( - ) = sub
    let ( * ) = mul
  end
end
