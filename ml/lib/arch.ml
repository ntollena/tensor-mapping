module type Vec_arch_t = sig
  val base_type_name : string
  val vec_size : int
  val vec_type_name : string
  val transpose_func : string

  val gen_load :
    (string -> string, unit, string, string, string, string) format6

  val gen_store :
    (string -> string -> string, unit, string, string, string, string) format6

  val gen_add :
    (string -> string -> string, unit, string, string, string, string) format6

  val gen_mul :
    (string -> string -> string, unit, string, string, string, string) format6

  val gen_sub :
    (string -> string -> string, unit, string, string, string, string) format6

  val gen_fma :
    ( string -> string -> string -> string,
      unit,
      string,
      string,
      string,
      string )
    format6

  val gen_broadcast :
    (string -> string, unit, string, string, string, string) format6
end

module SSE : Vec_arch_t = struct
  let base_type_name = "float"
  let vec_size = 4
  let vec_type_name = "__m128"
  let transpose_func = "transpose_base_stride_sse"
  let gen_add = Stdlib.format_of_string "_mm_add_ps(%s, %s)"
  let gen_sub = Stdlib.format_of_string "_mm_sub_ps(%s, %s)"
  let gen_mul = Stdlib.format_of_string "_mm_mul_ps(%s, %s)"

  (* gen_fma a b c = a * b + c *)
  let gen_fma = Stdlib.format_of_string "_mm_add_ps(_mm_mul_ps(%s, %s), %s)"
  let gen_broadcast = Stdlib.format_of_string "_mm_set1_ps(%s)"
  let gen_load = Stdlib.format_of_string "_mm_load_ps(&%s)"
  let gen_store = Stdlib.format_of_string "_mm_store_ps(&%s, %s)"
end

module AVX2 : Vec_arch_t = struct
  let base_type_name = "float"
  let vec_size = 8
  let vec_type_name = "__m256"
  let transpose_func = "transpose_base_stride_avx2"
  let gen_add = Stdlib.format_of_string "_mm256_add_ps(%s, %s)"
  let gen_sub = Stdlib.format_of_string "_mm256_sub_ps(%s, %s)"
  let gen_mul = Stdlib.format_of_string "_mm256_mul_ps(%s, %s)"

  (* gen_fma a b c = a * b + c *)
  let gen_fma = Stdlib.format_of_string "_mm256_fmadd_ps(%s, %s, %s)"
  let gen_broadcast = Stdlib.format_of_string "_mm256_set1_ps(%s)"
  let gen_load = Stdlib.format_of_string "_mm256_load_ps(&%s)"
  let gen_store = Stdlib.format_of_string "_mm256_store_ps(&%s, %s)"
end

module AVX2_unaligned : Vec_arch_t = struct
  let base_type_name = "float"
  let vec_size = 8
  let vec_type_name = "__m256"
  let transpose_func = "transpose_base_stride_avx2"
  let gen_add = Stdlib.format_of_string "_mm256_add_ps(%s, %s)"
  let gen_sub = Stdlib.format_of_string "_mm256_sub_ps(%s, %s)"
  let gen_mul = Stdlib.format_of_string "_mm256_mul_ps(%s, %s)"

  (* gen_fma a b c = a * b + c *)
  let gen_fma = Stdlib.format_of_string "_mm256_fmadd_ps(%s, %s, %s)"
  let gen_broadcast = Stdlib.format_of_string "_mm256_set1_ps(%s)"
  let gen_load = Stdlib.format_of_string "_mm256_loadu_ps(&%s)"
  let gen_store = Stdlib.format_of_string "_mm256_storeu_ps(&%s, %s)"
end

module AVX2_INT32 : Vec_arch_t = struct
  let base_type_name = "int32_t"
  let vec_size = 8
  let vec_type_name = "__m256i"
  let transpose_func = "unimplemented"
  let gen_add = Stdlib.format_of_string "_mm256_add_epi32(%s, %s)"
  let gen_sub = Stdlib.format_of_string "_mm256_sub_epi32(%s, %s)"
  let gen_mul = Stdlib.format_of_string "_mm256_mul_epi32(%s, %s)"

  let gen_fma =
    Stdlib.format_of_string "_mm256_add_epi32(_mm256_mul_epi32(%s, %s), %s)"

  let gen_broadcast = Stdlib.format_of_string "_mm256_set1_epi32(%s)"
  let gen_load = Stdlib.format_of_string "_mm256_load_epi32((void*)&%s)"
  let gen_store = Stdlib.format_of_string "_mm256_store_epi32((void*)&%s, %s)"
end

module AVX512 : Vec_arch_t = struct
  let base_type_name = "float"
  let vec_size = 16
  let vec_type_name = "__m512"
  let transpose_func = "transpose_base_stride_avx512"
  let gen_add = Stdlib.format_of_string "_mm512_add_ps(%s, %s)"
  let gen_sub = Stdlib.format_of_string "_mm512_sub_ps(%s, %s)"
  let gen_mul = Stdlib.format_of_string "_mm512_mul_ps(%s, %s)"
  let gen_fma = Stdlib.format_of_string "_mm512_fmadd_ps(%s, %s, %s)"
  let gen_broadcast = Stdlib.format_of_string "_mm512_set1_ps(%s)"
  let gen_load = Stdlib.format_of_string "_mm512_load_ps(&%s)"
  let gen_store = Stdlib.format_of_string "_mm512_store_ps(&%s, %s)"
end

module AVX512_unaligned : Vec_arch_t = struct
  let base_type_name = "float"
  let vec_size = 16
  let vec_type_name = "__m512"
  let transpose_func = "transpose_base_stride_avx512"
  let gen_add = Stdlib.format_of_string "_mm512_add_ps(%s, %s)"
  let gen_sub = Stdlib.format_of_string "_mm512_sub_ps(%s, %s)"
  let gen_mul = Stdlib.format_of_string "_mm512_mul_ps(%s, %s)"
  let gen_fma = Stdlib.format_of_string "_mm512_fmadd_ps(%s, %s, %s)"
  let gen_broadcast = Stdlib.format_of_string "_mm512_set1_ps(%s)"
  let gen_load = Stdlib.format_of_string "_mm512_loadu_ps(&%s)"
  let gen_store = Stdlib.format_of_string "_mm512_storeu_ps(&%s, %s)"
end
