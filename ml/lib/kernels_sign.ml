open Ids

module type TC_args_t = sig
  val left: Dim.t list
  val right: Dim.t list
  val red: Dim.t list
  val a: Tensor.t
  val b: Tensor.t
  val c: Tensor.t
  val reset: unit -> unit
end

module type MM_args_t = sig
  val i_dim: Dim.t
  val j_dim: Dim.t
  val k_dim: Dim.t
  val a: Tensor.t
  val b: Tensor.t
  val c: Tensor.t
  val reset: unit -> unit
end

module type Conv_args_t = sig
  val x_dim: Dim.t
  val w_dim: Dim.t
  val y_dim: Dim.t
  val h_dim: Dim.t
  val f_dim: Dim.t
  val c_dim: Dim.t
  val input: Tensor.t
  val params: Tensor.t
  val output: Tensor.t
  val reset: unit -> unit
end
