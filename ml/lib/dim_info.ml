open Utils
open Ids
open Exprs
open Instruction
open Loop_nest

type div_constraint = Flexible of int | Div [@@deriving show]

module DI (Inst : Inst_t) (LN : Loopnest_t with module Inst := Inst) = struct
  module DimMap = Make_map (struct
    type t = Dim.t

    let compare = Dim.compare
  end)

  type loop = U of int | V | S of int | A
  type bumped = (Index.t * Index.t) option

  type t = {
    dim : Dim.t;
    incr : Expr.t;
    last_tile_size : Expr.t option;
    par : bool;
    loop_scheme : loop list;
    is_vectorized : bool;
    is_closed : bool;
    div_constraint : div_constraint;
    index_clone : Index.gen;
    bump_info : bumped;
    next_index : Index.t;
    current_index : Index.t;
    indexes_list : (Index.t * Index.gen * (Index.t * Expr.t) list) list;
  }
  [@@deriving fields]

  (* A frozen snapshot of a dim state at some instant *)
  module Frozen = struct
    module T_arg = struct
      type t = {
        dim : Dim.t;
        incr : Expr.t;
        is_vectorized : bool;
        current_pack : Index.t;
        current_tile : Index.t;
        next_index : Index.t;
        current_index : Index.t;
        indexes_list : (Index.t * (Index.t * Expr.t) list) list;
      }
      [@@deriving eq, show]
    end

    module C_arg = struct
      type t = {
        dim : Dim.t;
        is_vectorized : bool;
        incr : Expr.t;
        last_index : Index.t;
        last_tile : Index.t option;
        indexes_list : (Index.t * (Index.t * Expr.t) list) list;
      }
      [@@deriving eq, show]
    end

    type t =
      | Nothing of Dim.t
        (* This dimension did not appear in the scheme yet
                                 *)
      | VecUnr of Dim.t * Expr.t * bool
      | Tile of T_arg.t
      | Close of C_arg.t (* This dimension is currently closed *)

    let dim_incr = function
      | Nothing _ -> Expr.const 0
      | VecUnr (_, incr, _) -> incr
      | Tile t_arg -> t_arg.incr
      | Close c_arg -> c_arg.incr

    let filter_indexes big_tile_index small_tile_index =
      let rec aux = function
        | [], l -> Some l
        | (ind1, _) :: t1, (ind2, _) :: t2 ->
            if Index.equal ind1 ind2 then aux (t1, t2) else None
        | _ -> None
      in
      (* indexes are sorted from bigger to smaller, reverse that so we can filter *)
      aux (List.rev big_tile_index, List.rev small_tile_index)
      |> Option.map List.rev

    (* This type allows us to have exhaustiveness check when we match on a pair that we assume to be well-formed
     * VU means vector/unroll *)
    type ordered_pair =
      | Close_close of C_arg.t
      | Close_tile of Index.t * C_arg.t * T_arg.t
      | Close_vu of C_arg.t * Expr.t * bool
      | Close_nothing of C_arg.t
      | Tile_tile of Index.t option * T_arg.t * T_arg.t
      | Same_tile of T_arg.t
      | Tile_vu of T_arg.t * Expr.t * bool
      | Tile_nothing of T_arg.t
      | VU_VU of Dim.t * Expr.t * Expr.t * bool
      | Same_VU of Dim.t * Expr.t * bool
      | VU_nothing of Dim.t * Expr.t * bool
      | Nothing_nothing of Dim.t
    [@@deriving show]

    (* returns Some ordered_pair if pairs are indeed well ordered, None elsewise *)
    let are_well_ordered t1 t2 =
      match (t1, t2) with
      | Nothing d1, Nothing d2 ->
          Option.from_cond_val (Dim.equal d1 d2) (Nothing_nothing d1)
      | VecUnr (d1, incr, is_vectorized), Nothing d2 ->
          Option.from_cond_val (Dim.equal d1 d2)
            (VU_nothing (d1, incr, is_vectorized))
      | VecUnr (d1, incr1, is_v1), VecUnr (d2, incr2, is_v2) ->
          if Expr.equal incr1 incr2 && is_v1 = is_v2 then
            Some (Same_VU (d1, incr1, is_v1))
          else
            Option.from_cond_val
              (Dim.equal d1 d2 && is_v1 = is_v2)
              (VU_VU (d1, incr1, incr2, is_v1))
      | Tile (T_arg.{ dim = d2; _ } as t_arg), Nothing d1 ->
          Option.from_cond_val (Dim.equal d1 d2) (Tile_nothing t_arg)
      | Tile (T_arg.{ dim = d2; _ } as t_arg), VecUnr (d1, incr, is_vec) ->
          Option.from_cond_val (Dim.equal d1 d2) (Tile_vu (t_arg, incr, is_vec))
      | T_arg.(
          ( Tile ({ dim = d1; indexes_list = big_indxs; _ } as t_big),
            Tile ({ dim = d2; indexes_list = small_indxs; _ } as t_small) )) ->
          Option.bind (filter_indexes big_indxs small_indxs) (function
            | (ind, _) :: _ ->
                Option.from_cond_val (Dim.equal d1 d2)
                  (Tile_tile (Some ind, t_big, t_small))
            | [] ->
                if T_arg.equal t_big t_small then Some (Same_tile t_big)
                else
                  Option.from_cond_val (Dim.equal d1 d2)
                    (Tile_tile (None, t_big, t_small)))
      | Close (C_arg.{ dim = d2; _ } as c_arg), Nothing d1 ->
          Option.from_cond_val (Dim.equal d1 d2) (Close_nothing c_arg)
      | Close (C_arg.{ dim = d2; _ } as c_arg), VecUnr (d1, incr, is_vec) ->
          Option.from_cond_val (Dim.equal d1 d2)
            (Close_vu (c_arg, incr, is_vec))
      | C_arg.(
          ( Close ({ dim = d1; indexes_list = small_indxs; _ } as c_big),
            Tile ({ dim = d2; indexes_list = big_indxs; _ } as t_small) )) ->
          Option.bind (filter_indexes big_indxs small_indxs) (function
            | (ind, _) :: _ ->
                Option.from_cond_val (Dim.equal d1 d2)
                  (Close_tile (ind, c_big, t_small))
            | [] -> None)
      | Close c1, Close c2 ->
          Option.from_cond_val (C_arg.equal c1 c2) (Close_close c1)
      | _ -> None

    exception Ill_ordered_tiles

    module Zip = LN.Zipper

    let gen_tile_loop to_vectorize =
      let get_incr incr =
        match (incr, to_vectorize) with
        | Expr.Const i, true
          when i mod Inst.A.vec_size = 0 && i >= Inst.A.vec_size ->
            Expr.const Inst.A.vec_size
        | Expr.SizeVar _, true -> Expr.const Inst.A.vec_size
        | _ -> Expr.one
      in
      function
      | Nothing_nothing dim | VU_nothing (dim, _, _) ->
          (* We pass here for weird reason, even if there is a "T w" over us *)
          let index = Index.from_dim dim in
          ([], Expr.index index, Expr.zero, Fun.id)
      | Same_VU (dim, incr, _) ->
          let increment = get_incr incr in
          let index = Index.from_dim dim in
          let global_index = Index.map_prefix index (fun s -> s ^ "g") in
          let local_index = Index.map_prefix index (fun s -> s ^ "l") in
          (*let tile_index = Index.map_prefix index (fun s -> s ^ "t") in*)
          let start = Expr.index index in
          let halt = Expr.(I.(start + incr)) in
          let aux = [ (local_index, Expr.zero) ] in
          ( [ global_index; local_index ],
            Expr.index global_index,
            Expr.index local_index,
            Zip.new_seq ~aux global_index start halt increment )
      | VU_VU _ ->
          (* TODO handle this case *)
          assert false
      | Tile_nothing T_arg.{ current_pack; _ } ->
          ([], Expr.index current_pack, Expr.zero, Fun.id)
      | Tile_vu (T_arg.{ dim; current_pack; _ }, incr, _) ->
          let increment = get_incr incr in
          let index = Index.from_dim dim in
          let global_index = Index.map_prefix index (fun s -> s ^ "g") in
          let local_index = Index.map_prefix index (fun s -> s ^ "l") in
          let aux = [ (local_index, Expr.zero) ] in
          let start = Expr.index current_pack in
          let halt = Expr.(I.(start + incr)) in
          ( [ global_index; local_index ],
            Expr.index global_index,
            Expr.index local_index,
            Zip.new_seq ~aux global_index start halt increment )
      | Close_nothing C_arg.{ dim; _ } ->
          ([], Expr.index @@ Index.from_dim dim, Expr.zero, Fun.id)
      | Close_vu (C_arg.{ dim; _ }, incr, _) ->
          let increment = get_incr incr in
          let index = Index.from_dim dim in
          let all_index = Index.map_prefix index (fun s -> s ^ "all") in
          let local_index = Index.map_prefix index (fun s -> s ^ "l") in
          let aux = [ (local_index, Expr.zero) ] in
          let start = Expr.index index in
          let halt = Expr.(I.(start + incr)) in
          ( [ all_index; local_index ],
            Expr.index all_index,
            Expr.index local_index,
            Zip.new_seq ~aux all_index start halt increment )
      | Tile_tile (Some tile_index, _, T_arg.{ incr; current_index; _ }) ->
          let increment = get_incr incr in
          let local_index = Index.map_prefix current_index (fun s -> s ^ "l") in
          let glob_index = Index.map_prefix current_index (fun s -> s ^ "g") in
          let aux = [ (local_index, Expr.zero) ] in
          let start = Expr.index tile_index in
          let halt = Expr.(I.(start + incr)) in
          ( [ glob_index; local_index ],
            Expr.index glob_index,
            Expr.index local_index,
            Zip.new_seq ~aux glob_index start halt increment )
      | Tile_tile (None, _, T_arg.{ incr; next_index; current_index; _ }) ->
          let increment = get_incr incr in
          let local_index = Index.map_prefix current_index (fun s -> s ^ "l") in
          let glob_index = Index.map_prefix current_index (fun s -> s ^ "g") in
          let aux = [ (local_index, Expr.zero) ] in
          let start = Expr.index next_index in
          let halt = Expr.(I.(start + incr)) in
          ( [ glob_index; local_index ],
            Expr.index glob_index,
            Expr.index local_index,
            Zip.new_seq ~aux glob_index start halt increment )
      | Same_tile T_arg.{ current_index; incr; _ } ->
          let increment = get_incr incr in
          let _tile_index =
            Index.map_prefix current_index (fun s -> s ^ "tile")
          in
          let glob_index = Index.map_prefix current_index (fun s -> s ^ "g") in
          let local_index = Index.map_prefix current_index (fun s -> s ^ "l") in
          let start = Expr.index current_index in
          let aux = [ (local_index, Expr.zero) ] in
          let halt = Expr.(I.(start + incr)) in
          ( [ glob_index; local_index ],
            Expr.index glob_index,
            Expr.index local_index,
            Zip.new_seq ~aux glob_index start halt increment )
      | Close_tile (_, _, T_arg.{ incr; current_index; _ }) ->
          let increment = get_incr incr in
          let local_index = Index.map_prefix current_index (fun s -> s ^ "l") in
          let glob_index = Index.map_prefix current_index (fun s -> s ^ "g") in
          let aux = [ (local_index, Expr.zero) ] in
          let start = Expr.index current_index in
          let halt = Expr.(I.(start + incr)) in
          ( [ local_index; glob_index ],
            Expr.index glob_index,
            Expr.index local_index,
            Zip.new_seq ~aux glob_index start halt increment )
      | Close_close C_arg.{ last_index; incr; dim; _ } ->
          let increment = get_incr incr in
          let index = Index.map_prefix last_index (fun s -> s ^ "all") in
          let start = Expr.zero in
          let halt = Expr.(size @@ Dim.size_id dim) in
          ( [ index ],
            Expr.index index,
            Expr.index index,
            Zip.new_seq index start halt increment )

    (* Special case for join dimension, we have to match on both dim state *)
    let gen_fused_join_loop to_vectorize iter_dim main_dim aux_dim main_diff
        aux_diff =
      let size_of dim = Expr.size @@ Dim.size_id dim in
      let get_incr incr =
        match (incr, to_vectorize) with
        | Expr.Const i, true
          when i mod Inst.A.vec_size = 0 && i >= Inst.A.vec_size ->
            Expr.const Inst.A.vec_size
        | Expr.SizeVar _, true -> Expr.const Inst.A.vec_size
        | _ -> Expr.one
      in
      match (aux_diff, main_diff) with
      | Nothing_nothing _, _ ->
          let indexes, local_index, global_index, loop =
            gen_tile_loop to_vectorize main_diff
          in
          let aux_index = Index.from_dim aux_dim in
          ( aux_index :: indexes,
            [
              (Dim.id aux_dim, Expr.index aux_index);
              (Dim.id main_dim, global_index);
            ],
            [ (Dim.id aux_dim, Expr.zero); (Dim.id main_dim, local_index) ],
            loop )
      | Close_nothing _, (Nothing_nothing _ | VU_nothing _) ->
          (* Never vectorize on small dimension *)
          let indexes, local_index, _, loop = gen_tile_loop false aux_diff in
          let main_index = Index.from_dim main_dim in
          ( main_index :: indexes,
            [
              (Dim.id main_dim, Expr.index main_index);
              (Dim.id aux_dim, Expr.index @@ Index.from_dim aux_dim);
            ],
            [ (Dim.id main_dim, Expr.zero); (Dim.id aux_dim, local_index) ],
            loop )
      | Close_nothing _, Close_nothing _ ->
          let indexes, _, _global_index, loop = gen_tile_loop false aux_diff in
          let main_index = Index.from_dim main_dim in
          ( main_index :: indexes,
            [
              (Dim.id main_dim, Expr.index main_index);
              (Dim.id aux_dim, Expr.index @@ Index.from_dim aux_dim);
            ],
            [ (Dim.id main_dim, Expr.zero); (Dim.id aux_dim, Expr.zero) ],
            loop )
      | Close_nothing _, Close_vu (_, incr, _) ->
          let increment = get_incr incr in
          let main_index = Index.from_dim main_dim
          and aux_index = Index.from_dim aux_dim in
          let global_index = Index.map_prefix main_index (fun s -> s ^ "g") in
          let local_index = Index.map_prefix main_index (fun s -> s ^ "l") in
          let aux = [ (local_index, Expr.zero) ] in
          let start = Expr.index main_index in
          let halt = Expr.(I.(start + incr)) in
          ( [ global_index; local_index ],
            [
              (Dim.id main_dim, Expr.index global_index);
              (Dim.id aux_dim, Expr.index aux_index);
            ],
            [
              (Dim.id main_dim, Expr.index local_index);
              (Dim.id aux_dim, Expr.zero);
            ],
            Zip.new_seq ~aux global_index start halt increment )
      | Close_nothing _, Close_tile (main_ind, _, T_arg.{ incr; _ }) ->
          let increment = get_incr incr in
          (* Find better names. main_ind is the next index, main_index is ranging
           * from main_ind to main_ind + incr *)
          let main_index = Index.from_dim main_dim
          and aux_index = Index.from_dim aux_dim in
          let global_index = Index.map_prefix main_index (fun s -> s ^ "g") in
          let local_index = Index.map_prefix main_index (fun s -> s ^ "l") in
          let aux = [ (local_index, Expr.zero) ] in
          let start = Expr.index main_ind in
          let halt = Expr.(I.(start + incr)) in
          ( [ global_index; local_index ],
            [
              (Dim.id main_dim, Expr.index global_index);
              (Dim.id aux_dim, Expr.index aux_index);
            ],
            [
              (Dim.id main_dim, Expr.index local_index);
              (Dim.id aux_dim, Expr.zero);
            ],
            Zip.new_seq ~aux global_index start halt increment )
      | Close_nothing _, Close_close C_arg.{ incr; _ } ->
          let increment = get_incr incr in
          let main_index = Index.from_dim main_dim in
          let aux_index = Index.from_dim aux_dim in
          let global_index = Index.map_prefix main_index (fun s -> s ^ "g") in
          let local_index = Index.map_prefix main_index (fun s -> s ^ "l") in
          let aux = [ (local_index, Expr.zero) ] in
          let start = Expr.index aux_index in
          let halt = Expr.(I.(start + size_of main_dim)) in
          ( [ global_index; local_index ],
            [
              (Dim.id main_dim, Expr.index global_index);
              (Dim.id aux_dim, Expr.index @@ Index.from_dim aux_dim);
            ],
            [
              (Dim.id main_dim, Expr.index local_index);
              (Dim.id aux_dim, Expr.zero);
            ],
            Zip.new_seq ~aux global_index start halt increment )
      | Close_nothing _, _ ->
          print_endline @@ show_ordered_pair main_diff;
          print_endline @@ show_ordered_pair aux_diff;
          assert false
      | Close_vu _, (Nothing_nothing _ | VU_nothing _) ->
          (* Never vectorize on small dimension *)
          let indexes, local_index, global_index, loop =
            gen_tile_loop false aux_diff
          in
          let main_index = Index.from_dim main_dim in
          ( main_index :: indexes,
            [
              (Dim.id main_dim, Expr.index main_index);
              (Dim.id aux_dim, global_index);
            ],
            [ (Dim.id main_dim, Expr.zero); (Dim.id aux_dim, local_index) ],
            loop )
      | Close_vu (_, aux_incr, _), Close_tile (main_index, _, T_arg.{ incr; _ })
        ->
          let increment = get_incr incr in
          let aux_index = Index.from_dim aux_dim in
          let global_index = Index.from_dim iter_dim in
          let local_index = Index.map_prefix global_index (fun s -> s ^ "g") in
          let aux = [ (local_index, Expr.zero) ] in
          let start = Expr.(I.(index main_index + index aux_index)) in
          let halt = Expr.(I.(start + aux_incr + incr - const 1)) in
          ( [ global_index; local_index ],
            [ (Dim.id iter_dim, Expr.index global_index) ],
            [ (Dim.id iter_dim, Expr.index local_index) ],
            Zip.new_seq ~aux global_index start halt increment )
      | Close_vu (_, aux_incr, _), Close_close C_arg.{ incr; _ } ->
          let increment = get_incr incr in
          let aux_index = Index.from_dim aux_dim in
          let global_index = Index.from_dim iter_dim in
          let local_index = Index.map_prefix global_index (fun s -> s ^ "g") in
          let aux = [ (local_index, Expr.zero) ] in
          let start = Expr.index aux_index in
          let halt = Expr.(I.(start + size_of main_dim + aux_incr - const 1)) in
          ( [ global_index; local_index ],
            [ (Dim.id iter_dim, Expr.index global_index) ],
            [ (Dim.id iter_dim, Expr.index local_index) ],
            Zip.new_seq ~aux global_index start halt increment )
      | Close_vu (_, aux_incr, _), Close_nothing C_arg.{ incr; _ } ->
          let increment = get_incr incr in
          let global_index = Index.from_dim iter_dim in
          let local_index = Index.map_prefix global_index (fun s -> s ^ "g") in
          let aux = [ (local_index, Expr.zero) ] in
          let main_index = Index.from_dim main_dim
          and aux_index = Index.from_dim aux_dim in
          let start = Expr.(I.(index main_index + index aux_index)) in
          let halt = Expr.(I.(start + aux_incr - const 1)) in
          ( [ global_index; local_index ],
            [ (Dim.id iter_dim, Expr.index global_index) ],
            [ (Dim.id iter_dim, Expr.index local_index) ],
            Zip.new_seq ~aux global_index start halt increment )
      | Close_vu _, _ ->
          print_endline @@ show_ordered_pair main_diff;
          print_endline @@ show_ordered_pair aux_diff;
          assert false
      | Close_close _aux_cargs, (Nothing_nothing _ | VU_nothing _) ->
          (* Never vectorize on small dimension *)
          let indexes, local_index, global_index, loop =
            gen_tile_loop false aux_diff
          in
          let main_index = Index.from_dim main_dim in
          ( main_index :: indexes,
            [
              (Dim.id main_dim, Expr.index main_index);
              (Dim.id aux_dim, global_index);
            ],
            [ (Dim.id main_dim, Expr.zero); (Dim.id aux_dim, local_index) ],
            loop )
      | Close_close _, Close_vu (_, main_incr, _) ->
          let increment = get_incr main_incr in
          let iter_index = Index.from_dim iter_dim in
          let global_index = Index.map_prefix iter_index (fun s -> s ^ "_g") in
          let local_index = Index.map_prefix iter_index (fun s -> s ^ "_l") in
          let main_index = Index.from_dim main_dim in
          let aux = [ (local_index, Expr.zero) ] in
          let start = Expr.index main_index in
          let halt = Expr.(I.(start + main_incr + size_of aux_dim - const 1)) in
          ( [ global_index; local_index ],
            [ (Dim.id iter_dim, Expr.index global_index) ],
            [ (Dim.id iter_dim, Expr.index local_index) ],
            Zip.new_seq ~aux global_index start halt increment )
      | Close_close _, Close_tile (ind, _, T_arg.{ incr; _ }) ->
          let increment = get_incr incr in
          let global_index = Index.from_dim iter_dim in
          let local_index = Index.map_prefix global_index (fun s -> s ^ "g") in
          let aux = [ (local_index, Expr.zero) ] in
          let start = Expr.index ind in
          let halt = Expr.(I.(start + incr + size_of aux_dim - const 1)) in
          ( [ global_index; local_index ],
            [ (Dim.id iter_dim, Expr.index global_index) ],
            [ (Dim.id iter_dim, Expr.index local_index) ],
            Zip.new_seq ~aux global_index start halt increment )
      | Close_close _, Close_close C_arg.{ incr; _ } ->
          let increment = get_incr incr in
          let global_index = Index.from_dim iter_dim in
          let local_index = Index.map_prefix global_index (fun s -> s ^ "g") in
          let aux = [ (local_index, Expr.zero) ] in
          let start = Expr.zero in
          let halt = Expr.(I.(size_of main_dim + size_of aux_dim - const 1)) in
          ( [ global_index; local_index ],
            [ (Dim.id iter_dim, Expr.index global_index) ],
            [ (Dim.id iter_dim, Expr.index local_index) ],
            Zip.new_seq ~aux global_index start halt increment )
      | Close_close _, Close_nothing C_arg.{ incr; _ } ->
          let increment = get_incr incr in
          let global_index = Index.from_dim iter_dim in
          let local_index = Index.map_prefix global_index (fun s -> s ^ "g") in
          let aux = [ (local_index, Expr.zero) ] in
          let main_index = Index.from_dim main_dim in
          let start = Expr.index main_index in
          let halt = Expr.(I.(start + size_of aux_dim - const 1)) in
          ( [ global_index; local_index ],
            [ (Dim.id iter_dim, Expr.index global_index) ],
            [ (Dim.id iter_dim, Expr.index local_index) ],
            Zip.new_seq ~aux global_index start halt increment )
      | _ ->
          print_endline @@ show_ordered_pair main_diff;
          print_endline @@ show_ordered_pair aux_diff;
          assert false

    let interval_loop_tile big_tile small_tile to_vectorize =
      let ordered_pair =
        Option.get_exn (are_well_ordered big_tile small_tile) Ill_ordered_tiles
      in
      gen_tile_loop to_vectorize ordered_pair

    let interval_loop_tile_join iter_dim main_dim aux_dim main_big_tile
        main_small_tile aux_big_tile aux_small_tile to_vectorize =
      let main_ordered_pair =
        Option.get_exn
          (are_well_ordered main_big_tile main_small_tile)
          Ill_ordered_tiles
      in
      let aux_ordered_pair =
        Option.get_exn
          (are_well_ordered aux_big_tile aux_small_tile)
          Ill_ordered_tiles
      in
      gen_fused_join_loop to_vectorize iter_dim main_dim aux_dim
        main_ordered_pair aux_ordered_pair
  end

  (* Get a frozen snapshot of the state of this dimension at a given moment *)
  let freeze dim = function
    | Some
        {
          dim;
          incr;
          is_vectorized;
          is_closed;
          bump_info;
          current_index;
          next_index;
          indexes_list;
          _;
        } -> (
        let open Frozen in
        let indexes_list =
          List.map (fun (ind, _, expr_list) -> (ind, expr_list)) indexes_list
        in
        match bump_info with
        | Some (current_tile, current_pack) ->
            if is_closed then
              Close
                {
                  dim;
                  incr;
                  is_vectorized;
                  last_tile = Some current_tile;
                  last_index = current_index;
                  indexes_list;
                }
            else
              Tile
                {
                  dim;
                  incr;
                  is_vectorized;
                  current_index;
                  next_index;
                  current_pack;
                  current_tile;
                  indexes_list;
                }
        | None ->
            if is_closed then
              Close
                {
                  dim;
                  incr;
                  is_vectorized;
                  last_tile = None;
                  last_index = current_index;
                  indexes_list;
                }
            else VecUnr (dim, incr, is_vectorized))
    | None -> Nothing dim

  let current_tile { bump_info; _ } = Option.map fst bump_info
  let current_pack { bump_info; _ } = Option.map snd bump_info

  let bump_index
      ({ next_index; index_clone; bump_info; current_index; indexes_list; _ } as
      elem) =
    let new_next = index_clone () in
    match bump_info with
    | None ->
        let current_pack, new_gen =
          Index.fresh_clone_mark current_index "p" ()
        in
        let indexes_list =
          [ (current_index, new_gen, [ (current_pack, Expr.zero) ]) ]
        in
        {
          elem with
          next_index = new_next;
          bump_info = Some (current_pack, current_pack);
          indexes_list;
          current_index = next_index;
        }
    | Some _ ->
        let fold_aux (new_cloned_index, acc) (ind, ind_clone, ind_list) =
          let new_clone = ind_clone () in
          let ind_list = (new_clone, Expr.index new_cloned_index) :: ind_list in
          (new_clone, (ind, ind_clone, ind_list) :: acc)
        in
        let base_new_index, new_gen =
          Index.fresh_clone_mark current_index "p" ()
        in
        let last_cloned, folded_list =
          List.fold_left fold_aux (base_new_index, []) indexes_list
        in
        let indexes_list =
          (current_index, new_gen, [ (base_new_index, Expr.zero) ])
          :: List.rev folded_list
        in
        {
          elem with
          next_index = new_next;
          bump_info = Some (base_new_index, last_cloned);
          indexes_list;
          current_index = next_index;
        }

  let is_closed { is_closed; _ } = is_closed

  let vectorize elem =
    assert (not elem.is_vectorized);
    {
      elem with
      incr = Expr.const Inst.A.vec_size;
      loop_scheme = V :: elem.loop_scheme;
      div_constraint = Div;
      indexes_list = [];
      is_vectorized = true;
    }

  let close
      ({ is_closed; dim; incr; bump_info; indexes_list; current_index; _ } as
      elem) =
    if is_closed then (
      Printf.printf "Dim %s is already closed\n" (Dim.show dim);
      assert false)
    else
      let clone = Index.clone current_index in
      let indexes_list = (current_index, clone, []) :: indexes_list in
      let last_tile_size = Some incr in
      let incr = Expr.size @@ Dim.size_id dim in
      (* TODO what is the right thing to do here ? *)
      let bump_info =
        match bump_info with
        | None -> Some (Index.from_dim dim, Index.from_dim dim)
        | _ -> bump_info
      in
      {
        elem with
        indexes_list;
        bump_info;
        last_tile_size;
        incr;
        is_closed = true;
      }

  (* Apply a function to incr *)
  let map_incr f elem = { elem with incr = f elem.incr }

  (* Apply a function to incr *)
  let set_par elem = { elem with par = true }
  let set_div_constraint div_constraint elem = { elem with div_constraint }

  let cur_bound_var { current_index; _ } =
    "N_" ^ Index.show_id_of_t current_index

  let next_bound_var { next_index; _ } = "N_" ^ Index.show_id_of_t next_index

  let default dim =
    let current_index = Index.from_dim dim in
    let index_clone = Index.clone current_index in
    let next_index = index_clone () in
    {
      dim;
      par = false;
      incr = Expr.one;
      div_constraint = Flexible 1;
      last_tile_size = None;
      current_index;
      next_index;
      loop_scheme = [];
      index_clone;
      bump_info = None;
      is_closed = false;
      is_vectorized = false;
      indexes_list = [];
    }
end
