open Arch
open Ids
open Exprs

module Concrete_types = struct
  type inst_type = Vec | Scal [@@deriving eq, show { with_path = false }]

  type t =
    | Nop
    (* Allocated is a placeholder for "factorized out": instructions that are
     * in some way "out of scope", we don't expect to generate a fresh name for
     * them. Still uneasy with that though *)
    | Allocated of inst_type * string
    | External_call of {
        name : string;
        tensors_info : (Tensor.t * Tensor.accesses) list;
        var_exprs : Expr.t list;
      }
    (* size that may vary *)
    (* void microk(float* C, float * A, float * B,
     * int strideI, int strideJ, int strideK)*)
    | Comment of string
    | Assign of t * inst_type * string
    | Read of Tensor.t * Tensor.accesses
    | Add of t * t
    | Sub of t * t
    | Mul of t * t
    | Write of t * Tensor.t * Tensor.accesses
    | Vread of Tensor.t * Tensor.accesses
    | Vadd of t * t
    | Vsub of t * t
    | Vmul of t * t
    | Vbcst of t
    | Vtranspose of
        Tensor.t
        * Tensor.accesses
        * Expr.t
        * Tensor.t
        * Tensor.accesses
        * Expr.t
    | Vwrite of t * Tensor.t * Tensor.accesses
  [@@deriving eq, show]
end

module type Inst_t = sig
  module A : Vec_arch_t
  include module type of Concrete_types

  val vectorize_on_dim : Dim.id -> t -> t
  val map_expr : Dim.id -> (Expr.t -> Expr.t) -> t -> t
  val access : Dim.id -> t -> bool
  val is_tensor_readonly : t list -> Tensor.t -> bool

  type mem_accesses_inf = (Tensor.t * Tensor.accesses) * (inst_type * string)

  val fact_loads_on_dims :
    Dim.id list ->
    mem_accesses_inf list ->
    t list ->
    mem_accesses_inf list * t list * t list

  val fact_stores_on_dims :
    Dim.id list ->
    mem_accesses_inf list ->
    t list ->
    mem_accesses_inf list * t list * t list

  val swap_tensor : Tensor.t -> Tensor.t -> t list -> t list

  val swap_accesses :
    (Tensor.t -> Tensor.accesses -> Tensor.accesses) -> t list -> t list

  val gen_code : string -> t list -> string list * string list * string
end
