open Ids
open Exprs
open Utils

module Args = struct
  let prefix = "T"
end

module T_id = Id (Args)

type id = T_id.t [@@deriving show, ord, eq]

(* A join dimension means that two distinct dimensions - meaning distinct loops
 * iterate on the same elements. Represents i + w for example *)
type t_dims = Single of Dim.t | Join of Dim.t * Dim.t * Dim.t * int option
[@@deriving eq, show]

let single dim = Single dim

let join_dims d1 d2 =
  let main_name = Dim.show_id_of_t d1 and aux_name = Dim.show_id_of_t d2 in
  let name = main_name ^ aux_name in
  let iter_dim, _ = Dim.fresh_gen name in
  Join (d1, d2, iter_dim (), None)

let join_dims_stride d1 d2 str =
  let main_name = Dim.show_id_of_t d1 and aux_name = Dim.show_id_of_t d2 in
  let name = main_name ^ aux_name in
  let iter_dim, _ = Dim.fresh_gen name in
  Join (d1, d2, iter_dim (), Some str)

type t = {
  id : id;
  dims : t_dims array;
  sizes : (t_dims * Expr.t) list;
  strides : (t_dims * Expr.t) list;
}
[@@deriving eq, show]

type gen = unit -> t
type accesses = (Dim.id * Expr.t) list [@@deriving eq, show]

let get_dim tensor dim =
  Array.find_opt
    (function
      | Single d when Dim.equal d dim -> true
      | Join (d1, d2, _, _) when Dim.equal dim d1 || Dim.equal dim d2 -> true
      | _ -> false)
    tensor.dims

let stride tensor dim =
  let open Option.Infix in
  get_dim tensor dim >>= fun dim ->
  List.assoc_eq equal_t_dims dim tensor.strides

let dim_size tensor dim = List.assoc_eq equal_t_dims (single dim) tensor.sizes

let size { sizes; _ } =
  let non_zero_sizes =
    List.filter (Bool.not % Expr.(equal zero)) (List.map snd sizes)
  in
  if List.is_empty non_zero_sizes then Expr.zero
  else List.fold_left Expr.mul Expr.one non_zero_sizes

let id { id; _ } = id
let show_tid { id; _ } = show_id id

let fresh_gen () : ?name:string -> unit -> id =
  let tensor_gen, _ = T_id.gen_id () in
  fun ?name ->
    match name with
    | Some name -> fun () -> T_id.set_prefix name (tensor_gen ())
    | None -> tensor_gen

let gen_clone ({ id; _ } as tensor) () =
  let f_id, gen_id = T_id.stack id () in
  ({ tensor with id = f_id }, fun () -> { tensor with id = gen_id () })

let t_dims { dims; _ } = dims
let t_dims_list { dims; _ } = Array.to_list dims

let dims_list { dims; _ } =
  Array.fold_right
    (fun dim l ->
      match dim with Single d -> d :: l | Join (d1, d2, _, _) -> d1 :: d2 :: l)
    dims []

let strides_in_order { strides; _ } = List.map snd strides

(* Should take dims from smaller stride to bigger
 * Return a list where first stride is the biggest and last is one *)
let strides_from_size dims dim_size_list =
  List.fold_left
    (fun (strides, current_stride) dim ->
      let dim_size = Option.default Expr.zero (List.assoc dim dim_size_list) in
      if Expr.equal Expr.zero dim_size then
        ((dim, current_stride) :: strides, current_stride)
      else
        let stride = Expr.(I.(current_stride * dim_size)) in
        ((dim, current_stride) :: strides, stride))
    ([], Expr.one) dims
  |> fst |> List.rev

(* Create a tensor with dims specified as an array of Dim,
 * with strides and sizes that can be optionally specified,
 * if sizes is not specified, we generate them directly from the dimensions.
 * if strides is not specified, we generate them from size list.
 * In that case, from (I, J, K, L) we generate (1, I, I * J, I * J * K) *)
let make ?name ?strides ?sizes dims =
  (* For now list are stored in reverse order so that last specified size get stride one *)
  let dims_list = Array.to_list dims in
  let sizes =
    match sizes with
    | None ->
        List.map (fun d -> (single d, Expr.size @@ Dim.size_id d)) dims_list
    | Some sizes ->
        List.map
          (fun dim ->
            ( single dim,
              Option.default (Expr.size @@ Dim.size_id dim)
              @@ List.assoc_eq Dim.equal dim sizes ))
          dims_list
  in
  let strides =
    match strides with
    | None -> strides_from_size (List.map single dims_list) sizes
    | Some strides -> List.map (map_fst single) strides
  in
  { id = fresh_gen () ?name (); dims = Array.map single dims; sizes; strides }

let make_join ?name ?strides ?sizes (dims : t_dims array) =
  let dims_list = Array.to_list dims in
  let sizes =
    let csize d = Expr.size @@ Dim.size_id d in
    match sizes with
    | None ->
        List.map
          (function
            | Single d as sd -> (sd, csize d)
            | Join (d1, d2, _, None) as jd ->
                (jd, Expr.(I.(csize d1 + csize d2 - const 1)))
            | Join (d1, d2, _, Some stride) as jd ->
                (jd, Expr.(I.((const stride * csize d1) + csize d2 - const 1))))
          dims_list
    | Some sizes ->
        List.map
          (function
            | Single d as sd ->
                (sd, Option.default (csize d) @@ List.assoc sd sizes)
            | Join (d1, d2, _, None) as jd ->
                ( jd,
                  Option.default Expr.(I.(csize d1 + csize d2 - const 1))
                  @@ List.assoc jd sizes )
            | Join (d1, d2, _, Some stride) as jd ->
                ( jd,
                  Option.default
                    Expr.(I.((const stride * csize d1) + csize d2 - const 1))
                  @@ List.assoc jd sizes ))
          dims_list
  in
  let strides =
    match strides with
    | None -> strides_from_size dims_list sizes
    | Some strides -> strides
  in
  { id = fresh_gen () ?name (); dims; sizes; strides }

let strides_from_size_list tens dim_size_list =
  let dims = t_dims_list tens in
  strides_from_size dims dim_size_list

let _validate_accesses tensor accesses =
  List.for_all (fun dim -> Array.mem dim tensor.dims) (List.map fst accesses)

let acc_does_access accesses dim =
  List.exists (fun (_, expr) -> Expr.access_dim_id expr dim) accesses

(* FIXME: accesses_list seems broken, for now we stay conservative *)
let tens_does_access tensor dim =
  Array.exists
    (let check_dim d = Dim.equal_id (Dim.id d) dim in
     function
     | Single d -> check_dim d
     | Join (d1, d2, _, _) -> check_dim d1 || check_dim d2)
    tensor.dims
(* && (List.assoc_opt dim accesses
   |> Option.map Expr.is_constant
   |> function Some b -> b
             | None -> false
   )
*)

let does_access tensor accesses dim =
  tens_does_access tensor dim || acc_does_access accesses dim

let sort_dim dim_list d1 d2 =
  if Dim.equal d1 d2 then 0
  else
    let rec sort_dim = function
      | [] -> failwith "None of d1 or d2 is present"
      | h :: _ when Dim.equal h d1 -> -1
      | h :: _ when Dim.equal h d2 -> 1
      | _ :: t -> sort_dim t
    in
    sort_dim dim_list

let get_main_dim = function Join (d, _, _, _) -> d | Single d -> d

let sort_tdim dim_list d1 d2 =
  let d1 = get_main_dim d1 and d2 = get_main_dim d2 in
  sort_dim dim_list d1 d2

(* Maybe useful someday *)
let reorder_dim dim_list size_list =
  let sort_dim (d1, _) (d2, _) =
    let d1, d2 = (get_main_dim d1, get_main_dim d2) in
    sort_dim dim_list d1 d2
  in
  List.sort sort_dim size_list

let reorder_layout tensor dim_list =
  let map_t_dim dim =
    match
      List.find
        (function Single d | Join (d, _, _, _) -> Dim.equal dim d)
        (t_dims_list tensor)
    with
    | Some t_dim -> t_dim
    | None ->
        failwith
        @@ Printf.sprintf "Dimension %s could not be found in tensor %s\n"
             (Dim.show_id_of_t dim) (show_tid tensor)
  in
  let strides = strides_from_size (List.map map_t_dim dim_list) tensor.sizes in
  let sorted_strides = reorder_dim dim_list strides in
  let sorted_sizes = reorder_dim dim_list tensor.sizes in
  let dims =
    Array.to_list (t_dims tensor)
    |> List.sort (sort_tdim dim_list)
    |> Array.of_list
  in
  { tensor with dims; strides = sorted_strides; sizes = sorted_sizes }

let modify_size_stride tensor size_list =
  let dims = t_dims_list tensor in
  let sizes =
    List.map
      (fun dim -> (dim, Option.default Expr.zero @@ List.assoc dim size_list))
      dims
  in
  let strides = strides_from_size_list tensor sizes in
  { tensor with strides; sizes }

let map_stride tensor f =
  let strides = List.map (fun (d, cexpr) -> (d, f cexpr)) tensor.strides in
  { tensor with strides }

let modify_stride tensor dim f =
  let strides =
    List.map
      (map_fst (function
        | Single d -> d
        | _ -> failwith "Does not support join for the moment"))
      tensor.strides
    |> List.modify_assoc Dim.equal f dim
    |> List.map (map_fst single)
  in
  { tensor with strides }

let modify_accesses accesses dim f =
  List.modify_assoc Dim.equal_id f dim accesses

let gen_access tensor accesses =
  let dim_stride_expr_pair =
    let get_expr d accesses = List.assoc_eq Dim.equal_id (Dim.id d) accesses in
    List.map
      (function
        | Single d, stride -> (Option.get @@ get_expr d accesses, stride)
        | Join (d1, d2, iter_dim, str), stride -> (
            let main = get_expr d1 accesses
            and aux = get_expr d2 accesses
            and iter = get_expr iter_dim accesses in
            let mul_stride e =
              Option.map_default (fun s -> Expr.(I.(const s * e))) e str
            in
            match (main, aux, iter) with
            | Some main, Some aux, None ->
                (Expr.add (mul_stride main) aux, stride)
            | None, None, Some iter -> (iter, stride)
            | Some _, _, Some _ | _, Some _, Some _ ->
                failwith
                  "Should access either main and aux or iter dimension but\n\
                  \            not both"
            | _ -> failwith "Missing accesses"))
      tensor.strides
  in
  let accesses_expr =
    List.fold_left
      (fun acc (acc_expr, stride) -> Expr.(I.((stride * acc_expr) + acc)))
      Expr.zero dim_stride_expr_pair
  in
  Format.sprintf "%s[%s]" (show_id tensor.id) (Expr.show accesses_expr)

let compare_dims_id tensor dim1 dim2 =
  let rec cmp = function
    | Single d :: _ when Dim.equal_id (Dim.id d) dim1 -> 1
    | Single d :: _ when Dim.equal_id (Dim.id d) dim2 -> -1
    | Join (d1, d2, _, _) :: _
      when (Dim.equal_id (Dim.id d1) dim1 && Dim.equal_id (Dim.id d2) dim2)
           || (Dim.equal_id (Dim.id d2) dim1 && Dim.equal_id (Dim.id d1) dim2)
      ->
        0
    | Join (d1, d2, _, _) :: _
      when Dim.equal_id (Dim.id d1) dim1 || Dim.equal_id (Dim.id d2) dim1 ->
        1
    | Join (d1, d2, _, _) :: _
      when Dim.equal_id (Dim.id d1) dim2 || Dim.equal_id (Dim.id d2) dim2 ->
        -1
    | _ :: t -> cmp t
    | [] -> failwith "Dim1 and dim2 are not present in tensor dims"
  in
  if Dim.equal_id dim1 dim2 then 0 else cmp @@ t_dims_list @@ tensor

let inner_dim tensor =
  (* TODO Does it make sense for us to have a zero-dimensional tensor ? *)
  t_dims_list tensor |> List.hd |> Option.get
