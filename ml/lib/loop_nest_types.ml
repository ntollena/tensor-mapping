open Ids
open Exprs
open Instruction

module type Loopnest_t = sig
  module Inst : Inst_t

  type loop =
    | Once of loop list
    | Statement of Inst.t list
    | Unroll of {
        comment : string list;
        dim_index_id : Index.t;
        start : Expr.t;
        size : int;
        increment : Expr.t;
        body : loop list;
      }
    | Loop of {
        comment : string list;
        pragma : string option;
        dim_index_id : Index.t;
        aux : (Index.t * Expr.t) list;
        start : Expr.t;
        halt : Expr.t;
        increment : Expr.t;
        body : loop list;
        vars_decl : (string * Expr.t) list;
      }
  [@@deriving show]

  val map_all_insts : (Inst.t list -> Inst.t list) -> loop -> loop
  val map_const_expr : (Expr.t -> Expr.t) -> loop -> loop

  module Zipper : sig
    type zipper =
      | Top
      | LoopChild of {
          parent : zipper;
          pragma : string option;
          aux : (Index.t * Expr.t) list;
          comment : string list;
          halt : Expr.t;
          dim_index_id : Index.t;
          start : Expr.t;
          increment : Expr.t;
          left : loop list;
          right : loop list;
          vars_decl : (string * Expr.t) list;
        }
      | UnrChild of {
          parent : zipper;
          comment : string list;
          dim_index_id : Index.t;
          start : Expr.t;
          size : int;
          increment : Expr.t;
          left : loop list;
          right : loop list;
        }
      | OnceChild of { parent : zipper; left : loop list; right : loop list }

    type t = zipper * loop [@@deriving show]

    val go_down : int -> t -> t option
    val go_up_with_path : t -> (zipper * loop * (t -> t)) option
    val go_up : t -> t option
    val set_top_comment : string list -> t -> t
    val to_tree : t -> loop
    val map_const_expr : (Expr.t -> Expr.t) -> t -> t
    val map_all_insts : (Inst.t list -> Inst.t list) -> t -> t
    val unroll_inst : Dim.id -> Expr.t -> int -> Expr.t -> t -> t
    val map_inst : (Inst.t list -> Inst.t list) -> t -> t
    val embed_before : loop -> t -> t
    val embed_before_at_top : loop -> t -> t
    val embed_after : loop -> t -> t
    val embed_after_at_top : loop -> t -> t

    val new_seq :
      ?comment:string list ->
      ?pragma:string ->
      ?aux:(Index.t * Expr.t) list ->
      ?vars_decl:(string * Expr.t) list ->
      Index.t ->
      Expr.t ->
      Expr.t ->
      Expr.t ->
      t ->
      t

    val new_unroll :
      ?comment:string list -> Index.t -> Expr.t -> int -> Expr.t -> t -> t
  end

  val show_alt : loop -> string
  val unroll : ?st:Expr.t -> int -> Dim.t -> Expr.t -> loop list -> loop

  val unroll_insts_dim :
    string list ->
    Dim.id ->
    Expr.t ->
    int ->
    Expr.t ->
    Inst.t list ->
    Inst.t list

  val loop :
    ?st:Expr.t ->
    ?halt:Expr.t ->
    ?incr:Expr.t ->
    ?pragma:string ->
    ?vars_decl:(string * Expr.t) list ->
    Dim.t ->
    loop list ->
    loop

  val set_aux : Index.t -> (Index.t * Expr.t) list -> loop -> loop
  val gen_code : loop -> string
end
