open Arch

module type Inst_t = Inst_sign.Inst_t

module I (A : Vec_arch_t) : Inst_t with module A = A
