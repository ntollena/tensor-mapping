open Ids
open Exprs
open Utils
module T = Tensor
open Arch

module type Inst_t = Inst_sign.Inst_t

module I (V : Vec_arch_t) : Inst_t = struct
  module A = V

  (* Definition for type inst  *)
  include Inst_sign.Concrete_types

  let string_of_inst_type = function Vec -> "vec" | Scal -> "scal"

  (* Allocated and Assign are supposed not to access any dimension, which is ... doubtful *)
  let rec access dim = function
    | Read (tensor, accesses) | Vread (tensor, accesses) ->
        T.does_access tensor accesses dim
    | Write (expr, tensor, accesses) | Vwrite (expr, tensor, accesses) ->
        access dim expr || T.does_access tensor accesses dim
    | External_call { tensors_info; _ } ->
        List.exists (fun (t, acc) -> T.does_access t acc dim) tensors_info
    | Add (e1, e2)
    | Mul (e1, e2)
    | Sub (e1, e2)
    | Vadd (e1, e2)
    | Vmul (e1, e2)
    | Vsub (e1, e2) ->
        access dim e1 || access dim e2
    | _ -> false

  let rec map_expr dim f inst =
    match inst with
    | Nop | Comment _ | Allocated _ | Assign _ | Vtranspose _ -> inst
    | External_call ({ tensors_info; _ } as arg) ->
        let tensors_info =
          List.map
            (fun (t, acc) ->
              (t, List.modify_assoc Dim.equal_id (Option.map f) dim acc))
            tensors_info
        in
        External_call { arg with tensors_info }
    | Read (s, access_list) ->
        Read (s, List.modify_assoc Dim.equal_id (Option.map f) dim access_list)
    | Write (value, tensor, access_list) ->
        Write
          ( map_expr dim f value,
            tensor,
            List.modify_assoc Dim.equal_id (Option.map f) dim access_list )
    | Add (v1, v2) -> Add (map_expr dim f v1, map_expr dim f v2)
    | Sub (v1, v2) -> Sub (map_expr dim f v1, map_expr dim f v2)
    | Mul (v1, v2) -> Mul (map_expr dim f v1, map_expr dim f v2)
    | Vread (s, access_list) ->
        Vread (s, List.modify_assoc Dim.equal_id (Option.map f) dim access_list)
    | Vwrite (value, tensor, access_list) ->
        Vwrite
          ( map_expr dim f value,
            tensor,
            List.modify_assoc Dim.equal_id (Option.map f) dim access_list )
    | Vadd (v1, v2) -> Vadd (map_expr dim f v1, map_expr dim f v2)
    | Vsub (v1, v2) -> Vsub (map_expr dim f v1, map_expr dim f v2)
    | Vmul (v1, v2) -> Vmul (map_expr dim f v1, map_expr dim f v2)
    | Vbcst value -> Vbcst (map_expr dim f value)

  let rec vectorize_on_dim dim =
    let open Either in
    (* Vectorize a pair of expression. If none of them has an access on the
     * vectorized dimension, we can just broadcast the result. Else we vectorize
     * both expression *)
    let vec_pair e1 e2 =
      match (access dim e1, access dim e2) with
      | false, false -> Left (e1, e2)
      | true, false ->
          let ve2 = Vbcst e2 and ve1 = vectorize_on_dim dim e1 in
          Right (ve1, ve2)
      | false, true ->
          let ve1 = Vbcst e1 and ve2 = vectorize_on_dim dim e2 in
          Right (ve1, ve2)
      | true, true ->
          let ve1 = vectorize_on_dim dim e1 and ve2 = vectorize_on_dim dim e2 in
          Right (ve1, ve2)
    in
    function
    | Read (tensor, accesses) ->
        if T.does_access tensor accesses dim then Vread (tensor, accesses)
        else Vbcst (Read (tensor, accesses))
    | Add (e1, e2) -> (
        match vec_pair e1 e2 with
        | Left (e1, e2) -> Vbcst (Add (e1, e2))
        | Right (e1, e2) -> Vadd (e1, e2))
    | Sub (e1, e2) -> (
        match vec_pair e1 e2 with
        | Left (e1, e2) -> Vbcst (Sub (e1, e2))
        | Right (e1, e2) -> Vsub (e1, e2))
    | Mul (e1, e2) -> (
        match vec_pair e1 e2 with
        | Left (e1, e2) -> Vbcst (Mul (e1, e2))
        | Right (e1, e2) -> Vmul (e1, e2))
    | Write (e, tensor, accesses) ->
        if T.does_access tensor accesses dim then
          Vwrite (vectorize_on_dim dim e, tensor, accesses)
        else Vbcst (Write (e, tensor, accesses))
    | Allocated (Scal, _) as e -> Vbcst e
    (* This catch-all pattern feels a bit uneasy... Supposed to catch only vectorized instructions *)
    | _ as e -> e

  let num_allocated_loads = ref 0
  let num_allocated_vloads = ref 0
  let num_allocated_stores = ref 0
  let num_allocated_vstores = ref 0

  let reiinit_all () =
    let reinit r = r := 0 in
    List.iter reinit
      [
        num_allocated_loads;
        num_allocated_vloads;
        num_allocated_stores;
        num_allocated_vstores;
      ]

  type mem_accesses_inf = (T.t * T.accesses) * (inst_type * string)

  (* Finds every access that is independent of all dimensions in dims, factorize them out
   * We have a problem : How to prevent naming interference. We have to keep some
   * track of how many variables have been allocated. No clear idea how to do that now *)
  let fact_loads_on_dims dims prev_accesses inst_list =
    let gen_name acc_type accesses loads value tensor expr =
      match
        List.assoc_eq [%eq: T.t * (Dim.id * Expr.t) list] (tensor, expr)
          accesses
      with
      | None -> (
          match
            List.assoc_eq [%eq: T.t * (Dim.id * Expr.t) list] (tensor, expr)
              prev_accesses
          with
          | None -> (
              match acc_type with
              | Scal ->
                  let num_load = Ref.post_incr num_allocated_loads in
                  let load_name = "mem_scal_" ^ string_of_int num_load in
                  ( ((tensor, expr), (Scal, load_name)) :: accesses,
                    Assign (value, Scal, load_name) :: loads,
                    Allocated (Scal, load_name) )
              | Vec ->
                  let num_load = Ref.post_incr num_allocated_vloads in
                  let load_name = "mem_vec_" ^ string_of_int num_load in
                  ( ((tensor, expr), (Vec, load_name)) :: accesses,
                    Assign (value, Vec, load_name) :: loads,
                    Allocated (Vec, load_name) ))
          | Some (_, var) ->
              ( ((tensor, expr), (acc_type, var)) :: accesses,
                Assign (value, acc_type, var) :: loads,
                Allocated (acc_type, var) ))
      | Some (_, var) -> (accesses, loads, Allocated (acc_type, var))
    in
    let rec recurse_inst (accesses_list, loads) = function
      | Read (tensor, accesses) as r
        when List.for_all
               (fun dim -> not (T.does_access tensor accesses dim))
               dims ->
          gen_name Scal accesses_list loads r tensor accesses
      | Vread (tensor, accesses) as r
        when List.for_all
               (fun dim -> not (T.does_access tensor accesses dim))
               dims ->
          gen_name Vec accesses_list loads r tensor accesses
      | (Nop | Allocated _ | External_call _ | Vtranspose _ | Comment _) as inst
        ->
          (accesses_list, loads, inst)
      | Assign (e, itype, ptr) ->
          let accesses_list, loads, e = recurse_inst (accesses_list, loads) e in
          (accesses_list, loads, Assign (e, itype, ptr))
      | Read _ as r -> (accesses_list, loads, r)
      | Vread _ as r -> (accesses_list, loads, r)
      | Add (e1, e2) ->
          let accesses_list, loads, e1 =
            recurse_inst (accesses_list, loads) e1
          in
          let accesses_list, loads, e2 =
            recurse_inst (accesses_list, loads) e2
          in
          (accesses_list, loads, Add (e1, e2))
      | Sub (e1, e2) ->
          let accesses_list, loads, e1 =
            recurse_inst (accesses_list, loads) e1
          in
          let accesses_list, loads, e2 =
            recurse_inst (accesses_list, loads) e2
          in
          (accesses_list, loads, Sub (e1, e2))
      | Mul (e1, e2) ->
          let accesses_list, loads, e1 =
            recurse_inst (accesses_list, loads) e1
          in
          let accesses_list, loads, e2 =
            recurse_inst (accesses_list, loads) e2
          in
          (accesses_list, loads, Mul (e1, e2))
      | Vadd (e1, e2) ->
          let accesses_list, loads, e1 =
            recurse_inst (accesses_list, loads) e1
          in
          let accesses_list, loads, e2 =
            recurse_inst (accesses_list, loads) e2
          in
          (accesses_list, loads, Vadd (e1, e2))
      | Vsub (e1, e2) ->
          let accesses_list, loads, e1 =
            recurse_inst (accesses_list, loads) e1
          in
          let accesses_list, loads, e2 =
            recurse_inst (accesses_list, loads) e2
          in
          (accesses_list, loads, Vsub (e1, e2))
      | Vmul (e1, e2) ->
          let accesses_list, loads, e1 =
            recurse_inst (accesses_list, loads) e1
          in
          let accesses_list, loads, e2 =
            recurse_inst (accesses_list, loads) e2
          in
          (accesses_list, loads, Vmul (e1, e2))
      | Vbcst e ->
          let accesses_list, loads, e = recurse_inst (accesses_list, loads) e in
          (accesses_list, loads, Vbcst e)
      | Vwrite (value, tensor, expr) ->
          let accesses_list, loads, e =
            recurse_inst (accesses_list, loads) value
          in
          (accesses_list, loads, Vwrite (e, tensor, expr))
      | Write (value, tensor, expr) ->
          let accesses_list, loads, e =
            recurse_inst (accesses_list, loads) value
          in
          (accesses_list, loads, Write (e, tensor, expr))
    in
    let factorize (accesses_list, loads, filtered) inst =
      let accesses_list, loads, new_inst =
        recurse_inst (accesses_list, loads) inst
      in
      (accesses_list, loads, new_inst :: filtered)
    in
    List.fold_left factorize ([], [], []) inst_list |> fun (acc, lds, insts) ->
    List.(rev acc, rev lds, rev insts)

  let is_tensor_readonly inst_list tensor =
    let does_modify_tens = function
      | External_call _ -> true
      | (Write (_, tens, _) | Vwrite (_, tens, _))
        when T.equal_id (T.id tens) (T.id tensor) ->
          true
      | _ -> false
    in
    List.for_all (Bool.not % does_modify_tens) inst_list

  (* Finds every write access that is independent of all dimensions in dim, factorize them out *)
  let fact_stores_on_dims dims prev_accesses inst_list =
    let gen_name acc_type accesses stores filtered value tensor expr =
      let find_tens_access tensor expr accesses =
        List.assoc_eq [%eq: T.t * (Dim.id * Expr.t) list] (tensor, expr)
          accesses
      in
      match find_tens_access tensor expr accesses with
      | None -> (
          match find_tens_access tensor expr prev_accesses with
          | None -> (
              match acc_type with
              | Scal ->
                  let num_stores = Ref.post_incr num_allocated_stores in
                  let store_name = "mem_scal_" ^ string_of_int num_stores in
                  let allocated = Allocated (Scal, store_name) in
                  ( ((tensor, expr), (Scal, store_name)) :: accesses,
                    Write (allocated, tensor, expr) :: stores,
                    Assign (value, Scal, store_name) :: filtered )
              | Vec ->
                  let num_stores = Ref.post_incr num_allocated_vstores in
                  let store_name = "mem_vec_" ^ string_of_int num_stores in
                  let allocated = Allocated (Vec, store_name) in
                  ( ((tensor, expr), (Vec, store_name)) :: accesses,
                    Vwrite (allocated, tensor, expr) :: stores,
                    Assign (value, Vec, store_name) :: filtered ))
          | Some (_, var) ->
              ( ((tensor, expr), (acc_type, var)) :: accesses,
                (let allocated = Allocated (acc_type, var) in
                 match acc_type with
                 | Scal -> Write (allocated, tensor, expr) :: stores
                 | Vec -> Vwrite (allocated, tensor, expr) :: stores),
                Assign (value, acc_type, var) :: filtered ))
      | Some (_, var) ->
          (accesses, stores, Assign (value, acc_type, var) :: filtered)
    in
    let factorize (accesses_list, stores, filtered) = function
      (* There is no need to call recursively factor_stores on
       * instructions because no write instructions must appear in this
       * place *)
      | Write (value, tensor, accesses)
        when List.for_all
               (fun dim -> not (T.does_access tensor accesses dim))
               dims ->
          let accesses, stores, filtered =
            gen_name Scal accesses_list stores filtered value tensor accesses
          in
          (accesses, stores, filtered)
      | Vwrite (value, tensor, accesses)
        when List.for_all
               (fun dim -> not (T.does_access tensor accesses dim))
               dims ->
          let accesses, stores, filtered =
            gen_name Vec accesses_list stores filtered value tensor accesses
          in
          (accesses, stores, filtered)
      | inst -> (accesses_list, stores, inst :: filtered)
    in
    List.fold_left factorize ([], [], []) inst_list |> fun (acc, lds, insts) ->
    List.(rev acc, rev lds, rev insts)

  let swap_tensor src_tensor dst_tensor inst_list =
    let rec swap_inst = function
      | Read (tensor, acc_expr) when T.equal tensor src_tensor ->
          Read (dst_tensor, acc_expr)
      | Vread (tensor, acc_expr) when T.equal tensor src_tensor ->
          Vread (dst_tensor, acc_expr)
      | Write (value, tensor, acc_expr) when T.equal tensor src_tensor ->
          let value = swap_inst value in
          Write (value, dst_tensor, acc_expr)
      | Vwrite (value, tensor, acc_expr) when T.equal tensor src_tensor ->
          let value = swap_inst value in
          Vwrite (value, dst_tensor, acc_expr)
      | Write (value, tensor, acc_expr) ->
          Write (swap_inst value, tensor, acc_expr)
      | Vwrite (value, tensor, acc_expr) ->
          Vwrite (swap_inst value, tensor, acc_expr)
      | Assign (value, vtype, tensor) -> Assign (swap_inst value, vtype, tensor)
      | Add (v1, v2) -> Add (swap_inst v1, swap_inst v2)
      | Sub (v1, v2) -> Sub (swap_inst v1, swap_inst v2)
      | Mul (v1, v2) -> Mul (swap_inst v1, swap_inst v2)
      | Vadd (v1, v2) -> Vadd (swap_inst v1, swap_inst v2)
      | Vsub (v1, v2) -> Vsub (swap_inst v1, swap_inst v2)
      | Vmul (v1, v2) -> Vmul (swap_inst v1, swap_inst v2)
      | Vbcst v -> Vbcst (swap_inst v)
      | External_call ({ tensors_info; _ } as args) ->
          let tensors_info =
            List.map
              (fun (t, acc) ->
                if T.equal t src_tensor then (dst_tensor, acc) else (t, acc))
              tensors_info
          in
          External_call { args with tensors_info }
      | inst -> inst
    in
    List.map swap_inst inst_list

  let swap_accesses f inst_list =
    let rec swap_inst = function
      | Read (tensor, acc_expr) ->
          let accesses = f tensor acc_expr in
          Read (tensor, accesses)
      | Vread (tensor, acc_expr) ->
          let accesses = f tensor acc_expr in
          Vread (tensor, accesses)
      | Write (value, tensor, acc_expr) ->
          let value = swap_inst value in
          let accesses = f tensor acc_expr in
          Write (value, tensor, accesses)
      | Vwrite (value, tensor, acc_expr) ->
          let value = swap_inst value in
          let accesses = f tensor acc_expr in
          Vwrite (value, tensor, accesses)
      | Assign (value, vtype, tensor) -> Assign (swap_inst value, vtype, tensor)
      | Add (v1, v2) -> Add (swap_inst v1, swap_inst v2)
      | Sub (v1, v2) -> Sub (swap_inst v1, swap_inst v2)
      | Mul (v1, v2) -> Mul (swap_inst v1, swap_inst v2)
      | Vadd (v1, v2) -> Vadd (swap_inst v1, swap_inst v2)
      | Vsub (v1, v2) -> Vsub (swap_inst v1, swap_inst v2)
      | Vmul (v1, v2) -> Vmul (swap_inst v1, swap_inst v2)
      | Vbcst v -> Vbcst (swap_inst v)
      | inst -> inst
    in
    List.map swap_inst inst_list

  type fresh = Fresh of string | Old of string

  let _rebuild_bin_fun = function
    | Add _ -> fun a b -> Add (a, b)
    | Sub _ -> fun a b -> Sub (a, b)
    | Mul _ -> fun a b -> Sub (a, b)
    | Vadd _ -> fun a b -> Vadd (a, b)
    | Vsub _ -> fun a b -> Vsub (a, b)
    | Vmul _ -> fun a b -> Vmul (a, b)
    | _ -> assert false

  (* Warning : terrible code ahead *)
  let gen_code tab value_list =
    (*TODO Is this correct to do that here ? *)
    let () = reiinit_all () in
    let forbidden_access = ref [] in
    let rec can_use_old_name = function
      | Vadd (a, b)
      | Vmul (a, b)
      | Vsub (a, b)
      | Add (a, b)
      | Mul (a, b)
      | Sub (a, b) ->
          can_use_old_name a && can_use_old_name b
      | Vwrite _ | Vtranspose _ | Write _ -> false
      | Comment _ | External_call _ | Allocated _ | Nop -> true
      | Vbcst e | Assign (e, _, _) -> can_use_old_name e
      | Vread (t, addr) | Read (t, addr) ->
          not
          @@ List.exists
               ([%eq: Tensor.t * Tensor.accesses] (t, addr))
               !forbidden_access
    in
    let gen_name inst_type state value =
      match value with
      | Allocated (_, name) -> Old name
      | _ -> (
          let id, name_list = !state in
          match List.assoc_eq equal value name_list with
          | Some ident -> Old ident
          | None ->
              let new_ident =
                Format.sprintf "%s_%d" (string_of_inst_type inst_type) id
              in
              state := (id + 1, (value, new_ident) :: name_list);
              Fresh new_ident)
    and scal_state = ref (0, [])
    and vec_state = ref (0, []) in
    let add_write_accesses t_addr =
      forbidden_access := t_addr :: !forbidden_access
    in
    let gen_vec_name = gen_name Vec scal_state
    and gen_scal_name = gen_name Scal vec_state in

    let rec get_code_var gen_name inst_type op =
      match (gen_name op, can_use_old_name op) with
      | Fresh var, _ | Old var, false -> (
          match inst_type with
          | Scal ->
              let var_list, vec_list, code = gen_inst op in
              (var :: var_list, vec_list, var, code)
          | Vec ->
              let var_list, vec_list, code = gen_inst op in
              (var_list, var :: vec_list, var, code))
      | Old var, true -> ([], [], var, "")
    and format_binop inst_type gen_name format_inst_string tab res op1 op2 =
      let f_a var_a =
        let var_list1, vec_list1, var1, code1 =
          get_code_var gen_name inst_type op1
        in
        let var_list2, vec_list2, var2, code2 =
          get_code_var gen_name inst_type op2
        in
        let var_list = List.concat [ var_list1; var_list2 ]
        and vec_list = List.concat [ vec_list1; vec_list2 ]
        and code = String.concat "\n" [ code1; code2 ] in
        let op = Printf.sprintf format_inst_string var1 var2 in
        (var_list, vec_list, Format.sprintf "%s\n%s%s = %s;" code tab var_a op)
      in
      match (gen_name res, can_use_old_name res) with
      | Old var_a, true -> f_a var_a
      | Old var_a, false | Fresh var_a, _ -> (
          let var_list, vec_list, code = f_a var_a in
          match inst_type with
          | Scal -> (var_a :: var_list, vec_list, code)
          | Vec -> (var_list, var_a :: vec_list, code))
    and format_3op inst_type gen_name format_inst_string tab res op1 op2 op3 =
      let f_a var_a =
        let var_list1, vec_list1, var1, code1 =
          get_code_var gen_name inst_type op1
        in
        let var_list2, vec_list2, var2, code2 =
          get_code_var gen_name inst_type op2
        in
        let var_list3, vec_list3, var3, code3 =
          get_code_var gen_name inst_type op3
        in
        let var_list = List.concat [ var_list1; var_list2; var_list3 ]
        and vec_list = List.concat [ vec_list1; vec_list2; vec_list3 ]
        and code = String.concat "\n" [ code1; code2; code3 ] in
        let op = Printf.sprintf format_inst_string var1 var2 var3 in
        (var_list, vec_list, Format.sprintf "%s\n%s%s = %s;" code tab var_a op)
      in

      match (gen_name res, can_use_old_name res) with
      | Old var_a, true -> f_a var_a
      | Old var_a, false | Fresh var_a, _ -> (
          let var_list, vec_list, code = f_a var_a in
          match inst_type with
          | Scal -> (var_a :: var_list, vec_list, code)
          | Vec -> (var_list, var_a :: vec_list, code))
    and gen_inst = function
      | Nop -> ([], [], Format.sprintf "%snop;" tab)
      | Comment c -> ([], [], Format.sprintf "%s// %s" tab c)
      | External_call { name; tensors_info; var_exprs } ->
          let strides =
            List.concat_map
              (fun (t, _) -> List.map [%show: Expr.t] @@ T.strides_in_order t)
              tensors_info
            |> String.concat ", "
          in
          let var_exprs =
            String.concat ", " @@ List.map [%show: Expr.t] var_exprs
          in
          let formatted_accesses =
            String.concat ", "
            @@ List.map (fun (t, acc) -> "&" ^ T.gen_access t acc) tensors_info
          in
          ( [],
            [],
            Format.sprintf "%s%s(%s, %s, %s);" tab name formatted_accesses
              var_exprs strides )
      | Allocated (Scal, name) -> ([ name ], [], Format.sprintf "%s%s;" tab name)
      | Allocated (Vec, name) -> ([], [ name ], Format.sprintf "%s%s;" tab name)
      | Vtranspose
          ( tensor_out,
            accesses_out,
            stride_out,
            tensor_in,
            accesses_in,
            stride_in ) ->
          let map2 f x y = (f x, f y) in
          let acc_out, acc_in =
            ( T.gen_access tensor_out accesses_out,
              T.gen_access tensor_in accesses_in )
          and str_out, str_in = map2 Expr.show stride_out stride_in in
          ( [],
            [],
            Format.sprintf "%s%s(&%s, %s, &%s, %s);" tab A.transpose_func
              acc_out str_out acc_in str_in )
      | Assign (Read (tensor, accesses), Scal, name) ->
          ( [ name ],
            [],
            Format.sprintf "%s%s = %s;" tab name (T.gen_access tensor accesses)
          )
      | Assign (value, Scal, name) -> (
          match gen_scal_name value with
          | Fresh var ->
              let scal_list, vec_list, code = gen_inst value in
              ( var :: name :: scal_list,
                vec_list,
                Format.sprintf "%s\n%s%s = %s;" code tab name var )
          | Old var -> ([ name ], [], Format.sprintf "%s%s = %s;" tab name var))
      | Read (tensor, accesses) as r -> (
          match gen_scal_name r with
          | Fresh name ->
              let read_str =
                Format.sprintf "%s = %s;" name (T.gen_access tensor accesses)
              in
              ([ name ], [], Format.sprintf "%s%s" tab read_str)
          | Old name ->
              let read_str =
                Format.sprintf "%s = %s;" name (T.gen_access tensor accesses)
              in
              ([], [], Format.sprintf "%s%s" tab read_str))
      | Write (value, tensor, accesses) -> (
          add_write_accesses (tensor, accesses);
          match (gen_scal_name value, can_use_old_name value) with
          | Fresh var, _ | Old var, false ->
              let scal_list, vec_list, code = gen_inst value in
              let wr_str =
                Format.sprintf "%s = %s;" (T.gen_access tensor accesses) var
              in
              ( var :: scal_list,
                vec_list,
                Format.sprintf "%s\n%s%s" code tab wr_str )
          | Old var, true ->
              let wr_str =
                Format.sprintf "%s = %s;" (T.gen_access tensor accesses) var
              in
              ([], [], Format.sprintf "%s%s" tab wr_str))
      | Add (v1, v2) as a ->
          format_binop Scal gen_scal_name "%s + %s" tab a v1 v2
      | Sub (v1, v2) as a ->
          format_binop Scal gen_scal_name "%s - %s" tab a v1 v2
      | Mul (v1, v2) as a ->
          format_binop Scal gen_scal_name "%s * %s" tab a v1 v2
      | Vbcst value as vb -> (
          match gen_vec_name vb with
          | Old var_a -> (
              match gen_scal_name value with
              | Fresh var1 ->
                  let scal_list, vec_list, code = gen_inst value in
                  let op = Format.sprintf V.gen_broadcast var1 in
                  ( var1 :: scal_list,
                    vec_list,
                    Format.sprintf "%s\n%s%s = %s;" code tab var_a op )
              | Old var1 ->
                  let op = Format.sprintf V.gen_broadcast var1 in
                  ([], [], Format.sprintf "%s%s = %s;" tab var_a op))
          | Fresh var_a -> (
              match gen_scal_name value with
              | Fresh var1 ->
                  let var_list, vec_list, code = gen_inst value in
                  let op = Format.sprintf V.gen_broadcast var1 in
                  ( var1 :: var_list,
                    var_a :: vec_list,
                    Format.sprintf "%s\n%s%s = %s;" code tab var_a op )
              | Old var1 ->
                  let op = Format.sprintf V.gen_broadcast var1 in
                  ([], [ var_a ], Format.sprintf "%s%s = %s;" tab var_a op)))
      | Vread (tensor, accesses) as r -> (
          match gen_vec_name r with
          | Fresh name ->
              let op =
                Format.sprintf V.gen_load (T.gen_access tensor accesses)
              in
              ([], [ name ], Format.sprintf "%s%s = %s;" tab name op)
          | Old name ->
              let op =
                Format.sprintf V.gen_load (T.gen_access tensor accesses)
              in
              ([], [], Format.sprintf "%s%s = %s;" tab name op))
      | Assign (Vread (tensor, accesses), Vec, name) ->
          let op = Format.sprintf V.gen_load (T.gen_access tensor accesses) in
          ([], [ name ], Format.sprintf "%s%s = %s;" tab name op)
      | Assign (value, Vec, name) -> (
          match gen_vec_name value with
          | Fresh var ->
              let scal_list, vec_list, code = gen_inst value in
              ( scal_list,
                var :: name :: vec_list,
                Format.sprintf "%s\n%s%s = %s;" code tab name var )
          | Old var -> ([], [ name ], Format.sprintf "%s%s = %s;" tab name var))
      | Vwrite (value, tensor, accesses) -> (
          add_write_accesses (tensor, accesses);
          match (gen_vec_name value, can_use_old_name value) with
          | Fresh var, _ | Old var, false ->
              let scal_list, vec_list, code = gen_inst value in
              let op =
                Format.sprintf V.gen_store (T.gen_access tensor accesses) var
              in
              ( scal_list,
                var :: vec_list,
                Format.sprintf "%s\n%s%s;" code tab op )
          | Old var, true ->
              let op =
                Format.sprintf V.gen_store (T.gen_access tensor accesses) var
              in
              ([], [], Format.sprintf "%s%s;" tab op))
      | (Vadd (Vmul (v1, v2), v3) | Vadd (v3, Vmul (v1, v2))) as a ->
          format_3op Vec gen_vec_name V.gen_fma tab a v1 v2 v3
      | Vadd (v1, v2) as a ->
          format_binop Vec gen_vec_name V.gen_add tab a v1 v2
      | Vsub (v1, v2) as s ->
          format_binop Vec gen_vec_name V.gen_sub tab s v1 v2
      | Vmul (v1, v2) as m ->
          format_binop Vec gen_vec_name V.gen_mul tab m v1 v2
    in
    let scal_insts, vec_insts, code =
      List.fold_left
        (fun (scal, vec, code) inst ->
          let scal', vec', code' = gen_inst inst in
          (List.append scal' scal, List.append vec' vec, code' :: code))
        ([], [], []) value_list
    in
    (List.rev scal_insts, List.rev vec_insts, String.concat "\n" (List.rev code))
end
