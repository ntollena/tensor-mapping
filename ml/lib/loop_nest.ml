open Ids
open Exprs
open Utils
open Instruction
include Loop_nest_types

module L (Inst : Inst_t) = struct
  type loop =
    | Once of loop list
    | Statement of Inst.t list
    | Unroll of {
        comment : string list;
        dim_index_id : Index.t;
        start : Expr.t;
        size : int;
        increment : Expr.t;
        body : loop list;
      }
    | Loop of {
        comment : string list;
        pragma : string option;
        dim_index_id : Index.t;
        aux : (Index.t * Expr.t) list;
        start : Expr.t;
        halt : Expr.t;
        increment : Expr.t;
        body : loop list;
        vars_decl : (string * Expr.t) list;
      }
  [@@deriving show]

  let rec show_alt = function
    | Once l ->
        "Once {\n" ^ String.concat "\n" (List.map show_alt l) ^ "} // Once\n"
    | Statement _ -> "Insts\n"
    | Unroll { dim_index_id; size; body; _ } ->
        Printf.sprintf "Unroll %s %d {\n%s\n} // Unroll\n"
          (Index.show_id_of_t dim_index_id)
          size
          (String.concat "\n" (List.map show_alt body))
    | Loop { dim_index_id; body; _ } ->
        Printf.sprintf "Loop %s {\n%s\n}\n"
          (Index.show_id_of_t dim_index_id)
          (String.concat "\n" (List.map show_alt body))

  let rec map_all_insts f = function
    | Once l -> Once (List.map (map_all_insts f) l)
    | Statement insts -> Statement (f insts) (* Do we need to map here ? *)
    | Unroll ({ start; increment; body; _ } as arg) ->
        let body = List.map (map_all_insts f) body in
        Unroll { arg with body; start; increment }
    | Loop ({ body; _ } as arg) ->
        let body = List.map (map_all_insts f) body in
        Loop { arg with body }

  let rec map_const_expr f = function
    | Once l -> Once (List.map (map_const_expr f) l)
    | Statement insts -> Statement insts (* Do we need to map here ? *)
    | Unroll ({ start; increment; body; _ } as arg) ->
        let body = List.map (map_const_expr f) body
        and start = f start
        and increment = f increment in
        Unroll { arg with body; start; increment }
    | Loop ({ start; halt; increment; body; _ } as arg) ->
        let body = List.map (map_const_expr f) body
        and start = f start
        and halt = f halt
        and increment = f increment in
        Loop { arg with body; halt; start; increment }

  (* Returns Some list of instructions if all subloops in loop are instructions
   * Returns None otherwise *)
  let to_instructions_list loop_list =
    let rec to_inst_lst acc = function
      | [] -> Some acc
      | Statement inst :: tail -> to_inst_lst (inst @ acc) tail
      | _ -> None
    in
    to_inst_lst [] loop_list

  let set_comment comment = function
    | Loop args -> Loop { args with comment }
    | Unroll args -> Unroll { args with comment }
    (* Other sorts of loops don't support comment yet *)
    | l -> l

  let unroll_insts_dim _comments dim start size increment insts =
    let rewrite_insts cnst inst =
      Inst.map_expr dim
        (Expr.alpha_replace_dim dim Expr.(I.(start + (const cnst * increment))))
        inst
    in
    List.init size Fun.id
    |> List.concat_map (fun cnst -> List.map (rewrite_insts cnst) insts)

  module Zipper = struct
    type zipper =
      | Top
      (*       |  OnceChild of loop list * int * loop list *)
      | LoopChild of {
          parent : zipper;
          pragma : string option;
          aux : (Index.t * Expr.t) list;
          comment : string list;
          halt : Expr.t;
          dim_index_id : Index.t;
          start : Expr.t;
          increment : Expr.t;
          left : loop list;
          right : loop list;
          vars_decl : (string * Expr.t) list;
        }
      | UnrChild of {
          parent : zipper;
          comment : string list;
          dim_index_id : Index.t;
          start : Expr.t;
          size : int;
          increment : Expr.t;
          left : loop list;
          right : loop list;
        }
      | OnceChild of { parent : zipper; left : loop list; right : loop list }
    [@@deriving show]

    type t = zipper * loop [@@deriving show]

    let go_down i = function
      | _, Statement _ -> None
      | ( z,
          Loop
            {
              pragma;
              aux;
              comment;
              halt;
              dim_index_id;
              start;
              increment;
              body;
              vars_decl;
            } ) -> (
          match List.takedrop i body with
          | left, elem :: right ->
              Some
                ( LoopChild
                    {
                      parent = z;
                      left;
                      right;
                      pragma;
                      aux;
                      comment;
                      halt;
                      dim_index_id;
                      start;
                      increment;
                      vars_decl;
                    },
                  elem )
          | _ -> None)
      | z, Unroll { size; comment; dim_index_id; start; increment; body } -> (
          match List.takedrop i body with
          | left, elem :: right ->
              Some
                ( UnrChild
                    {
                      parent = z;
                      left;
                      right;
                      comment;
                      size;
                      dim_index_id;
                      start;
                      increment;
                    },
                  elem )
          | _ -> None)
      | z, Once l -> (
          match List.takedrop i l with
          | left, elem :: right ->
              Some (OnceChild { parent = z; left; right }, elem)
          | _ -> None)

    let go_up_with_path = function
      | Top, _ -> None
      | OnceChild { parent; left; right }, t ->
          Some
            ( parent,
              Once (left @ [ t ] @ right),
              Option.get % go_down (List.length left) )
      | ( LoopChild
            {
              parent;
              left;
              aux;
              halt;
              right;
              comment;
              pragma;
              dim_index_id;
              start;
              increment;
              vars_decl;
            },
          t ) ->
          Some
            ( parent,
              Loop
                {
                  comment;
                  aux;
                  halt;
                  pragma;
                  dim_index_id;
                  start;
                  increment;
                  body = left @ [ t ] @ right;
                  vars_decl;
                },
              Option.get % go_down (List.length left) )
      | ( UnrChild
            {
              parent;
              left;
              size;
              right;
              comment;
              dim_index_id;
              start;
              increment;
            },
          t ) ->
          Some
            ( parent,
              Unroll
                {
                  comment;
                  size;
                  dim_index_id;
                  start;
                  increment;
                  body = left @ [ t ] @ right;
                },
              Option.get % go_down (List.length left) )

    let go_up = Option.map (fun (z, l, _) -> (z, l)) % go_up_with_path

    let set_top_comment comment =
      (* back is a "back path". It brings original argument back where it was *)
      let rec aux back (z, l) =
        match go_up_with_path (z, l) with
        | Some (z', l', b) -> aux (back %> b) (z', l')
        | None -> back (z, set_comment comment l)
      in
      aux Fun.id

    let map_all_insts f (z, l) =
      let rec loop = function
        | Top -> Top
        | LoopChild ({ parent; left; right; _ } as arg) ->
            let left = List.map (map_all_insts f) left
            and right = List.map (map_all_insts f) right
            and parent = loop parent in
            LoopChild { arg with left; right; parent }
        | UnrChild ({ parent; left; right; _ } as arg) ->
            let left = List.map (map_all_insts f) left
            and right = List.map (map_all_insts f) right
            and parent = loop parent in
            UnrChild { arg with left; right; parent }
        | OnceChild { parent; left; right } ->
            let left = List.map (map_all_insts f) left
            and right = List.map (map_all_insts f) right
            and parent = loop parent in
            OnceChild { left; right; parent }
      in
      (loop z, map_all_insts f l)

    let map_const_expr f (z, l) =
      let rec loop = function
        | Top -> Top
        | LoopChild
            ({ parent; start; halt; increment; left; right; vars_decl; _ } as
            arg) ->
            let left = List.map (map_const_expr f) left
            and right = List.map (map_const_expr f) right
            and vars_decl = List.map (Utils.map_snd f) vars_decl
            and start = f start
            and halt = f halt
            and increment = f increment
            and parent = loop parent in
            LoopChild
              {
                arg with
                left;
                right;
                start;
                halt;
                increment;
                parent;
                vars_decl;
              }
        | UnrChild ({ parent; start; increment; left; right; _ } as arg) ->
            let left = List.map (map_const_expr f) left
            and right = List.map (map_const_expr f) right
            and start = f start
            and increment = f increment
            and parent = loop parent in
            UnrChild { arg with left; right; start; increment; parent }
        | OnceChild { parent; left; right } ->
            let left = List.map (map_const_expr f) left
            and right = List.map (map_const_expr f) right
            and parent = loop parent in
            OnceChild { left; right; parent }
      in
      (loop z, map_const_expr f l)

    let to_tree =
      let rec loop (z, ln) =
        match go_up (z, ln) with Some (z', ln') -> loop (z', ln') | None -> ln
      in
      loop

    let map_inst f = function
      | z, Statement insts -> (z, Statement (f insts))
      | _ -> failwith "Not an instruction"

    let unroll_inst dim start size increment =
      let rewrite_insts inst cnst =
        Inst.map_expr dim
          (Expr.alpha_replace_dim dim
             Expr.(I.(start + (const cnst * increment))))
          inst
      in
      let unroll_inst inst =
        List.init size Fun.id |> List.map (rewrite_insts inst)
      in
      function
      | z, Statement stmts -> (z, Statement (List.concat_map unroll_inst stmts))
      | _ -> failwith "Cannot unroll on anything but a statement"

    let embed_before t = function
      | Top, Once t2 -> (Top, Once (t :: t2))
      | Top, (Statement _ as s) -> (Top, Once (t :: [ s ]))
      | Top, l -> (Top, Once (t :: [ l ]))
      | OnceChild { left; parent; right }, l ->
          (OnceChild { left = t :: left; parent; right }, l)
      | LoopChild ({ left; _ } as arg), l ->
          (LoopChild { arg with left = t :: left }, l)
      | UnrChild ({ left; _ } as arg), l ->
          (UnrChild { arg with left = t :: left }, l)

    let embed_before_at_top t (z, l) =
      let rec aux = function
        | Top -> OnceChild { parent = Top; left = [ t ]; right = [] }
        | LoopChild ({ parent; _ } as arg) ->
            LoopChild { arg with parent = aux parent }
        | UnrChild ({ parent; _ } as arg) ->
            UnrChild { arg with parent = aux parent }
        | OnceChild ({ parent; _ } as arg) ->
            OnceChild { arg with parent = aux parent }
      in
      (aux z, l)

    let embed_after t = function
      | Top, Once t2 -> (Top, Once (t2 @ [ t ]))
      | Top, l -> (Top, Once ([ l ] @ [ t ]))
      | OnceChild { left; parent; right }, l ->
          (OnceChild { left; parent; right = right @ [ t ] }, l)
      | LoopChild ({ right; _ } as arg), l ->
          (LoopChild { arg with right = right @ [ t ] }, l)
      | UnrChild ({ right; _ } as arg), l ->
          (UnrChild { arg with right = right @ [ t ] }, l)

    let embed_after_at_top t (z, l) =
      let rec aux = function
        | Top -> OnceChild { parent = Top; left = []; right = [ t ] }
        | LoopChild ({ parent; _ } as arg) ->
            LoopChild { arg with parent = aux parent }
        | UnrChild ({ parent; _ } as arg) ->
            UnrChild { arg with parent = aux parent }
        | OnceChild ({ parent; _ } as arg) ->
            OnceChild { arg with parent = aux parent }
      in
      (aux z, l)

    let new_seq ?(comment = []) ?pragma ?aux ?(vars_decl = []) dim_index_id
        start halt increment =
      let aux = match aux with Some l -> l | None -> [] in
      let rec wrap_up = function
        | Top ->
            LoopChild
              {
                parent = Top;
                dim_index_id;
                start;
                aux;
                pragma;
                vars_decl;
                halt;
                increment;
                comment;
                left = [];
                right = [];
              }
        | LoopChild ({ parent; _ } as arg) ->
            LoopChild { arg with parent = wrap_up parent }
        | UnrChild ({ parent; _ } as arg) ->
            UnrChild { arg with parent = wrap_up parent }
        | OnceChild ({ parent; _ } as arg) ->
            OnceChild { arg with parent = wrap_up parent }
      in
      fun (z, tree) -> (wrap_up z, tree)

    let new_unroll ?(comment = []) dim_index_id start size increment =
      let rec wrap_up = function
        | Top ->
            UnrChild
              {
                parent = Top;
                dim_index_id;
                start;
                size;
                increment;
                comment;
                left = [];
                right = [];
              }
        | LoopChild ({ parent; _ } as arg) ->
            LoopChild { arg with parent = wrap_up parent }
        | UnrChild ({ parent; _ } as arg) ->
            UnrChild { arg with parent = wrap_up parent }
        | OnceChild ({ parent; _ } as arg) ->
            OnceChild { arg with parent = wrap_up parent }
      in
      fun (z, tree) -> (wrap_up z, tree)
  end

  let unroll ?st:(start = Expr.Zero) size dim increment body =
    Unroll
      {
        comment = [];
        dim_index_id = Index.from_dim dim;
        start;
        size;
        increment;
        body;
      }

  let loop ?st:(start = Expr.Zero) ?halt ?(incr = Expr.One) ?pragma
      ?(vars_decl = []) dim body =
    let halt = Option.default (Expr.size (Dim.size_id dim)) halt in
    Loop
      {
        dim_index_id = Index.from_dim dim;
        aux = [];
        start;
        halt;
        vars_decl;
        increment = incr;
        body;
        pragma;
        comment = [];
      }

  let rec set_aux index aux = function
    | Loop ({ dim_index_id; _ } as arg) when Index.equal index dim_index_id ->
        Loop { arg with aux }
    | Loop ({ body; _ } as arg) ->
        Loop { arg with body = List.map (set_aux index aux) body }
    | Unroll ({ body; _ } as arg) ->
        Unroll { arg with body = List.map (set_aux index aux) body }
    | Statement stmts -> Statement stmts
    | Once body -> Once (List.map (set_aux index aux) body)

  let rec propagate_expr old_id expr = function
    | Statement stmt ->
        let f =
          Inst.map_expr (Index.dim old_id) (Expr.alpha_replace old_id expr)
        in
        Statement (List.map f stmt)
    | Unroll ({ start; body; _ } as l_args) ->
        let start = Expr.alpha_replace old_id expr start in
        let body = List.map (propagate_expr old_id expr) body in
        Unroll { l_args with start; body }
    | Loop ({ start; halt; vars_decl; increment; body; _ } as l_args) ->
        let map_3tuple f x y z = (f x, f y, f z) in
        let start, increment, halt =
          map_3tuple (Expr.alpha_replace old_id expr) start increment halt
        in
        let vars_decl =
          List.map (Utils.map_snd @@ Expr.alpha_replace old_id expr) vars_decl
        in
        let body = List.map (propagate_expr old_id expr) body in
        Loop { l_args with start; increment; body; halt; vars_decl }
    | Once loop -> Once (List.map (propagate_expr old_id expr) loop)

  let rec gen_loop_code tab loop_nest : string list * string list * string =
    (* This call is probably to be removed *)
    let gen_body body =
      match to_instructions_list body with
      | Some inst_list -> Inst.gen_code (tab ^ "\t") inst_list
      | None ->
          let scal_list, vec_list, code_list =
            List.split3 @@ List.map (gen_loop_code (tab ^ "\t")) body
          in
          ( List.concat scal_list,
            List.concat vec_list,
            String.concat "\n" code_list )
    in
    match loop_nest with
    | Statement inst -> Inst.gen_code tab inst
    | Once loops -> gen_body loops
    | Unroll { comment; dim_index_id; start; size; body; increment; _ } ->
        let comment =
          comment |> List.map (fun c -> tab ^ " // " ^ c) |> String.concat "\n"
          |> fun s -> if String.is_empty s then s else s ^ "\n"
        in
        (* List of numbers from 0 to loop_size - 1 *)
        List.init size Fun.id
        |> List.map (fun cnst_index ->
               List.map
                 (propagate_expr dim_index_id
                    Expr.(I.(start + (increment * const cnst_index))))
                 body)
        |> List.flatten |> gen_body
        |> fun (sl, vl, code) -> (sl, vl, comment ^ code)
    | Loop
        {
          comment;
          pragma;
          dim_index_id;
          aux;
          start;
          halt;
          increment;
          body;
          vars_decl;
          _;
        } ->
        let scal_list, vec_list, body_str = gen_body body in
        let index_str = Index.show_id (Index.id dim_index_id) in
        let start_str = Expr.show start in
        let halt_str = Expr.show @@ Expr.simplify halt in
        let increment_str = Expr.show @@ Expr.simplify increment in
        let aux_list =
          List.map
            (fun (index, start) ->
              Printf.sprintf "%s = %s"
                (Index.show_id (Index.id index))
                (Expr.show @@ Expr.simplify start))
            aux
        in
        let comment =
          comment |> List.map (fun c -> tab ^ "// " ^ c) |> String.concat "\n"
          |> fun s -> if String.is_empty s then s else s ^ "\n"
        in
        let vars_decls_str =
          List.map
            (fun (name, expr) ->
              Printf.sprintf "int %s = %s;" name (Expr.show expr))
            vars_decl
        in
        let var_decls =
          if List.is_empty vars_decl then ""
          else String.concat "\n" vars_decls_str
        in
        let pragma =
          Option.map_default (fun c -> tab ^ "#pragma " ^ c ^ "\n") "" pragma
        in
        let aux_start =
          match aux_list with
          | [] -> ""
          | _ -> ", " ^ String.concat ", " aux_list
        in
        let aux_list =
          List.map
            (fun (index, _) ->
              Printf.sprintf "%s += %s"
                (Index.show_id (Index.id index))
                (Expr.show @@ Expr.simplify increment))
            aux
        in
        let aux_incr =
          match aux_list with
          | [] -> ""
          | _ -> ", " ^ String.concat ", " aux_list
        in
        ( scal_list,
          vec_list,
          Format.sprintf
            "%s%s%sfor (%s = %s%s;\n\
             %s\t%s < %s;\n\
             %s\t%s += %s%s){\n\
             \t%s%s\n\
             %s\n\
             %s}"
            comment pragma tab index_str start_str aux_start tab index_str
            halt_str tab index_str increment_str aux_incr tab var_decls body_str
            tab )

  let gen_code loop_nest =
    (*     print_endline (show_alt loop_nest); *)
    let scal_list, vec_list, code = gen_loop_code "" loop_nest in
    let scal_decl =
      match scal_list with
      | [] -> ""
      | _ ->
          String.concat " ," (List.sort_unique String.compare scal_list)
          |> Printf.sprintf "%s %s;\n" Inst.A.base_type_name
    in
    let vec_decl =
      match vec_list with
      | [] -> ""
      | _ ->
          String.concat " ," (List.sort_unique String.compare vec_list)
          |> Printf.sprintf "%s %s;\n" Inst.A.vec_type_name
    in
    scal_decl ^ vec_decl ^ code
end
