open Utils
open Ids
open Exprs
open Loop_nest
module T = Tensor
open Instruction

module PI (Inst : Inst_t) (LN : Loopnest_t with module Inst := Inst) = struct
  module D = Dim_info.DI (Inst) (LN)
  module Zip = LN.Zipper
  module DimMap = D.DimMap

  exception DimNotVectorisable of string

  type snapshot = (Dim.id * D.Frozen.t) list

  let build_dims_snapshot tensor dim_map =
    let dims = T.dims_list tensor in
    List.map
      (fun dim -> (Dim.id dim, DimMap.find dim dim_map |> D.freeze dim))
      dims

  let fold_dims vectorize small_snapshot big_snapshot
      (indexes, glob_accesses, loc_accesses, loop_nest) =
    let open Tensor in
    function
    | Single d ->
        let d_id = Dim.id d in
        let small_frozen =
          Option.get @@ List.assoc_eq Dim.equal_id d_id small_snapshot
        and big_frozen =
          Option.get @@ List.assoc_eq Dim.equal_id d_id small_snapshot
        in
        let to_vectorize = vectorize d_id in
        let new_indexes, glob_index, local_index, enclose =
          D.Frozen.interval_loop_tile big_frozen small_frozen to_vectorize
        in
        ( List.rev_append new_indexes indexes,
          (d_id, glob_index) :: glob_accesses,
          (d_id, local_index) :: loc_accesses,
          enclose % loop_nest )
    (* TODO we may want to change something here - Or do we ? *)
    | Join (main_dim, aux_dim, iter_dim, _) ->
        let main_id = Dim.id main_dim and aux_id = Dim.id aux_dim in
        let get_frozen snapshot d =
          Option.get @@ List.assoc_eq Dim.equal_id d snapshot
        in
        let small_frozen_main = get_frozen small_snapshot main_id
        and big_frozen_main = get_frozen big_snapshot main_id
        and small_frozen_aux = get_frozen small_snapshot aux_id
        and big_frozen_aux = get_frozen big_snapshot aux_id in
        let new_indexes, new_glob_accesses, new_local_accesses, enclose =
          D.Frozen.interval_loop_tile_join iter_dim main_dim aux_dim
            big_frozen_main small_frozen_main big_frozen_aux small_frozen_aux
            false
        in
        ( List.rev_append new_indexes indexes,
          List.rev_append new_glob_accesses glob_accesses,
          List.rev_append new_local_accesses loc_accesses,
          enclose % loop_nest )

  let build_loop_base dims vectorize small_snapshot big_snapshot =
    List.fold_left
      (fold_dims vectorize small_snapshot big_snapshot)
      ([], [], [], Fun.id) dims

  let build_transpose_loop transpose_dims glob_inner_dim loc_inner_dim
      small_snapshot big_snapshot =
    let vectorize_dim id =
      Dim.equal_id (Dim.id glob_inner_dim) id
      || Dim.equal_id (Dim.id loc_inner_dim) id
    in
    build_loop_base transpose_dims vectorize_dim small_snapshot big_snapshot

  let build_loop dims inner_dim small_snapshot big_snapshot =
    let vectorize_dim id = Dim.equal_id (Dim.id inner_dim) id in
    build_loop_base dims vectorize_dim small_snapshot big_snapshot

  (* Tensor access body-loop generation *)
  let transpose_access src_inner_dim dst_inner_dim src_tensor dst_tensor
      src_accesses dst_accesses =
    let open Inst in
    let stride_src = Option.get @@ T.stride src_tensor dst_inner_dim
    and stride_dst = Option.get @@ T.stride dst_tensor src_inner_dim in
    [
      Vtranspose
        ( src_tensor,
          src_accesses,
          stride_src,
          dst_tensor,
          dst_accesses,
          stride_dst );
    ]

  let access to_vectorize src_tensor dst_tensor src_accesses dst_accesses =
    let open Inst in
    if to_vectorize then
      [ Vwrite (Vread (src_tensor, src_accesses), dst_tensor, dst_accesses) ]
    else [ Write (Read (src_tensor, src_accesses), dst_tensor, dst_accesses) ]

  type goal = Tensor.t -> D.t DimMap.t -> Zip.t -> Zip.t

  (* Build intermediate structure holding temporary packing information *)
  let build_pack local_dim_map tensor local_tensor is_readonly glob_dim_map
      glob_tensor ln =
    let load_comment =
      Printf.sprintf "Pack %s into %s" (T.show_tid tensor)
        (T.show_tid local_tensor)
    in
    let store_comment =
      Printf.sprintf "Unpack %s into %s" (T.show_tid local_tensor)
        (T.show_tid tensor)
    in
    (* is it ok to vectorize on a join dim ? *)
    let get_inner_dim =
      Tensor.inner_dim %> function Single d -> d | Join (d, _, _, _) -> d
    in
    let inner_dim = get_inner_dim tensor in
    let inner_dim_size =
      DimMap.find inner_dim local_dim_map |> function
      | Some dmap -> (
          match D.incr dmap with Expr.Const i -> i | _ -> raise Not_found)
      | None -> 1
    in
    let to_vectorize =
      inner_dim_size >= Inst.A.vec_size
      && inner_dim_size mod Inst.A.vec_size = 0
    in
    (* For the moment we only support packing with both inner dims vectorisable *)
    let _ =
      if not to_vectorize then
        let dim_name = Dim.show_id_of_t inner_dim in
        let error_msg =
          Printf.sprintf
            "Dimension '%s' of size %d (packed tensor) is not vectorizable \
             (vector size %d doesn't divide it)."
            dim_name inner_dim_size Inst.A.vec_size
        in
        let rule_msg =
          "Both inner dimensions of packed and global tensor should be \
           vectorisable."
        in
        raise (DimNotVectorisable (Printf.sprintf "%s %s" error_msg rule_msg))
      else ()
    in
    let small_snapshot = build_dims_snapshot tensor local_dim_map in
    let big_snapshot = build_dims_snapshot tensor glob_dim_map in
    let glob_inner_dim = get_inner_dim glob_tensor
    and loc_inner_dim = get_inner_dim local_tensor in

    let indexes, glob_accesses, loc_accesses, loop_enclose =
      if Dim.equal glob_inner_dim loc_inner_dim then
        build_loop
          (Tensor.t_dims_list local_tensor)
          glob_inner_dim small_snapshot big_snapshot
      else
        let dims = T.t_dims_list local_tensor in
        build_transpose_loop dims glob_inner_dim loc_inner_dim small_snapshot
          big_snapshot
    in
    let load_kernel =
      if Dim.equal glob_inner_dim loc_inner_dim then
        access to_vectorize glob_tensor local_tensor glob_accesses loc_accesses
      else
        transpose_access glob_inner_dim loc_inner_dim glob_tensor local_tensor
          glob_accesses loc_accesses
    in
    let store_kernel =
      if Dim.equal glob_inner_dim loc_inner_dim then
        access to_vectorize local_tensor glob_tensor loc_accesses glob_accesses
      else
        transpose_access loc_inner_dim glob_inner_dim local_tensor glob_tensor
          loc_accesses glob_accesses
    in
    let load_loop =
      loop_enclose (Zip.Top, LN.Statement load_kernel)
      |> Zip.set_top_comment [ load_comment ]
      |> Zip.to_tree
    and store_loop =
      loop_enclose (Zip.Top, LN.Statement store_kernel)
      |> Zip.set_top_comment [ store_comment ]
      |> Zip.to_tree
    in
    ( indexes,
      ln
      |> Zip.embed_before_at_top load_loop
      |> if is_readonly then Fun.id else Zip.embed_after_at_top store_loop )
end
