open Utils

module type Id_t = sig
  type t [@@deriving show, eq, ord]
  type gen = unit -> t
  val gen_id: unit -> gen * (unit -> unit)
  val set_prefix: string -> t -> t
  val map_prefix: (string -> string) -> t -> t
  val stack: t -> unit -> t * (unit -> t)
  val stack_m: t -> string -> unit -> t * (unit -> t)
  val clone: t -> unit -> t
end
module type Index_args = sig val prefix: string end

module Id(I: Index_args): Id_t = struct
  (* suffixes stored in reverse order *)
  type aux = Str of string | Int of int [@@deriving eq, ord]
  type t = {prefix: string; id_list: aux list ;
            clone: unit -> int }

  type inj = {inj_prefix: string; inj_id_list: aux list} [@@deriving  eq, ord]

  let inj {prefix; id_list;_} = {inj_prefix = prefix; inj_id_list= id_list}

  let equal a b = equal_inj (inj a) (inj b)

  let compare a b = compare_inj (inj a) (inj b)

  type gen = unit -> t

  let _suffix_to_string suffixes =
    let rec suffix_to_string = function
      | [] -> ""
      | h::t -> "_" ^ string_of_int h ^  suffix_to_string t
    in suffix_to_string (List.rev suffixes)

  let to_string = function
    | Str s -> s
    | Int i -> string_of_int i

  let show {prefix; id_list;_} =
    Format.sprintf "%s%s" prefix @@ String.concat "_" @@ List.map to_string @@ List.rev id_list

  let pp fmt id = Format.fprintf fmt "%s" (show id)

  let gen_id () =
    let i = ref 0 in
    (fun () -> {prefix = I.prefix;  id_list = [];
                clone = fun () -> let old_i = !i in i := old_i
                                                         +1;
                  old_i }),
    fun () -> i := 0

  let set_prefix prefix id =
    {id with prefix}

  let map_prefix f ({prefix;_} as id) =
    {id with prefix = f prefix}

  (* Stack a new level of scope *)
  let stack {prefix; id_list; _} () =
    let new_id = ref 1 in
    let clone = fun () -> Ref.post_incr new_id in
    {prefix; id_list= Int 0::id_list; clone},
    fun () -> let n_id = clone () in {prefix; id_list= Int n_id :: id_list; clone}

  (* Stack a new level of scope *)
  let stack_m {prefix; id_list; _} mark () =
    let new_id = ref 1 in
    let clone = fun () -> Ref.post_incr new_id in
    {prefix; id_list= Int 0::Str mark::id_list; clone},
    fun () -> let n_id = clone () in {prefix; id_list= Int n_id :: Str mark :: id_list; clone}

  let clone {prefix; id_list; clone}  () =
    match id_list with
    | [] -> let old_i = clone () in {prefix; id_list = [Int old_i]; clone}
    | _::tail -> let new_id = clone () in
      {prefix; id_list= Int new_id :: tail; clone}

end

module Size = struct
  module Args = struct
    let prefix = "I"
    let suffix = []
  end
  module Size_id = Id(Args)
  type id = Size_id.t  [@@deriving show, eq, ord]

  let gen : (unit -> id) * (unit -> unit) = Size_id.gen_id ()

  let fresh_gen : unit -> string option  -> (unit -> id) * (unit -> unit) =
    fun () -> 
    let size_gen, reset = Size_id.gen_id () in
    fun name ->
      match name with
        Some name -> (fun () -> Size_id.set_prefix name (size_gen ())), reset
      | None -> size_gen, reset

  let clone_id id () = (Size_id.clone id ())
end

module rec Index: sig
  type t [@@deriving show, eq, ord]
  type id [@@deriving show, eq, ord]
  type gen = unit -> t
  val show_id_of_t: t -> string
  val id: t -> id
  val dim: t -> Dim.id
  val from_dim: Dim.t -> t
  val clone: t -> unit -> t
  val map_prefix: t -> (string-> string) -> t
  val clone_map_prefix: t -> (string -> string) -> unit -> t
  val clone_id: id -> unit -> id
  val fresh_clone: t -> ?prefix:string -> unit -> t * gen
  val fresh_clone_map_prefix: t -> (string -> string) -> unit -> t * gen
  val fresh_clone_mark: t -> string -> unit -> t * gen
  val fresh_gen_id: string ->  (unit -> id) * (unit -> unit)
  val from_dim_id: Dim.id -> id -> t
end = struct
  module Args = struct
    let prefix = "i"
  end
  module Var_id = Id(Args)
  type id = Var_id.t [@@deriving eq, show, ord]
  type t = {id: id; dim: Dim.id} [@@deriving eq, show, ord]
  type gen = unit -> t
  let id {id; _} = id
  let dim {dim;_} = dim

  let show_id_of_t = show_id % id

  let clone {id; dim} =
    fun () -> {id = Var_id.clone id (); dim}

  let clone_id id = Var_id.clone id

  let map_prefix {id; dim} f =
    { id = Var_id.map_prefix f id; dim}

  let clone_map_prefix {id; dim} f () =
    {id = Var_id.clone id () |> Var_id.map_prefix f; dim}

  let fresh_gen_id name =
    let gen_id, reset = Var_id.gen_id () in
    (fun () -> Var_id.set_prefix name (gen_id ())), reset

  let fresh_clone {id; dim} ?prefix () =
    let first_clone, fresh_id = Var_id.stack id () in
    match prefix with
    | Some prefix -> {id = first_clone |> Var_id.set_prefix prefix;dim},
                      fun () -> {id = fresh_id () |> Var_id.set_prefix prefix; dim}
    | None -> {id = first_clone; dim},
      fun () ->  {id = fresh_id (); dim}

  let fresh_clone_map_prefix {id; dim} f () =
    let first_id, fresh_id = Var_id.stack id () in
    {id = Var_id.map_prefix f first_id ; dim},
    fun () -> {id = Var_id.map_prefix f (fresh_id ()); dim}

  let fresh_clone_mark {id; dim} mark () =
    let first_id, fresh_id = Var_id.stack_m id mark  () in
    {id = first_id ; dim},
    fun () -> {id = (fresh_id ()); dim}

  let from_dim_id dim_id id =
    {dim = dim_id; id}

  let from_dim dim =
    Dim.to_index dim
end

and Dim: sig
  type t [@@deriving show, eq, ord]
  type id [@@deriving show, eq, ord]
  val id: t -> id
  val show_id_of_t: t -> string
  val index_id: t -> Index.id
  val size_id: t -> Size.id
  val clone_index: t -> unit -> t
  val clone_size: t -> unit -> t
  val fresh_gen: string  -> (unit -> t) * (unit -> unit)
  val to_index: t -> Index.t
end = struct
  module Args = struct
    let prefix = "d"
  end
  module Dim_id = Id(Args)
  type id = Dim_id.t [@@deriving eq, show, ord]
  type t = {id: id; current_index: Index.id; size: Size.id} [@@deriving eq, show, ord]
  let id {id;_} = id
  let index_id {current_index;_} = current_index
  let size_id {size;_} = size

  let show_id_of_t = show_id % id
  let clone_index ({current_index;_} as dim) () =
    {dim with current_index = Index.clone_id current_index ()}

  let clone_size ({size;_} as dim) () =
    {dim with size = Size.clone_id size ()}

  let to_index dim =
    let current_index_id = Dim.index_id dim
    and dim_id = Dim.id dim in
    Index.from_dim_id dim_id current_index_id

  let fresh_gen: string -> (unit -> t) * (unit -> unit)  =
    fun name ->
    let fresh_dim_id, reset_dim_id = Dim_id.gen_id () in
      let fresh_index, reset_index = Index.fresh_gen_id name in
      let cap_name = String.capitalize name in
      let fresh_size_id, reset_size_id = Size.fresh_gen () (Some cap_name) in
      (fun () ->
         {id = fresh_dim_id () |> Dim_id.set_prefix name;
          current_index = fresh_index ();
          size = fresh_size_id () }),
      fun () -> reset_dim_id (); reset_size_id (); reset_index ()
end
