open Ids
open Exprs

type t [@@deriving eq, show]
type id [@@deriving eq, ord, show]

type t_dims = Single of Dim.t | Join of Dim.t * Dim.t * Dim.t * int option
[@@deriving eq, show]

type gen = unit -> t
type accesses = (Dim.id * Expr.t) list [@@deriving eq, show]

val single : Dim.t -> t_dims
val join_dims : Dim.t -> Dim.t -> t_dims
val join_dims_stride : Dim.t -> Dim.t -> int -> t_dims

val make :
  ?name:string ->
  ?strides:(Dim.t * Expr.t) list ->
  ?sizes:(Dim.t * Expr.t) list ->
  Dim.t array ->
  t

val make_join :
  ?name:string ->
  ?strides:(t_dims * Expr.t) list ->
  ?sizes:(t_dims * Expr.t) list ->
  t_dims array ->
  t

val t_dims : t -> t_dims array
val t_dims_list : t -> t_dims list
val dims_list : t -> Dim.t list
val id : t -> id
val show_tid : t -> string
val gen_clone : t -> unit -> t * (unit -> t)

val strides_from_size_list :
  t -> (t_dims * Expr.t) list -> (t_dims * Expr.t) list

val strides_in_order : t -> Expr.t list

(* val sort_by_stride: t -> t_dims list -> t_dims list *)
val size : t -> Expr.t
val reorder_layout : t -> Dim.t list -> t
val dim_size : t -> Dim.t -> Expr.t option
val stride : t -> Dim.t -> Expr.t option
val acc_does_access : accesses -> Dim.id -> bool
val tens_does_access : t -> Dim.id -> bool
val does_access : t -> accesses -> Dim.id -> bool
val modify_size_stride : t -> (t_dims * Expr.t) list -> t
val map_stride : t -> (Expr.t -> Expr.t) -> t
val modify_stride : t -> Dim.t -> (Expr.t option -> Expr.t option) -> t

val modify_accesses :
  accesses -> Dim.id -> (Expr.t option -> Expr.t option) -> accesses

val gen_access : t -> accesses -> string
val compare_dims_id : t -> Dim.id -> Dim.id -> int
val inner_dim : t -> t_dims
