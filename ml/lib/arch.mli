module type Vec_arch_t = sig
  val base_type_name : string
  val vec_size : int
  val vec_type_name : string
  val transpose_func : string

  val gen_load :
    (string -> string, unit, string, string, string, string) format6

  val gen_store :
    (string -> string -> string, unit, string, string, string, string) format6

  val gen_add :
    (string -> string -> string, unit, string, string, string, string) format6

  val gen_mul :
    (string -> string -> string, unit, string, string, string, string) format6

  val gen_sub :
    (string -> string -> string, unit, string, string, string, string) format6

  val gen_fma :
    ( string -> string -> string -> string,
      unit,
      string,
      string,
      string,
      string )
    format6

  val gen_broadcast :
    (string -> string, unit, string, string, string, string) format6
end

module SSE : Vec_arch_t
module AVX2 : Vec_arch_t
module AVX2_unaligned : Vec_arch_t
module AVX2_INT32 : Vec_arch_t
module AVX512 : Vec_arch_t
module AVX512_unaligned : Vec_arch_t
