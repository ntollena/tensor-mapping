open Utils

module type Algebra_t = sig
  type 'a base
  type 'a fix = Fix of 'a fix base

  val in_: 'a fix base -> 'a fix
  val out: 'a fix -> 'a fix base
  val map: ('a fix -> 'a fix) -> 'b fix -> 'a fix
  val cata: ('a base -> 'a) -> 'b fix -> 'a
  val ana: ('a -> 'a base) -> 'a -> 'b fix
  val hylo: ('a -> 'a base) -> ('b base -> 'b) ->'a -> 'b
  val para: (('a fix * 'b) base -> 'b) -> 'a fix -> 'b
  val para': ('a fix -> 'b base -> 'b) -> 'a fix -> 'b
  val apo: ('a -> ('b fix, 'a) either base) -> 'a -> 'b fix 

  type 'a attr
  val attribute: 'a attr -> 'a
  val hole: 'a attr -> 'a attr base
  val mk_attr: 'a -> 'a attr base -> 'a attr

  val histo: ('a attr base -> 'a) -> 'b fix  -> 'a
end

module type Mappable = sig type 'a t val map: ('a -> 'b) -> 'a t -> 'b t end

module A(M: Mappable): Algebra_t with type 'a base := 'a M.t
