open Utils
open Ids
open Exprs
module T = Tensor
open Kernels_sign

type iter = Iter of int [@@deriving show { with_path = false }]
type arg = Arg of int [@@deriving show { with_path = false }]

module Tensor_tile (A : Arch.Vec_arch_t) = struct
  module Inst = Instruction.I (A)
  module LN = Loop_nest.L (Inst)
  module Zip = LN.Zipper

  let show_arg_iter = [%show: (iter * arg) list]

  type ext_call_arg = {
    name : string;
    fixed_size : (Dim.t * int) list;
    var_dim : Dim.t;
    dynamic_sizes : (Dim.t * int) list;
    tensors_list : Tensor.t list;
    max_range : int;
  }
  [@@deriving show]

  let pp_ext_call_arg fmt { name; _ } = Format.fprintf fmt "<%s>" name

  type loop_type =
    | U of int * Dim.t
        [@printer
          fun fmt (unroll, dim) ->
            fprintf fmt "U (%d, %s)" unroll (Dim.show_id_of_t dim)]
    | V of Dim.t
        [@printer fun fmt dim -> fprintf fmt "V %s" (Dim.show_id_of_t dim)]
    | External_call of ext_call_arg
    (* Partial_Tile (d, size) does size iteration or less *)
    | Tile_partial of int * Dim.t
        [@printer
          fun fmt (size, dim) ->
            fprintf fmt "Tile_partial (%d, %s)" size (Dim.show_id_of_t dim)]
    (* generalized exact tile *)
    | Tile_gexact of int * Dim.t
        [@printer
          fun fmt (size, dim) ->
            fprintf fmt "Tile_gexact (%d, %s)" size (Dim.show_id_of_t dim)]
    | Tile_exact of
        int
        * Dim.t
          (* Tile_Exact (n, d) does exactly n
             iterations - no multiplication *)
        [@printer
          fun fmt (size, dim) ->
            fprintf fmt "Tile_exact (%d, %s)" size (Dim.show_id_of_t dim)]
    | ULambda of Dim.t
        [@printer
          fun fmt dim -> fprintf fmt "ULambda %s" (Dim.show_id_of_t dim)]
    | TLambda of Dim.t
        [@printer
          fun fmt dim -> fprintf fmt "TLambda %s" (Dim.show_id_of_t dim)]
    | Lambda_apply of Dim.t * (iter * arg) list
        [@printer
          fun fmt (dim, l) ->
            fprintf fmt "Lambda_apply %s %s" (Dim.show_id_of_t dim)
              (show_arg_iter l)]
    | T of int * Dim.t
        [@printer
          fun fmt (size, dim) ->
            fprintf fmt "T (%d, %s)" size (Dim.show_id_of_t dim)]
    | T_par of int * Dim.t
        [@printer
          fun fmt (size, dim) ->
            fprintf fmt "T_par (%d, %s)" size (Dim.show_id_of_t dim)]
    | Fused_T_pars of (int * Dim.t) list
        [@printer
          fun fmt list_s_d ->
            fprintf fmt "Fused_T_pars [%s]"
              (String.concat "; "
                 (List.map
                    (fun (size, dim) ->
                      Printf.sprintf "(%d, %s)" size (Dim.show_id_of_t dim))
                    list_s_d))]
    | Pack_tens of T.t
        [@printer fun fmt tens -> fprintf fmt "Pack %s" (T.show_tid tens)]
    | Pack_trans of T.t * Dim.t list
        [@printer
          fun fmt (tens, _) -> fprintf fmt "Pack/transpose %s" (T.show_tid tens)]
    | Hoist_vars of Dim.t list
        [@printer
          fun fmt dim_list ->
            fprintf fmt "Hoist_vars [%s]"
              (String.concat ";" @@ List.map Dim.show_id_of_t dim_list)]
    | R of Dim.t
        [@printer fun fmt dim -> fprintf fmt "R %s" (Dim.show_id_of_t dim)]
  [@@deriving show { with_path = false }]

  type tile_scheme = loop_type list

  let tile_to_string = [%show: loop_type list]

  module Payload = struct
    (* Module holding all necessary tensor information - for now just which clone to use *)
    module Te = struct
      type t = { tensor_clone : T.gen; tensor_list : (T.t * Expr.t) list }

      let get_new_tens_size new_tens size_list =
        let new_tensor = T.modify_size_stride new_tens size_list in
        let new_size = T.size new_tensor in
        (new_tensor, new_size)

      let make tensor size_list =
        let tens, tensor_clone = T.gen_clone tensor () in
        let tens, size = get_new_tens_size tens size_list in
        { tensor_clone; tensor_list = [ (tens, size) ] }

      let pack ({ tensor_clone; tensor_list; _ } as state) size_list () =
        let new_tens = tensor_clone () in
        let new_tensor, new_size = get_new_tens_size new_tens size_list in
        { state with tensor_list = (new_tensor, new_size) :: tensor_list }

      (* Fails if clone was not called before *)
      let current_tensor { tensor_list; _ } =
        List.hd tensor_list |> Option.get |> fst

      let page_size = 1024

      let decl_alloc_free_tens { tensor_list; _ } =
        tensor_list
        |> List.map (fun (t, s) ->
               ( Printf.sprintf "%s * %s = (%s *)ALLOC(%d, sizeof(%s) * %s);"
                   Inst.A.base_type_name
                   (T.show_id (T.id t))
                   Inst.A.base_type_name (*(Inst.A.vec_size * 4)*) page_size
                   Inst.A.base_type_name (Expr.show s),
                 Printf.sprintf "FREE(%s);" (T.show_id (T.id t)) ))
        |> List.split
        |> fun (allocs, frees) ->
        (String.concat "\n" allocs, String.concat "\n" frees)
    end

    (* Building Map modules needed to hold dimension and tensor infos *)
    module TidMap = Make_map (struct
      type t = T.id

      let compare = T.compare_id
    end)

    module D = Dim_info.DI (Inst) (LN)
    module DimMap = D.DimMap
    module PI = Packing.PI (Inst) (LN)

    type lnest =
      | Concrete of Index.t list * Zip.t
      | Lambda of Dim.t * (int -> lnest)
      | WaitPack of (Tensor.t * (D.t DimMap.t -> T.t -> lnest))
      | WaitTile of Dim.t * (Expr.t -> lnest)

    let rec compose_lnest f = function
      | Concrete (indexes, ln) -> Concrete (indexes, f ln)
      | WaitPack (tensor, thunk) ->
          WaitPack (tensor, fun t di -> compose_lnest f (thunk t di))
      | Lambda (dim, thunk) -> Lambda (dim, fun i -> compose_lnest f (thunk i))
      | WaitTile (dim, thunk) ->
          WaitTile (dim, fun e -> compose_lnest f (thunk e))

    let introduce_wait_tile dim f = function
      | Concrete (indexes, ln) ->
          WaitTile (dim, fun e -> Concrete (indexes, f e ln))
      | WaitPack (tensor, thunk) ->
          WaitTile
            ( dim,
              fun i ->
                WaitPack (tensor, fun t di -> compose_lnest (f i) (thunk t di))
            )
      | WaitTile (dim', thunk) ->
          WaitTile (dim, fun e -> WaitTile (dim', compose_lnest (f e) % thunk))
      | Lambda (dim', thunk) ->
          WaitTile (dim, fun e -> Lambda (dim', compose_lnest (f e) % thunk))

    let rec eliminate_wait_tile dim tile_size = function
      | Concrete _ as c -> c
      | WaitTile (dim', thunk) when Dim.equal dim dim' -> thunk tile_size
      | WaitTile (dim', thunk) ->
          WaitTile (dim', fun e -> eliminate_wait_tile dim tile_size @@ thunk e)
      | Lambda (dim', thunk) ->
          Lambda (dim', fun i -> eliminate_wait_tile dim tile_size @@ thunk i)
      | WaitPack (tensor, thunk) ->
          WaitPack
            (tensor, fun di t -> eliminate_wait_tile dim tile_size @@ thunk di t)

    let introduce_lambda dim f = function
      | Concrete (indexes, ln) ->
          Lambda (dim, fun i -> Concrete (indexes, f i ln))
      | WaitPack (tensor, thunk) ->
          Lambda
            ( dim,
              fun i ->
                WaitPack (tensor, fun t di -> compose_lnest (f i) (thunk t di))
            )
      | WaitTile (dim', thunk) ->
          Lambda (dim, fun i -> WaitTile (dim', compose_lnest (f i) % thunk))
      | Lambda (dim', thunk) ->
          Lambda (dim, fun i -> Lambda (dim', compose_lnest (f i) % thunk))

    let rec compose_pair f = function
      | Concrete (indexes, ln) ->
          let idx', ln' = f ln in
          Concrete (indexes @ idx', ln')
      | WaitPack (tensor, thunk) ->
          WaitPack (tensor, fun di t -> compose_pair f (thunk di t))
      | WaitTile (dim, thunk) ->
          WaitTile (dim, fun i -> compose_pair f (thunk i))
      | Lambda (dim, thunk) -> Lambda (dim, fun i -> compose_pair f (thunk i))

    let introduce_wait_pack tensor f = function
      | Concrete (indexes, ln) ->
          WaitPack
            ( tensor,
              fun di t ->
                let indexes', ln = f di t ln in
                Concrete (indexes @ indexes', ln) )
      | WaitPack (tensor', thunk) ->
          WaitPack
            ( tensor,
              fun di t ->
                WaitPack
                  (tensor', fun di' t' -> compose_pair (f di t) (thunk di' t'))
            )
      | Lambda (dim, thunk) ->
          WaitPack
            (tensor, fun di t -> Lambda (dim, compose_pair (f di t) % thunk))
      | WaitTile (dim, thunk) ->
          WaitPack
            (tensor, fun di t -> WaitTile (dim, compose_pair (f di t) % thunk))

    type t = {
      dmap : D.t DimMap.t;
      loop_nest : lnest;
      dim_list : Dim.t list;
      is_top : bool;
      kernel : Inst.t list;
      tmap : Te.t TidMap.t;
      vectorize_dim : Dim.t option;
    }

    let init dim_list inst =
      let statement = (Zip.Top, LN.Statement inst) in
      {
        dim_list;
        dmap = DimMap.empty;
        kernel = inst;
        tmap = TidMap.empty;
        is_top = true;
        loop_nest = Concrete ([], statement);
        vectorize_dim = None;
      }

    let finalize_loop_nest { dmap; loop_nest; _ } =
      let rec loop = function
        | Concrete (indexes, ln) -> (indexes, ln)
        | WaitTile (dim, _) ->
            failwith
            @@ Printf.sprintf "WaitTile %s was not applied"
                 (Dim.show_id_of_t dim)
        | Lambda (dim, _) ->
            failwith
            @@ Printf.sprintf "lambda %s was not applied" (Dim.show_id_of_t dim)
        | WaitPack (tensor, thunk) -> loop (thunk dmap tensor)
      in
      loop loop_nest

    let show_current_state dim_list { dmap; _ } =
      dim_list
      |> List.map (fun dim ->
             DimMap.find_opt dim dmap |> function
             | Some dinfo ->
                 let incr = D.incr dinfo and _next_index = D.next_index dinfo in
                 Printf.sprintf "%s = %s" (Dim.show_id_of_t dim)
                   (Expr.show incr)
             | None -> Printf.sprintf "%s = 1" (Dim.show_id_of_t dim))
      |> String.concat ", "

    let apply_dmap f payload =
      f payload.dmap |> fun dmap -> { payload with dmap }

    let find_dim dim payload = DimMap.find dim payload.dmap

    let modify_map_dim dim f payload =
      let dmap, res = DimMap.modify_map dim f payload.dmap in
      ({ payload with dmap }, res)

    (* Is p true for all (dim, dp) in payload *)
    let _for_all_dims p payload = DimMap.for_all p payload.dmap

    let apply_ln f ({ loop_nest; _ } as payload) =
      let loop_nest = f loop_nest in
      { payload with loop_nest }

    let modify_block f =
      apply_ln
        (compose_lnest
           LN.(
             function
             | z, Statement stmts -> (z, Statement (f stmts))
             | _ -> failwith "is not a block"))

    let external_call payload call_name var_dim max_range tensors_list dim_sizes
        dynamic_sizes =
      let payload, cur_bound_var =
        apply_dmap
          (fun p ->
            List.fold_left
              (fun p (d, s) ->
                DimMap.modify_opt d
                  (function
                    | Some _ ->
                        failwith
                          "External_call is only valid at innermost position"
                    | None ->
                        Some
                          (D.default d
                          |> D.map_incr (fun _ -> Expr.const s)
                          |> D.set_div_constraint Dim_info.Div))
                  p)
              p
              (dim_sizes @ dynamic_sizes))
          payload
        |> modify_map_dim var_dim (function
             | None ->
                 let dp =
                   D.default var_dim
                   |> D.map_incr (fun _ -> Expr.const max_range)
                   |> D.set_div_constraint (Dim_info.Flexible 1)
                 in
                 let cur_bound_var = D.cur_bound_var dp in
                 (Some dp, cur_bound_var)
             | Some _ ->
                 failwith "External_call is only valid at innermost position")
      in
      let dyn_sizes = List.map (fun (_, c) -> Expr.const c) dynamic_sizes in
      apply_ln
        (introduce_wait_tile var_dim (fun _ (z, _) ->
             let tensors_info =
               List.map
                 (fun t ->
                   ( t,
                     List.map (fun d ->
                         (Dim.id d, Expr.index @@ Index.from_dim d))
                     @@ Tensor.dims_list t ))
                 tensors_list
             in
             let var_expr = Expr.Var cur_bound_var in
             ( z,
               LN.Statement
                 [
                   Inst.External_call
                     {
                       name = call_name;
                       tensors_info;
                       var_exprs = var_expr :: dyn_sizes;
                     };
                 ] )))
        payload

    let vectorize_dim payload dim =
      payload
      |> apply_dmap
           (DimMap.modify_opt dim (function
             | Some _ -> failwith "Dim has already been defined"
             | None -> Some (D.vectorize @@ D.default dim)))

    let vectorize ({ is_top; _ } as payload) dim =
      assert is_top;
      let vect kernel = List.map (Inst.vectorize_on_dim @@ Dim.id dim) kernel in
      let payload = vectorize_dim payload dim in
      let payload = modify_block vect payload in
      payload

    let unroll ({ loop_nest; dim_list; _ } as payload) dim unroll_size =
      let unroll_incr =
        match unroll_size with
        | Some unroll_size -> Expr.const unroll_size
        | None ->
            Expr.PlaceHolder (Printf.sprintf "ph_%s" (Dim.show_id_of_t dim))
      in
      let payload, (cur_index, increment, next_incr) =
        modify_map_dim dim
          (function
            | Some elem ->
                let incr = D.incr elem in
                let next_incr = Expr.(I.(unroll_incr * incr)) in
                ( Some
                    (D.map_incr (fun _ -> next_incr) elem
                    |> D.set_div_constraint Div),
                  (D.current_index elem, incr, next_incr) )
            | None ->
                ( Some
                    (D.map_incr (Fun.const unroll_incr) (D.default dim)
                    |> D.set_div_constraint Div),
                  (Index.from_dim dim, Expr.one, unroll_incr) ))
          payload
      in
      let gen_unroll_loop usize loop =
        let start = Expr.index cur_index in
        let state_comment = show_current_state dim_list payload in
        let unr_comment =
          Printf.sprintf "U (%s, %d), (%s / %s)" (Dim.show_id_of_t dim) usize
            (Expr.show next_incr) (Expr.show increment)
        in
        let comment = [ state_comment; unr_comment ] in
        Zip.new_unroll ~comment cur_index start usize increment loop
      in
      let gen_kernel usize kernel =
        let glob_comm =
          Printf.sprintf "U (%s, %d), (%s / %s)" (Dim.show_id_of_t dim) usize
            (Expr.show next_incr) (Expr.show increment)
        in
        LN.unroll_insts_dim [ glob_comm ] (Dim.id dim) (Expr.index cur_index)
          usize increment kernel
      in
      let apply_unroll unroll_size =
        let open LN in
        function
        | Zip.Top, Statement insts ->
            (Zip.Top, Statement (gen_kernel unroll_size insts))
        | zln -> gen_unroll_loop unroll_size zln
      in
      let unroll_ln =
        match unroll_size with
        | Some unroll_size -> compose_lnest @@ apply_unroll unroll_size
        | None -> introduce_lambda dim apply_unroll
      in
      { payload with loop_nest = unroll_ln loop_nest }

    type ('a, 'b, 'c) ternary = One of 'a | Two of 'b | Three of 'c

    let lambda_apply ({ loop_nest; dmap; _ } as payload) dim iter_args_list =
      let cur_index, next_index, incr =
        match DimMap.find dim dmap with
        | Some dinfo ->
            let cur_index = D.current_index dinfo in
            let next_index = D.next_index dinfo in
            let incr = D.incr dinfo in
            (cur_index, next_index, incr)
        | None ->
            let dinfo = D.default dim in
            let cur_index = D.current_index dinfo in
            let next_index = D.next_index dinfo in
            (cur_index, next_index, Expr.one)
      in
      let placeholder_name = "ph_" ^ Dim.show_id_of_t dim in
      let rec apply = function
        | Concrete _ ->
            failwith
              (Printf.sprintf "Dim %s wasn't closured" (Dim.show_id_of_t dim))
        | Lambda (d, g) when Dim.equal d dim ->
            let rec handle_ln_list l =
              (* Collect results of applying lambda. Each of them should be of
               * the same variant *)
              List.fold_right
                (fun ln llist ->
                  match (llist, ln) with
                  | None, Concrete (indexes, ln) -> Some (One [ (indexes, ln) ])
                  | None, Lambda (d, g) -> Some (Two (d, [ g ]))
                  | None, WaitPack (t, h) -> Some (Three (t, [ h ]))
                  | Some (One l), Concrete (idx, ln) ->
                      Some (One ((idx, ln) :: l))
                  | Some (Two (d, l)), Lambda (d', f) when Dim.equal d d' ->
                      Some (Two (d, f :: l))
                  | Some (Three (t, l)), WaitPack (t', f) when T.equal t t' ->
                      Some (Three (t, f :: l))
                  | _ -> failwith "Incoherent state")
                l None
              |> function
              | None -> failwith "Empty list"
              | Some (One llist) ->
                  let idxs, ln =
                    Option.get
                    @@ List.reduce
                         (fun (idx, ln) (idx', g) ->
                           ( idx @ idx',
                             Zip.embed_after_at_top (Zip.to_tree ln) g ))
                         llist
                  in
                  Concrete (idxs, ln)
              | Some (Two (d, llist)) ->
                  Lambda
                    (d, fun i -> handle_ln_list @@ List.map (fun f -> f i) llist)
              | Some (Three (t, llist)) ->
                  WaitPack
                    ( t,
                      fun pt di ->
                        handle_ln_list @@ List.map (fun f -> f pt di) llist )
            in
            List.fold_left
              (fun (expr_start, l) (Iter i, Arg a) ->
                let uincr =
                  Expr.instanciate_placeholder placeholder_name a incr
                in
                let halt = Expr.(I.(expr_start + (const i * incr))) in
                let ln_a =
                  compose_lnest
                    (Zip.map_const_expr
                       (Expr.simplify
                       % Expr.instanciate_placeholder placeholder_name a)
                    % Zip.new_seq cur_index expr_start halt uincr)
                    (g a)
                in
                (Expr.(I.(expr_start + (const i * uincr))), ln_a :: l))
              (Expr.index next_index, [])
              iter_args_list
            |> snd |> handle_ln_list
        | Lambda (d, g) -> Lambda (d, fun i -> apply (g i))
        | WaitTile (d, g) -> WaitTile (d, fun i -> apply (g i))
        | WaitPack (t, thunk) -> WaitPack (t, fun t di -> apply (thunk t di))
      in
      let body_size =
        List.fold_left (fun s (Iter i, Arg a) -> s + (i * a)) 0 iter_args_list
      in
      let new_incr =
        Expr.(
          I.(const body_size * instanciate_placeholder placeholder_name 1 incr))
      in
      let dmap =
        DimMap.modify dim (D.bump_index % D.map_incr (Fun.const new_incr)) dmap
      in
      { payload with dmap; loop_nest = apply loop_nest }

    let hoist_var ({ loop_nest; _ } as payload) dim_list =
      let loop_nest =
        compose_lnest
          (function
            | z, LN.Statement kernel ->
                let accesses, loads, kernel =
                  Inst.fact_loads_on_dims (List.map Dim.id dim_list) [] kernel
                in
                let _, stores, kernel =
                  Inst.fact_stores_on_dims (List.map Dim.id dim_list) accesses
                    kernel
                in
                let loads = LN.Statement loads
                and stores = LN.Statement stores in
                (z, LN.Statement kernel)
                |> Zip.embed_before_at_top loads
                |> Zip.embed_after_at_top stores
            | _ -> failwith "Ambiguous kernel")
          loop_nest
      in
      { payload with loop_nest }

    (* This function is "generic" toward both inner and outer level
     * both its bound and its increment are flexible *)
    let tile_generalized_exact_partial dim_list payload to_outer to_inner dim
        tile_size =
      let ( payload,
            ( cur_index,
              next_index,
              cur_bound_name,
              next_bound_name,
              incr,
              next_incr ) ) =
        modify_map_dim dim
          (function
            | Some dpayload ->
                let div_const = D.div_constraint dpayload in
                let cur_index = D.current_index dpayload in
                let next_index = D.next_index dpayload in
                let incr = D.incr dpayload in
                let open Dim_info in
                let new_div_constraint =
                  match div_const with
                  | Div ->
                      (if not to_inner then
                       match incr with
                       | One -> ()
                       | Const c ->
                           if tile_size mod c <> 0 then
                             failwith
                             @@ Printf.sprintf
                                  "T_partial was called but dim %s is not \
                                   flexible"
                             @@ Dim.show_id_of_t dim
                       | _ ->
                           Printf.printf "Incr %s : %s\n" (Dim.show_id_of_t dim)
                           @@ [%show: Expr.t] incr;
                           failwith
                           @@ Printf.sprintf
                                "T_partial was called but dim %s is not \
                                 flexible"
                           @@ Dim.show_id_of_t dim);
                      Div
                  | Flexible factor ->
                      if to_inner then assert (tile_size mod factor = 0);
                      if to_outer then Flexible factor else Div
                in
                let next_incr = Expr.(Const tile_size) in
                let cur_bound_name = D.cur_bound_var dpayload in
                let next_bound_name = D.next_bound_var dpayload in
                ( Some
                    (D.(
                       bump_index
                       % map_incr (fun _ -> next_incr)
                       % set_div_constraint new_div_constraint)
                       dpayload),
                  ( cur_index,
                    next_index,
                    cur_bound_name,
                    next_bound_name,
                    incr,
                    next_incr ) )
            | None ->
                let () =
                  if (not to_outer) || to_inner then
                    failwith
                    @@ "Tile Exact cannot be the first  specifier on dimension "
                    ^ Dim.show_id_of_t dim
                in
                let dpayload = D.default dim in
                let cur_index = D.current_index dpayload in
                let next_index = D.next_index dpayload in
                let cur_bound_name = D.cur_bound_var dpayload in
                let next_bound_name = D.next_bound_var dpayload in
                let div_const = Dim_info.Flexible 1 in
                let incr = D.incr dpayload in
                let next_incr = Expr.(I.(const tile_size * incr)) in
                ( Some
                    (D.(
                       bump_index
                       % map_incr (fun _ -> next_incr)
                       % set_div_constraint div_const)
                       dpayload),
                  ( cur_index,
                    next_index,
                    cur_bound_name,
                    next_bound_name,
                    incr,
                    next_incr ) ))
          payload
      in
      let gen_loop tile_expr =
        let start = Expr.index next_index
        (*and halt = Expr.(Infix.(min (next_index --+ incr * (const tile_size))
         * (size (Dim.size_id dim)))) in*)
        and halt =
          let open Expr in
          let open I in
          if to_outer then index next_index + Var next_bound_name
          else index next_index + const tile_size
        in
        let glob_state = show_current_state dim_list payload in
        let loop_comment =
          match (to_outer, to_inner) with
          | true, false ->
              Printf.sprintf "T_partial (%s, %d) (%s / %s)"
                (Dim.show_id_of_t dim) tile_size (Expr.show next_incr)
                (Expr.show incr)
          | true, true ->
              Printf.sprintf "T_gexact (%s, %d) (%s / %s)"
                (Dim.show_id_of_t dim) tile_size (Expr.show next_incr)
                (Expr.show incr)
          | false, true ->
              Printf.sprintf "T_exact (%s, %d) (%s / %s)" (Dim.show_id_of_t dim)
                tile_size (Expr.show next_incr) (Expr.show incr)
          | false, false -> assert false
        in
        let comment = [ glob_state; loop_comment ] in
        let vars_decl =
          [
            ( cur_bound_name,
              Expr.(
                I.(Min (incr, tile_expr - (index cur_index - index next_index))))
            );
          ]
        in
        if to_inner then
          Zip.new_seq ~comment ~vars_decl cur_index start halt incr
        else Zip.new_seq ~comment cur_index start halt incr
      in
      let new_loop =
        (if to_outer then introduce_wait_tile dim gen_loop
        else compose_lnest (gen_loop (Const tile_size)))
        %
        if to_inner then eliminate_wait_tile dim (Var cur_bound_name)
        else eliminate_wait_tile dim (Const tile_size)
      in
      { payload with loop_nest = new_loop payload.loop_nest }

    (* TILING : could be factorized with tile_generalized_exact_partial *)
    let tile_on_dim ?(par = false) dim_list payload dim tile_size_opt =
      let tile_size =
        match tile_size_opt with
        | Some tile_size -> Expr.const tile_size
        | None ->
            Expr.PlaceHolder (Printf.sprintf "ph_%s" (Dim.show_id_of_t dim))
      in
      let payload, (cur_index, next_index, incr, next_incr) =
        let set_par_dim = if par then D.set_par else Fun.id in
        modify_map_dim dim
          (function
            | Some dpayload ->
                let cur_index = D.current_index dpayload in
                let next_index = D.next_index dpayload in
                let incr = D.incr dpayload in
                (* weird test, should find a proper semantic here *)
                let () =
                  assert (
                    D.div_constraint dpayload = Div
                    || D.div_constraint dpayload = Flexible 1)
                in
                let next_incr = Expr.(I.(tile_size * incr)) in
                ( Some
                    (D.(
                       set_par_dim % bump_index % map_incr (fun _ -> next_incr))
                       dpayload),
                  (cur_index, next_index, incr, next_incr) )
            | None ->
                let next_incr = tile_size in
                let dpayload =
                  D.default dim |> D.map_incr (Fun.const next_incr)
                in
                let cur_index = D.current_index dpayload in
                let next_index = D.next_index dpayload in
                ( Some D.(set_par_dim @@ bump_index dpayload),
                  (cur_index, next_index, Expr.one, next_incr) ))
          payload
      in

      let gen_loop tile_size =
        let start =
          if par then
            Expr.I.((incr * Expr.Var "thread_it_min") + Expr.index next_index)
          else Expr.index next_index
        and halt =
          if par then Expr.I.(incr * Expr.Var "thread_it_max")
          else Expr.(I.(index next_index + (incr * const tile_size)))
        in
        let glob_state = show_current_state dim_list payload in
        let suffix_par = if par then "_par" else "" in
        let loop_comment =
          Printf.sprintf "T%s (%s, %d) (%s / %s)" suffix_par
            (Dim.show_id_of_t dim) tile_size (Expr.show next_incr)
            (Expr.show incr)
        in
        let comment = [ glob_state; loop_comment ] in
        Zip.new_seq ~comment cur_index start halt incr
      in
      let new_loop =
        match tile_size_opt with
        (* WARNING : I don't know how this should interact with lambda - for now
         * it crashes *)
        | Some tile_size -> compose_lnest (gen_loop tile_size)
        | None -> introduce_lambda dim gen_loop
      in
      { payload with loop_nest = new_loop payload.loop_nest }

    (**inspired by tile_on_dim to merge all parallel loops declared inside Fused_T_pars *)
    let fuse_parallel_loops size_dim_list payload =
      (* regroup T_par on the same dimension to one bigger T_par *)
      let size_dim_list =
        let dims = List.map snd size_dim_list in
        let unique_dims = List.unique ~eq:Dim.equal dims in
        List.map
          (fun udim ->
            let sizes_udim =
              List.filter_map
                (function
                  | size, dim when Dim.equal dim udim -> Some size | _ -> None)
                size_dim_list
            in
            let total_size =
              List.fold (fun acc size -> acc * size) 1 sizes_udim
            in
            (total_size, udim))
          unique_dims
      in
      if List.length size_dim_list < 2 then
        failwith
          "Fused_T_pars should have at least 2 loops to fuse. Use T_par if you \
           only want to run parallelisation on 1 dimension.";
      let size_dim_list =
        List.map (fun (s, d) -> (Expr.const s, d)) size_dim_list
      in
      let payload, tmp_info =
        List.fold
          (fun (old_payload, tuples) (tile_size, dim) ->
            let new_payload, tuple =
              modify_map_dim dim
                (function
                  | Some dpayload ->
                      let cur_index = D.current_index dpayload in
                      let _next_index = D.next_index dpayload in
                      let incr = D.incr dpayload in
                      (* weird test, should find a proper semantic here *)
                      let () =
                        assert (
                          D.div_constraint dpayload = Div
                          || D.div_constraint dpayload = Flexible 1)
                      in
                      let next_incr = Expr.(I.(tile_size * incr)) in
                      ( Some
                          (D.(
                             D.set_par % bump_index
                             % map_incr (fun _ -> next_incr))
                             dpayload),
                        (cur_index, incr, tile_size) )
                  | None ->
                      let next_incr = tile_size in
                      let dpayload =
                        D.default dim |> D.map_incr (Fun.const next_incr)
                      in
                      let cur_index = D.current_index dpayload in
                      let _next_index = D.next_index dpayload in
                      ( Some D.(set_par @@ bump_index dpayload),
                        (cur_index, Expr.const 1, tile_size) ))
                old_payload
            in
            (* since payload is updated it is important to keep the updated version at each call of modify_map*)
            (new_payload, tuple :: tuples))
          (payload, []) size_dim_list
      in

      let comment =
        [
          Printf.sprintf "Fused_T_pars %s"
          @@ [%show: (Expr.t * string) list]
               (List.map (fun (s, d) -> (s, Dim.show_id_of_t d)) size_dim_list);
        ]
      in
      let gen_fake_dim, _ = Dim.fresh_gen "thread_it" in
      let cur_index =
        Index.from_dim (gen_fake_dim ())
        (* hack create new dimension to create our own index here for thread work sharing*)
      and start = Expr.Var "thread_it_min"
      and halt = Expr.Var "thread_it_max"
      and incr = Expr.Const 1 in

      (* need to be careful with the dimension sizes from inner to outter for the right choice of % and / to compute indices from thread iter*)
      (* example: Fused_T_pars (I, i) (J, j) (K, k) -> thread_it = i + I*j + IJ*k
         so we must do thread_it % I to find i, (thread_it / I) % J to find j, thread_it / (JJ) to find k*)
      let vars_decl =
        List.fold_lefti
          (fun acc i (index, incr, size) ->
            let product_previous_sizes =
              match acc with [] -> 1 | h :: _ -> fst h
            in
            let size_int, incr_int =
              match (size, incr) with
              | Expr.Const si, Expr.Const incr -> (si, incr)
              | _ -> failwith "Expr.const expected in tile_size"
            in
            let str =
              if i = 0 then
                Printf.sprintf "(thread_it %% %d) * %d" size_int incr_int
              else if i = List.length tmp_info - 1 then
                Printf.sprintf "(thread_it / %d) * %d" product_previous_sizes
                  incr_int
              else
                Printf.sprintf "((thread_it / %d) %% %d) * %d"
                  product_previous_sizes size_int incr_int
            in
            ( product_previous_sizes * size_int,
              (Index.show_id_of_t index, Expr.Var str) )
            :: acc)
          [] tmp_info
      in
      let vars_decl = List.map snd vars_decl in
      let new_loop =
        compose_lnest
          (Zip.new_seq ~comment ~vars_decl cur_index start halt incr)
      in
      { payload with loop_nest = new_loop payload.loop_nest }

    (* Same semantic as List.fold_left f init (DimMap.bindings payload.dmap) *)
    let fold_dims f init payload = DimMap.fold f payload.dmap init

    (* Get a list of every indexes used in loops generated so far *)
    let all_indexes payload =
      let dim_indexes =
        fold_dims
          (fun _ dp ind_list ->
            let aux =
              D.indexes_list dp |> List.map (fun (ind, _, l) -> (ind, l))
            in
            List.fold_left
              (fun l (ind, ind_l) ->
                ind :: List.rev_append (List.map fst ind_l) l)
              ind_list aux)
          [] payload
      in
      dim_indexes

    let fold_tens f init payload = TidMap.fold f payload.tmap init

    let check_vectorisation payload tensor =
      match payload.vectorize_dim with
      | Some dim ->
          if List.mem dim (T.dims_list tensor) then
            let inner_dim = T.inner_dim tensor in
            match inner_dim with
            | Single i_dim when Dim.equal dim i_dim -> ()
            | _ ->
                let s1 = Dim.show_id_of_t dim in
                let s2 = T.show_tid tensor in
                let s3 = [%show: T.t_dims] inner_dim in
                let error_msg =
                  Printf.sprintf
                    "Incorrect vectorisation of dim %s, whereas tensor %s \
                     inner dim is %s."
                    s1 s2 s3
                in
                failwith error_msg
          else ()
      | None -> ()

    let pack_tensor ({ dmap; loop_nest; _ } as payload) tensor size_list
        is_readonly transpose_opt accesses =
      let tmap, (tpayload, is_packed) =
        TidMap.modify_map (T.id tensor)
          (function
            | Some tpld ->
                let clone = Te.pack tpld size_list () in
                (Some clone, (Te.current_tensor clone, true))
            | None ->
                let def_tens_pld = Te.make tensor size_list in
                (Some def_tens_pld, (Te.current_tensor def_tens_pld, false)))
          payload.tmap
      in

      let payload, current_tensor = ({ payload with tmap }, tpayload) in
      let current_tensor =
        match transpose_opt with
        | Some transposition ->
            T.reorder_layout current_tensor (List.rev transposition)
        | None -> current_tensor
      in

      let _ =
        match TidMap.find (T.id current_tensor) payload.tmap with
        | None ->
            check_vectorisation payload current_tensor
            (* in case of same tensor packed twice only check for innermost packed array *)
        | _ -> ()
      in

      let rec apply_pack_to_last = function
        | Concrete _ as c -> c
        | Lambda (dim, f) -> Lambda (dim, fun i -> apply_pack_to_last (f i))
        | WaitTile (dim, f) -> WaitTile (dim, fun i -> apply_pack_to_last (f i))
        | WaitPack (tensor', thunk) when T.equal tensor tensor' ->
            thunk dmap current_tensor
        | WaitPack (tensor', thunk) ->
            WaitPack (tensor', fun t di -> apply_pack_to_last (thunk t di))
      in
      (* Swap A for A0/A1 in every inst expression *)
      let transform insts = Inst.swap_tensor tensor current_tensor insts in
      let map_access dim access_expr new_accesses_map =
        let access =
          Option.default (Fun.const Expr.zero)
            (List.assoc_eq Dim.equal_id dim new_accesses_map)
        in
        (dim, access access_expr)
      in
      let swap_new_tensor_access insts =
        Inst.swap_accesses
          (fun other_tensor old_accesses ->
            if T.equal_id (T.id other_tensor) (T.id current_tensor) then
              List.map
                (fun (d, old_expr) -> map_access d old_expr accesses)
                old_accesses
            else old_accesses)
          insts
      in
      let pack kernel =
        let kernel = transform kernel in
        if is_packed then kernel else swap_new_tensor_access kernel
      in
      let intro_pack =
        introduce_wait_pack tensor (fun gdi gt ->
            PI.build_pack dmap tensor current_tensor is_readonly gdi gt
            % Zip.map_all_insts pack)
      in
      { payload with loop_nest = intro_pack @@ apply_pack_to_last loop_nest }
  end

  type sym_size = Const_size of int | TimesVar of int

  (* Get size of tile on this particular dimension *)
  let dim_tile_size tile_scheme dim =
    let aux current_size = function
      | V d when Dim.equal d dim -> Const_size Inst.A.vec_size
      | External_call { fixed_size; max_range; dynamic_sizes; _ } -> (
          match List.assoc_opt dim fixed_size with
          | Some s -> Const_size s
          | None -> (
              match List.assoc_opt dim dynamic_sizes with
              | Some s -> Const_size s
              | None -> Const_size max_range))
      | (U (size, d) | T (size, d)) when Dim.equal d dim -> (
          match current_size with
          | Const_size c -> Const_size (c * size)
          | TimesVar c -> TimesVar (c * size))
      | Tile_exact (size, d) when Dim.equal d dim -> (
          match current_size with
          | Const_size _ -> Const_size size
          | TimesVar _ -> assert false)
      | ULambda d when Dim.equal d dim -> (
          match current_size with
          | Const_size c -> TimesVar c
          | TimesVar _ -> failwith "Multiple lambda on a single dimension")
      | Lambda_apply (d, liter_arg) when Dim.equal d dim -> (
          match current_size with
          | Const_size _ -> failwith "lambda apply called without lambda"
          | TimesVar c ->
              Const_size
                (List.fold_left
                   (fun acc (Iter i, Arg a) -> acc + (i * a))
                   0 liter_arg
                * c))
      | _ -> current_size
    in
    List.fold_left aux (Const_size 1) tile_scheme |> function
    | Const_size c -> c
    | _ -> failwith @@ "Lambda without apply on dim " ^ Dim.show_id_of_t dim

  (* Monstruous function - This thing is an absolute mess *)
  let handle_pack payload tensor transpose_opt =
    let module P = Payload in
    let t_dims = T.t_dims_list tensor in
    let dims = T.dims_list tensor in
    (* Retrieve current status of relevant dimensions - dimensions that appear in the tensor *)
    let dims_payload_opt =
      List.map (fun dim -> (dim, P.(find_dim dim payload))) dims
    in
    (* Filtered dims payloads - that is, only dimensions that concretely
     * appears in a loop nest around the kernel appear there *)
    let dims_payload =
      List.filter_map
        (fun (d, opt) -> Option.map (fun pl -> (d, pl)) opt)
        dims_payload_opt
    in
    (* List of sizes for new tensor dimensions *)
    let size_list =
      List.map
        (let open Tensor in
        function
        | Single dim as sd ->
            let d_opt = P.find_dim dim payload in
            let incr = Option.map_default P.D.incr Expr.zero d_opt in
            (sd, incr)
        | Join (d1, d2, _, stride) as jd -> (
            let d1_opt = P.find_dim d1 payload in
            let d2_opt = P.find_dim d2 payload in
            let mul_stride e =
              Option.map_default (fun s -> Expr.(I.(const s * e))) e stride
            in
            match (Option.map P.D.incr d1_opt, Option.map P.D.incr d2_opt) with
            | ( Some inc1,
                (Some (Expr.SizeVar _ as size) | Some (Expr.Const _ as size)) )
              ->
                (* TODO TEST TEST TEST *)
                (jd, Expr.add (mul_stride inc1) size)
            | Some _, Some _ ->
                (* TODO clean that, this is dirty *)
                failwith "Tiling on small dim !!!"
            | None, Some incr -> (jd, incr)
            | Some incr, None -> (jd, incr)
            | None, None -> (jd, Expr.zero)))
        t_dims
    in
    let current_pack_indexes_opt =
      List.map (fun (d, dp) -> (d, P.D.current_pack dp)) dims_payload
    in
    let accesses =
      List.map
        (function
          | d, Some index ->
              ( Dim.id d,
                fun expr ->
                  Expr.alpha_replace (Index.from_dim d) (Expr.index index) expr
              )
          | d, None ->
              ( Dim.id d,
                fun expr -> Expr.alpha_replace (Index.from_dim d) Expr.zero expr
              ))
        current_pack_indexes_opt
    in
    (* TODO: find a cleaner way to do that ?*)
    let is_readonly = Inst.is_tensor_readonly payload.kernel tensor in
    P.pack_tensor payload tensor size_list is_readonly transpose_opt accesses

  (** check that:
      * - all tiles after T_par or Fused_T_pars are on red dims
      * - no red dimension is on T_par or Fused_T_pars
    *)
  let check_parallel leftover_tiles red_dims =
    if List.is_empty leftover_tiles then ()
    else
      let is_red_dim dim = List.mem dim red_dims in
      let error_msg_red_dim op dim =
        let s_rd = [%show: Dim.id] (Dim.id dim) in
        Printf.sprintf "Red dims like %s are not allowed in %s." s_rd op
      in
      let to_check = List.drop 1 leftover_tiles in
      let tile_par = List.first leftover_tiles in
      let _ =
        match tile_par with
        | T_par (_, dim) when is_red_dim dim ->
            failwith (error_msg_red_dim "T_par" dim)
        | Fused_T_pars list ->
            let dim_list = List.map snd list in
            let opt_red_dim = List.find is_red_dim dim_list in
            let res =
              match opt_red_dim with
              | Some dim -> failwith (error_msg_red_dim "Fused_T_pars" dim)
              | None -> ()
            in
            res
        | _ -> ()
      in
      List.iter
        (fun tile ->
          match tile with
          | T (_, dim) when is_red_dim dim -> ()
          | _ ->
              let s = [%show: loop_type] tile in
              let s2 = [%show: loop_type] tile_par in
              let error_msg =
                Printf.sprintf
                  "%s encountered after %s, only T(_, dim) on red dims are \
                   allowed."
                  s s2
              in
              failwith error_msg)
        to_check

  (** Generate a parallel loop merged from all the T_par loops *)
  let gen_parallel_utils n_iters fused =
    if n_iters = 1 then ("", "", "")
    else
      let thread_id = "tid" in
      let thread_it = "thread_it" in
      let n_threads = "n_threads" in
      let ind_min = "thread_it_min" in
      let ind_max = "thread_it_max" in
      let base_job = "base_job" in
      let residual_job = "residual" in
      let header =
        [
          "#pragma omp parallel";
          "{\n" (* important to block the entire following code *);
        ]
      in
      let declarations =
        [
          (thread_id, "omp_get_thread_num()");
          (n_threads, "omp_get_num_threads()");
          (base_job, Printf.sprintf "%d / %s" n_iters n_threads);
          (residual_job, Printf.sprintf "%d %% %s" n_iters n_threads);
          (ind_min, "0");
          (ind_max, "0");
        ]
      in
      let declarations =
        if fused then (thread_it, "0") :: declarations else declarations
      in
      let declarations =
        List.map
          (fun (name, value) -> Printf.sprintf "int %s = %s;" name value)
          declarations
      in
      let balance_threads =
        [
          Printf.sprintf "if (%s < %s) {" thread_id residual_job;
          Printf.sprintf "\t%s = %s * (%s + 1);" ind_min thread_id base_job;
          Printf.sprintf "\t%s = %s + (%s + 1);" ind_max ind_min base_job;
          "} else {";
          Printf.sprintf "\t%s = %s * (%s + 1) + (%s - %s) * %s;" ind_min
            residual_job base_job thread_id residual_job base_job;
          Printf.sprintf "\t%s = %s + %s;" ind_max ind_min base_job;
          "}\n";
        ]
      in
      let footer = "} // pragma omp parallel\n" in
      let header = String.concat "\n" header in
      let declarations =
        String.concat "\n"
          (List.map (String.concat "\n") [ declarations; balance_threads ])
      in
      (header, declarations, footer)

  let gen_loop ?(const_dim_sizes = []) dim_list red_dim_list inst loop_tiles =
    let module P = Payload in
    let sanitize final_payload =
      (* catch info to future correctness checking for vectorization dimension *)
      let tensors_list =
        let open Inst in
        match inst with
        | Write (Add (_, Mul (Read (a, _), Read (b, _))), c, _) -> [ a; b; c ]
        | _ -> []
      in
      let transposed_tensors =
        List.filter_map
          (fun loop_t ->
            match loop_t with
            | Pack_trans (tensor, _) -> Some tensor
            | _ -> None)
          loop_tiles
      in
      let to_check_vec_tensors =
        List.filter
          (fun tensor -> not (List.mem tensor transposed_tensors))
          tensors_list
      in
      List.iter
        (fun tensor -> P.check_vectorisation final_payload tensor)
        to_check_vec_tensors;
      (* end of the correctness checking on vectorization *)
      List.iter
        (fun d ->
          match P.find_dim d final_payload with
          | Some dim_p -> (
              match
                ( P.D.is_closed dim_p,
                  P.D.incr dim_p,
                  List.assoc_eq Dim.equal d const_dim_sizes )
              with
              | true, _, _ -> ()
              (* TODO : to_int_opt is supposed to take a size_id * size list,
               * we give it an empty list. This should be enough but is a bit dirty *)
              | false, inc, Some dim_size
                when Expr.to_int_opt [] inc = Some dim_size ->
                  ()
              | false, _, None ->
                  failwith
                    (Printf.sprintf
                       "Dim %s is not closed and no dim size was given"
                       (Dim.show_id_of_t d))
              | false, Expr.Const inc, Some dim_size ->
                  failwith
                    (Printf.sprintf
                       "Dim %s is not closed and current tile size is %d while \
                        given size is %d"
                       (Dim.show_id_of_t d) inc dim_size)
              | is_closed, incr, expected_incr ->
                  failwith
                    (Printf.sprintf
                       "Inconsistent state with Dim %s: closed %b, incr : %s, \
                        expected incr : %s"
                       (Dim.show_id_of_t d) is_closed (Expr.show incr)
                       ([%show: int option] expected_incr)))
          | None -> (
              match List.assoc_eq Dim.equal d const_dim_sizes with
              | Some size when size = 1 -> ()
              | Some size ->
                  failwith
                  @@ Printf.sprintf
                       "Dim %s does not appear in tile scheme but was expected \
                        of size %d"
                       (Dim.show_id_of_t d) size
              | None ->
                  failwith
                  @@ Printf.sprintf "Dim %s does not appear in tile scheme "
                       (Dim.show_id_of_t d)))
        dim_list
    in
    let finalize final_payload =
      sanitize final_payload;
      let init_unclosed_dims =
        List.fold_left
          (fun buf d ->
            match P.find_dim d final_payload with
            | Some dp ->
                if P.D.is_closed dp then buf
                else
                  let last_index = P.D.current_index dp in
                  let init =
                    Printf.sprintf "int %s = 0;\n"
                    @@ Index.show_id_of_t last_index
                  in
                  buf ^ init
            | None ->
                let init =
                  Printf.sprintf "int %s = 0;\n"
                  @@ Index.show_id_of_t @@ Index.from_dim d
                in
                buf ^ init)
          "" dim_list
      in
      let tensor_decl, tensor_free =
        P.fold_tens
          (fun _ tp (list_decl, list_free) ->
            let decl, free = P.Te.decl_alloc_free_tens tp in
            (decl :: list_decl, free :: list_free))
          ([], []) final_payload
      in
      let tensor_decl =
        Printf.sprintf "%s\n" (String.concat "\n" tensor_decl)
      in
      let tensor_free =
        Printf.sprintf "\n%s\n" (String.concat "\n" tensor_free)
      in
      let guards =
        List.fold_left
          (fun buf d ->
            match P.find_dim d final_payload with
            | Some dp -> (
                let dim_size = P.D.dim dp |> Dim.size_id in
                let last_incr =
                  Option.default Expr.zero (P.D.last_tile_size dp)
                in
                let div_guard =
                  if Expr.equal Expr.zero last_incr then ""
                  else
                    Printf.sprintf " && (%s %% %s == 0)" (Size.show_id dim_size)
                      (Expr.show last_incr)
                in
                match List.assoc_eq Dim.equal d const_dim_sizes with
                | Some size ->
                    let dim_size_s = Size.show_id dim_size in
                    Printf.sprintf "%sassert((%s == %d)%s);\n" buf dim_size_s
                      size div_guard
                | None ->
                    Printf.sprintf "%sassert((%s <= %s)%s);\n" buf
                      (Expr.show last_incr) (Size.show_id dim_size) div_guard)
            | None -> (
                match List.assoc_eq Dim.equal d const_dim_sizes with
                | Some _ ->
                    let dim_size = Dim.size_id d in
                    let dim_size_s = Size.show_id dim_size in
                    Printf.sprintf "%sassert((%s == 1));\n" buf dim_size_s
                | None -> failwith "What ?"))
          "" dim_list
      in
      (tensor_decl, tensor_free, guards, init_unclosed_dims)
    in

    (* We are going to have an helper payload structure  that
     * specifies the index to use, the increment, etc.*)
    let gen_loop_fold payload = function
      | External_call
          { fixed_size; var_dim; max_range; tensors_list; name; dynamic_sizes }
        ->
          P.external_call payload name var_dim max_range tensors_list fixed_size
            dynamic_sizes
      | Lambda_apply (dim, iter_args) -> P.lambda_apply payload dim iter_args
      | V dim ->
          let payload = { payload with vectorize_dim = Some dim } in
          P.vectorize payload dim
      | U (unroll_size, dim) -> P.unroll payload dim (Some unroll_size)
      | ULambda dim -> P.unroll payload dim None
      | T (tile_size, dim) ->
          P.tile_on_dim dim_list payload dim (Some tile_size)
      | T_par (tile_size, dim) ->
          P.tile_on_dim ~par:true dim_list payload dim (Some tile_size)
      | Fused_T_pars size_dim_list ->
          P.fuse_parallel_loops size_dim_list payload
      | TLambda dim -> P.tile_on_dim dim_list payload dim None
      | Hoist_vars dim_list -> P.hoist_var payload dim_list
      | Pack_trans (tensor, transpose_list) ->
          handle_pack payload tensor (Some transpose_list)
      | Pack_tens tensor -> handle_pack payload tensor None
      | Tile_partial (size, dim) ->
          P.tile_generalized_exact_partial dim_list payload true false dim size
      | Tile_gexact (n_iter, dim) ->
          P.tile_generalized_exact_partial dim_list payload true true dim n_iter
      | Tile_exact (n_iter, dim) ->
          P.tile_generalized_exact_partial dim_list payload false true dim
            n_iter
      | R dim ->
          let payload, (cur_index, div_const, boundvar, incr) =
            P.modify_map_dim dim
              (function
                | Some dp ->
                    ( Some (P.D.close dp),
                      ( P.D.current_index dp,
                        P.D.div_constraint dp,
                        P.D.cur_bound_var dp,
                        P.D.incr dp ) )
                | None ->
                    ( Some (P.D.close @@ P.D.default dim),
                      (Index.from_dim dim, Dim_info.Div, "", Expr.one) ))
              payload
          in
          let size_dim = Expr.size (Dim.size_id dim) in
          let start = Expr.zero and halt = size_dim in
          let loop_comment =
            Printf.sprintf "R %s (%s / %s)" (Dim.show_id_of_t dim)
              (Expr.show size_dim) (Expr.show incr)
          and state = Payload.show_current_state dim_list payload in
          let comment = [ state; loop_comment ] in
          let new_loop =
            let open Dim_info in
            match div_const with
            | Div -> Zip.new_seq ~comment cur_index start halt incr
            | Flexible _ ->
                let vars_decl =
                  [
                    ( boundvar,
                      Expr.(
                        I.(min incr (size (Dim.size_id dim) - index cur_index)))
                    );
                  ]
                in
                Zip.new_seq ~comment ~vars_decl cur_index start halt incr
          in
          {
            payload with
            loop_nest =
              P.eliminate_wait_tile dim (Expr.SizeVar (Dim.size_id dim))
              @@ P.compose_lnest new_loop payload.loop_nest;
          }
    in

    (*End aux function *)
    let payload = P.init dim_list [ inst ] in
    let payload = List.fold_left gen_loop_fold payload loop_tiles in
    let _par_dims =
      P.fold_dims (fun d dp l -> if P.D.par dp then d :: l else l) [] payload
    in

    let tiles_with_parallel =
      List.drop_while
        (fun tile ->
          match tile with T_par _ | Fused_T_pars _ -> false | _ -> true)
        loop_tiles
    in
    check_parallel tiles_with_parallel red_dim_list;
    let n_par_iters =
      List.fold
        (fun acc tile ->
          acc
          *
          match tile with
          | T_par (x, _) -> x
          | Fused_T_pars list -> List.fold (fun prod (i, _) -> prod * i) 1 list
          | _ -> 1)
        1 tiles_with_parallel
    in
    let is_fused =
      Option.is_some
        (List.find
           (function Fused_T_pars _ -> true | _ -> false)
           tiles_with_parallel)
    in
    let par_header, par_decl, par_footer =
      gen_parallel_utils n_par_iters is_fused
    in
    let tensor_decl, tensor_free, guards, init_unclosed_dims =
      finalize payload
    in
    let indexes, loop_nest = P.finalize_loop_nest payload in
    let list_indexes =
      indexes @ P.all_indexes payload |> List.sort_unique Index.compare
    in
    let indexes_decl =
      String.concat ", " @@ List.map (Index.show_id % Index.id) list_indexes
      |> Printf.sprintf "int %s;\n"
    in
    let final_loop =
      P.fold_dims
        (fun _ dp loop_nest ->
          let aux =
            P.D.indexes_list dp |> List.map (fun (ind, _, l) -> (ind, l))
          in
          List.fold_left (fun ln (ind, l) -> LN.set_aux ind l ln) loop_nest aux)
        (Zip.to_tree loop_nest) payload
    in
    ( Printf.sprintf "/*\n%s\n*/\n" ([%show: loop_type list] loop_tiles),
      indexes_decl,
      tensor_decl,
      init_unclosed_dims,
      tensor_free,
      guards,
      final_loop,
      par_header,
      par_decl,
      par_footer )

  module Conv_build (Conv_args : Conv_args_t) = struct
    include Conv_args

    (*
     * Output[i,j,f] += Input[i + w, j + h, c] * Params[w, h, c, f]
     * *)
    let loop_body =
      let open Inst in
      let from_dim dim = (Dim.id dim, Expr.index @@ Index.from_dim dim) in
      let read_out =
        Read (output, [ from_dim x_dim; from_dim y_dim; from_dim f_dim ])
      and read_1 =
        Read
          ( input,
            [
              from_dim x_dim;
              from_dim w_dim;
              from_dim y_dim;
              from_dim h_dim;
              from_dim c_dim;
            ] )
      and read_2 =
        Read
          ( params,
            [ from_dim c_dim; from_dim f_dim; from_dim h_dim; from_dim w_dim ]
          )
      in
      let contract = Add (read_out, Mul (read_1, read_2)) in
      Write
        (contract, output, [ from_dim x_dim; from_dim y_dim; from_dim f_dim ])

    let dim_list = [ y_dim; x_dim; h_dim; w_dim; c_dim; f_dim ]
    let red_dim_list = [ h_dim; w_dim; c_dim ]

    let gen_code ?(dim_sizes = []) tile_scheme =
      reset ();
      let ( tile_scheme,
            indexes_decl,
            tensor_decl,
            init_dims,
            tensor_free,
            guards,
            loop,
            par_header,
            par_decl,
            par_footer ) =
        gen_loop ~const_dim_sizes:dim_sizes dim_list red_dim_list loop_body
          tile_scheme
      in
      let loop_code = LN.gen_code loop in
      let buffer = Buffer.create 2000 in
      let () =
        List.iter (Buffer.add_string buffer)
          [
            tile_scheme;
            par_header;
            par_decl;
            indexes_decl;
            tensor_decl;
            guards;
            init_dims;
            loop_code;
            tensor_free;
            par_footer;
          ]
      in
      Buffer.contents buffer
  end

  module MM_build (MM_args : MM_args_t) = struct
    include MM_args

    let loop_body =
      let open Inst in
      let from_dim dim = (Dim.id dim, Expr.index @@ Index.from_dim dim) in
      let read_out = Read (c, [ from_dim i_dim; from_dim j_dim ])
      and read_1 = Read (a, [ from_dim i_dim; from_dim k_dim ])
      and read_2 = Read (b, [ from_dim k_dim; from_dim j_dim ]) in
      let contract = Add (read_out, Mul (read_1, read_2)) in
      Write (contract, c, [ from_dim i_dim; from_dim j_dim ])

    let dim_list = [ i_dim; j_dim; k_dim ]
    let red_dim_list = [ k_dim ]

    let gen_code ?(dim_sizes = []) tile_scheme =
      reset ();
      let ( tile_scheme,
            indexes_decl,
            tensor_decl,
            init_dims,
            tensor_free,
            guards,
            loop,
            par_header,
            par_decl,
            par_footer ) =
        gen_loop ~const_dim_sizes:dim_sizes dim_list red_dim_list loop_body
          tile_scheme
      in
      let loop_code = LN.gen_code loop in
      let buffer = Buffer.create 2000 in
      let () =
        List.iter (Buffer.add_string buffer)
          [
            tile_scheme;
            par_header;
            par_decl;
            indexes_decl;
            tensor_decl;
            guards;
            init_dims;
            loop_code;
            tensor_free;
            par_footer;
          ]
      in
      Buffer.contents buffer
  end

  module TC_build (TC_args : TC_args_t) = struct
    include TC_args

    let loop_body =
      let open Inst in
      let from_dim dim = (Dim.id dim, Expr.index @@ Index.from_dim dim) in
      let out_access = List.map from_dim left @ List.map from_dim right in
      let read_out = Read (c, out_access)
      and read_1 = Read (a, List.map from_dim left @ List.map from_dim red)
      and read_2 = Read (b, List.map from_dim red @ List.map from_dim right) in
      let contract = Add (read_out, Mul (read_1, read_2)) in
      Write (contract, c, out_access)

    let dim_list = left @ right @ red

    let gen_code ?(dim_sizes = []) tile_scheme =
      reset ();
      let ( tile_scheme,
            indexes_decl,
            tensor_decl,
            init_dims,
            tensor_free,
            guards,
            loop,
            par_header,
            par_decl,
            par_footer ) =
        gen_loop ~const_dim_sizes:dim_sizes dim_list red loop_body tile_scheme
      in
      let loop_code = LN.gen_code loop in
      let buffer = Buffer.create 2000 in
      let () =
        List.iter (Buffer.add_string buffer)
          [
            tile_scheme;
            par_header;
            par_decl;
            indexes_decl;
            tensor_decl;
            guards;
            init_dims;
            loop_code;
            tensor_free;
            par_footer;
          ]
      in
      Buffer.contents buffer
  end
end
