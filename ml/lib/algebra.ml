open Utils

module type Mappable = sig
  type 'a t

  val map : ('a -> 'b) -> 'a t -> 'b t
end

module A (F : Mappable) = struct
  type 'a fix = Fix of 'a fix F.t

  let out = function Fix fix -> fix
  let in_ t = Fix t

  (* Generalization of fold_right *)
  let rec cata fn = out %> F.map (fun x -> cata fn x) %> fn
  let map fn x = cata (in_ %> fn) x

  (* Generalization of unfold_right *)
  let rec ana fn = fn %> F.map (fun x -> ana fn x) %> in_
  let hylo f g = cata g % ana f

  let rec para ralg =
    let fanout t = (t, para ralg t) in
    out %> F.map (fun x -> fanout x) %> ralg

  let rec para' ralg t = out t |> F.map (fun x -> para' ralg x) |> ralg t

  let rec apo rcoalg =
    let fanin t =
      let open Either in
      match t with Left x -> x | Right y -> apo rcoalg y
    in
    rcoalg %> F.map (fun x -> fanin x) %> in_

  type 'a attr = { attribute : 'a; hole : 'a attr F.t }

  let mk_attr attribute hole = { attribute; hole }
  let attribute { attribute; _ } = attribute
  let hole { hole; _ } = hole
  let ( &&& ) f g x = (f x, g x)

  let histo h =
    let rec worker t =
      out t |> F.map (fun x -> worker x) |> (h &&& Fun.id) |> uncurry mk_attr
    in
    worker %> attribute
end

(** A few examples of application
 * Standard application of TopDown
    let rec _topDown fn  =
    fn
    %> out
    %> F.fmap (_topDown fn)
    %> in_
  *
  * BottomUp can also be implemented very easily with cata
  * let bottomUp f x = cata (in_ %> f) x
  *
  * And same with topDown :
  * let topDown f x = ana (f %> out) x
  *
 * The idea behind this implementation of ana' is that we always want to
 * recurse through the initial structure
 * So we systematically apply right - getting left would mean stop recursing
 * let ana' f = apo (fun x -> F.fmap Either.right (f x))
* Yet another implementation of cata
 * let cata' f = para' (Fun.const f)
*)

module type Algebra_t = sig
  type 'a base
  type 'a fix = Fix of 'a fix base

  val in_ : 'a fix base -> 'a fix
  val out : 'a fix -> 'a fix base
  val map : ('a fix -> 'a fix) -> 'b fix -> 'a fix
  val cata : ('a base -> 'a) -> 'b fix -> 'a
  val ana : ('a -> 'a base) -> 'a -> 'b fix
  val hylo : ('a -> 'a base) -> ('b base -> 'b) -> 'a -> 'b
  val para : (('a fix * 'b) base -> 'b) -> 'a fix -> 'b
  val para' : ('a fix -> 'b base -> 'b) -> 'a fix -> 'b
  val apo : ('a -> ('b fix, 'a) either base) -> 'a -> 'b fix

  type 'a attr

  val attribute : 'a attr -> 'a
  val hole : 'a attr -> 'a attr base
  val mk_attr : 'a -> 'a attr base -> 'a attr
  val histo : ('a attr base -> 'a) -> 'b fix -> 'a
end
