from functools import reduce
import islpy as isl

# A tensor is implicitely represented as a list of sizes

def foldl(iterable, base, f):
    output = base
    for elem in iterable:
        output = f(output, elem)
    return output

def rec_indices_aff(remainings, affs_dict):
    if len(remainings) == 1:
        elem = remainings.pop(0)
        print(affs_dict[elem[0]])
        print(type(affs_dict[elem[0]]))
        return affs_dict[elem[0]]
    else:
        elem = remainings.pop(0)
        inner = rec_indices_aff(remainings, affs_dict)
        mul_expr = affs_dict[elem[1]].mul(inner)
        add_expr = affs_dict[elem[0]].add(mul_expr)
        return add_expr

def rec_indices_expr(remainings):
    if len(remainings) == 1:
        elem = remainings.pop(0)
        return elem[0]
    else:
        elem = remainings.pop(0)
        return "{} + {} * ( {} )".format(elem[0], elem[1], rec_indices_expr(remainings))

def select_indexes(l, l_indexes):
    print(l)
    print(l_indexes)
    return reduce(lambda out, index: out + [l[index]], l_indexes, [])

# Packing is representing as a input tensor and two list : first one
# representing the indices mapped on i (outer dimension of packed tensor)
# second one representing indices mapped on j (inner dimension of packed
# tensor)
class Packing:
    def __init__(self, tensor):
        self.tensor = tensor

    def mapping(self, block_lists):
        dim_names = list(map(lambda arg: ("d_{}".format(arg[0]), "D_{}".format(arg[0])), enumerate(self.tensor)))
        # We create a space with variable coming from the original tensor dimensions, the variable i and j in the blocked space we want to create,
        # the sizes of the blocks and other things
        l = ["i", "j", "I", "J"] + list(sum(dim_names, ()))
        space = isl.Space.create_from_names(isl.DEFAULT_CONTEXT, set=(l))
        affs_dict = isl.affs_from_space(space)
        def add_dim_sizes(isl_map, indices):
            constraint = isl.Constraint.ineq_from_names(space, {1: -1, indices[0]: -1, indices[1]: 1})
            return isl_map.add_constraint(constraint)

        def add_custom_constraint(isl_map, indices, index_list):
            intern_dim_names = select_indexes(dim_names, index_list)
            #ind_expr = rec_indices_expr(dim_names)
            ind_expr = rec_indices_aff(dim_names, affs_dict)
            size_prod_list = list(map(lambda p: p[1], intern_dim_names))
            sizes_prod = " * ".join(size_prod_list)
            expr = isl.Aff.read_from_str(isl_map.get_ctx(), ind_expr)
            cons = isl.Constraint("{} = {}".format(indices[0], ind_expr))
            return (isl_map
                    # This constraint just expresses the fact that i (or j) is a composite index
                    .add_constraint(isl.Constraint("{} = {}".format(indices[0], ind_expr)))
                    # I (or J) is equal to the product of all dimensions sizes involved
                    .add_constraint(isl.Constraint("{} = {}".format(indices[1], sizes_prod)))
                    )
        space_map = isl.BasicMap.universe(space)
        space_map = foldl(dim_names, space_map, add_dim_sizes)
        space_map = add_custom_constraint(space_map, ["i", "I"], block_lists[0])
        space_map = add_custom_constraint(space_map, ["j", "J"], block_lists[1])
        return space_map

p = Packing([3, 14, 18])
mapping = p.mapping(([0, 1], [2]))

print()
